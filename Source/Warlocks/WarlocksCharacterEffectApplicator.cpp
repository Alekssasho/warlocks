// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "WarlocksCharacterEffectApplicator.h"

#include <UObject/ConstructorHelpers.h>

#include "GameFramework/Character.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Materials/MaterialInstance.h"

#include "Warlocks/WarlocksGameplayTagCache.h"
#include "WarlocksBaseCharacter.h"

UCharacterEffectApplicatorComponent::UCharacterEffectApplicatorComponent()
{
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> InvisibleLocalMaterialFinder(TEXT("/Game/Materials/Invisible_Local"));
	if (InvisibleLocalMaterialFinder.Succeeded())
	{
		InvisibleLocalControlledMaterial = InvisibleLocalMaterialFinder.Object;
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInstance> InvisibleRemoteMaterialFinder(TEXT("/Game/Materials/Invisible_Remote"));
	if (InvisibleRemoteMaterialFinder.Succeeded())
	{
		InvisibleRemoteControlledMaterial = InvisibleRemoteMaterialFinder.Object;
	}
}

void UCharacterEffectApplicatorComponent::Setup()
{
	AWarlocksBaseCharacter* Character = GetCharacter();

	DefaultGroundFriction = Character->GetCharacterMovement()->GroundFriction;
	DefaultMaxWalkSpeed = Character->GetCharacterMovement()->MaxWalkSpeed;

	// Add Gameplay Tag Modify Callbacks
	FloorSlideDelegateHandle = Character->GetAbilitySystemComponent()->RegisterGameplayTagEvent(
		FWarlockGameplayTagCache::Get()->FloorSlide,
		EGameplayTagEventType::NewOrRemoved
	).AddUObject(this, &UCharacterEffectApplicatorComponent::FloorSlideChangedServer);

	CharacterSlowDelegateHandle = Character->GetAbilitySystemComponent()->RegisterGameplayTagEvent(
		FWarlockGameplayTagCache::Get()->ModificationCharacterSlow,
		EGameplayTagEventType::NewOrRemoved
	).AddUObject(this, &UCharacterEffectApplicatorComponent::CharacterSpeedChangedServer);

	CharacterFastDelegateHandle = Character->GetAbilitySystemComponent()->RegisterGameplayTagEvent(
		FWarlockGameplayTagCache::Get()->ModificationCharacterFast,
		EGameplayTagEventType::NewOrRemoved
	).AddUObject(this, &UCharacterEffectApplicatorComponent::CharacterSpeedChangedServer);

	CharacterInvisibleDelegateHandle = Character->GetAbilitySystemComponent()->RegisterGameplayTagEvent(
		FWarlockGameplayTagCache::Get()->ModificationCharacterInvisible,
		EGameplayTagEventType::NewOrRemoved
	).AddUObject(this, &UCharacterEffectApplicatorComponent::CharacterInvisibleChangedServer);
}

void UCharacterEffectApplicatorComponent::RemoveDelegates()
{
	if (FloorSlideDelegateHandle.IsValid())
	{
		GetCharacter()->GetAbilitySystemComponent()->UnregisterGameplayTagEvent(FloorSlideDelegateHandle, FWarlockGameplayTagCache::Get()->FloorSlide, EGameplayTagEventType::NewOrRemoved);
		FloorSlideDelegateHandle.Reset();
	}

	if (CharacterSlowDelegateHandle.IsValid())
	{
		GetCharacter()->GetAbilitySystemComponent()->UnregisterGameplayTagEvent(CharacterSlowDelegateHandle, FWarlockGameplayTagCache::Get()->ModificationCharacterSlow, EGameplayTagEventType::NewOrRemoved);
		CharacterSlowDelegateHandle.Reset();
	}

	if (CharacterFastDelegateHandle.IsValid())
	{
		GetCharacter()->GetAbilitySystemComponent()->UnregisterGameplayTagEvent(CharacterFastDelegateHandle, FWarlockGameplayTagCache::Get()->ModificationCharacterFast, EGameplayTagEventType::NewOrRemoved);
		CharacterFastDelegateHandle.Reset();
	}

	if (CharacterInvisibleDelegateHandle.IsValid())
	{
		GetCharacter()->GetAbilitySystemComponent()->UnregisterGameplayTagEvent(CharacterInvisibleDelegateHandle, FWarlockGameplayTagCache::Get()->ModificationCharacterInvisible, EGameplayTagEventType::NewOrRemoved);
		CharacterInvisibleDelegateHandle.Reset();
	}
}

AWarlocksBaseCharacter* UCharacterEffectApplicatorComponent::GetCharacter()
{
	AWarlocksBaseCharacter* Character = Cast<AWarlocksBaseCharacter>(GetOwner());
	return Character;
}

void UCharacterEffectApplicatorComponent::ClientSetGroundFriction_Implementation(float GroundFriction)
{
	GetCharacter()->GetCharacterMovement()->GroundFriction = GroundFriction;
}

void UCharacterEffectApplicatorComponent::ClientSetMaxWalkSpeed_Implementation(float MaxWalkSpeed)
{
	GetCharacter()->GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
}

void UCharacterEffectApplicatorComponent::FloorSlideChangedServer(const FGameplayTag Tag, int32 NewCount)
{
	check(Tag == FWarlockGameplayTagCache::Get()->FloorSlide);
	float NewGroundFriction = DefaultGroundFriction;
	if (NewCount > 0)
	{
		// Start Sliding
		NewGroundFriction = DefaultGroundFriction / 10.0f;
	}

	GetCharacter()->GetCharacterMovement()->GroundFriction = NewGroundFriction;
	ClientSetGroundFriction(NewGroundFriction);
}

void UCharacterEffectApplicatorComponent::CharacterSpeedChangedServer(const FGameplayTag Tag, int32 NewCount)
{
	float NewMaxWalkSpeed = DefaultMaxWalkSpeed;
	if (NewCount > 0)
	{
		if(Tag == FWarlockGameplayTagCache::Get()->ModificationCharacterSlow)
		{
			// Slow
			NewMaxWalkSpeed = DefaultMaxWalkSpeed / 2.0f;
		} else if(Tag == FWarlockGameplayTagCache::Get()->ModificationCharacterFast)
		{
			// Fast
			NewMaxWalkSpeed = DefaultMaxWalkSpeed * 2.0f;
		}
	}

	GetCharacter()->GetCharacterMovement()->MaxWalkSpeed = NewMaxWalkSpeed;
	ClientSetMaxWalkSpeed(NewMaxWalkSpeed);
}

void UCharacterEffectApplicatorComponent::CharacterInvisibleChangedServer(const FGameplayTag Tag, int32 NewCount)
{
	MulticastCharacterToggleInvisible(NewCount > 0);
}

void UCharacterEffectApplicatorComponent::MulticastCharacterToggleInvisible_Implementation(bool ShouldBeInvisible)
{
	// MeshComponent is actually holding only override materials and not the original ones
	// which are kept inside the mesh class and not in the component class.
	// So setting nullptr is just clearing the override material.

	UMaterialInterface* MaterialToSet = nullptr;
	if(ShouldBeInvisible)
	{
		if(GetCharacter()->IsLocallyControlled())
		{
			MaterialToSet = InvisibleLocalControlledMaterial;
		}
		else
		{
			MaterialToSet = InvisibleRemoteControlledMaterial;
		}
	}

	USkeletalMeshComponent* MeshComponent = GetCharacter()->GetMesh();
	const int32 NumMaterials = MeshComponent->GetNumMaterials();
	for (int32 Index = 0; Index < NumMaterials; ++Index)
	{
		MeshComponent->SetMaterial(Index, MaterialToSet);
	}
}
