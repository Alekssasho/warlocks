// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlockAttributes.h"
#include "Warlocks.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameplayEffectExtension.h"
#include "WarlocksBaseCharacter.h"

UWarlockAttributes::UWarlockAttributes()
{
	DefaultInit();
}

void UWarlockAttributes::DefaultInit()
{
	InitHealth(1.0f);
	InitArmor(0.0f);
	InitDamage(0.0f);
}

void UWarlockAttributes::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetHealthAttribute())
	{
		//UE_LOG(LogWarlocks, Warning, TEXT("Health changed to %f %s"), NewValue, *GetOwningActor()->GetName());
		FString M = FString::Printf(TEXT("Health changed to %f %s"), NewValue, *GetOwningActor()->GetName());
		UKismetSystemLibrary::PrintString(GetWorld(), M);
	}
}

void UWarlockAttributes::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	AWarlocksBaseCharacter* TargetCharacter = Cast<AWarlocksBaseCharacter>(Data.Target.GetAvatarActor());
	if (!IsValid(TargetCharacter))
	{
		return;
	}

	if (Data.EvaluatedData.Attribute == GetDamageAttribute()
		&& Data.EvaluatedData.Magnitude > 0.0f)
	{
		const float LocalDamage = GetDamage();
		SetDamage(0.0f); // Nullify the damage as it is transient value and meta attribute

		// LocalDamage has already taken into account armor application
		const float NewHealth = GetHealth() - LocalDamage;
		// TODO: Add MaxHealth Attribute
		SetHealth(FMath::Clamp(NewHealth, 0.0f, 1.0f));

		// If we are dealing damage to Health play hit react
		// Check if we are alive
		if (GetHealth() > 0.0f)
		{
			TargetCharacter->HitReact();
		}
		else
		{
			TargetCharacter->Die();
		}
	}
}

void UWarlockAttributes::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UWarlockAttributes, Health);
	DOREPLIFETIME(UWarlockAttributes, Armor);
}

void UWarlockAttributes::OnRep_Health(const FGameplayAttributeData& Old)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UWarlockAttributes, Health, Old);
}

void UWarlockAttributes::OnRep_Armor(const FGameplayAttributeData& Old)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UWarlockAttributes, Armor, Old);
}