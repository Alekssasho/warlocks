// Fill out your copyright notice in the Description page of Project Settings.


#include "AIControlledCharacter.h"
#include "WarlockAttributes.h"
#include "Warlocks/AI/WarlocksBaseAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"

AAIControlledCharacter::AAIControlledCharacter()
{
	// Create ability system component, and set it to be explicitly replicated
	HardRefAbilitySystem = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	HardRefAbilitySystem->SetIsReplicated(true);

	// Minimal Mode means that no GameplayEffects will replicate. They will only live on the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
	HardRefAbilitySystem->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);

	// Set our parent's TWeakObjectPtr
	AbilitySystem = HardRefAbilitySystem;

	// Create the attribute set, this replicates by default
	// Adding it as a subobject of the owning actor of an AbilitySystemComponent
	// automatically registers the AttributeSet with the AbilitySystemComponent
	HardRefAttributes = CreateDefaultSubobject<UWarlockAttributes>(TEXT("WarlockAttributes"));

	// Set our parent's TWeakObjectPtr
	Attributes = HardRefAttributes;
}

void AAIControlledCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (AbilitySystem.IsValid())
	{
		// Initialize the ability system component. We don't have player state or controller we want
		// for replication so we just give this as both owner and avatar.
		AbilitySystem->InitAbilityActorInfo(this, this);
		if (HasAuthority())
		{
			GiveAbilities();
			EquipItemSet(StartingItemSet);
		}
	}
}


FHitResult AAIControlledCharacter::ProvideLocation()
{
	// TODO: Move this to the controller
	const UBlackboardComponent* BlackboardComp = Controller->FindComponentByClass<UBlackboardComponent>();
	check(BlackboardComp);
	FHitResult Result;
	// TODO: This is a very fragile implementation relying on a string as the key, refactor *somehow*
	FVector TargetLocation = BlackboardComp->GetValue<UBlackboardKeyType_Vector>(FName("TargetLocation"));
	Result.Location = TargetLocation;
	return Result;
}
