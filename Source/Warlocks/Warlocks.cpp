// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Warlocks.h"
#include "Modules/ModuleManager.h"
#include "Serialization/CustomVersion.h"
#include "ISettingsModule.h"

#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Abilities/AbilityCollisionResponseSettings.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FWarlocksModule, Warlocks, "Warlocks");

DEFINE_LOG_CATEGORY(LogWarlocks)

#define LOCTEXT_NAMESPACE "WarlocksNS"

void FWarlocksModule::StartupModule()
{
    if (ISettingsModule* SettingsModule = FModuleManager::Get().GetModulePtr<ISettingsModule>(FName(TEXT("Settings"))))
    {
        SettingsModule->RegisterSettings("Project", "Game", "Abilities Collision Response Settings",
            LOCTEXT("RuntimeSettingsName", "Abilities Collision Response Settings"),
            LOCTEXT("RuntimeSettingsDescription", "Set up collision depending on the ability schools"),
            GetMutableDefault<UAbilityCollisionResponseSettings>()
        );
    }
}

void FWarlocksModule::ShutdownModule()
{
    if (ISettingsModule* SettingsModule = FModuleManager::Get().GetModulePtr<ISettingsModule>(FName(TEXT("Settings"))))
    {
        SettingsModule->UnregisterSettings("Project", "Game", "Abilities Collision Response Settings");
    }
}

#undef LOCTEXT_NAMESPACE