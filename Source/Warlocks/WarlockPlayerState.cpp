// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlockPlayerState.h"
#include "UObject/ConstructorHelpers.h"
#include "Net/UnrealNetwork.h"

AWarlockPlayerState::AWarlockPlayerState()
	: bIsPlayerReadyForNextRound(false)
{
	// Create ability system component, and set it to be explicitly replicated
	AbilitySystem = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystem->SetIsReplicated(true);

	// Mixed mode means we only are replicated the GEs to ourself, not the GEs to simulated proxies. If another GDPlayerState (Hero) receives a GE,
	// we won't be told about it by the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
	AbilitySystem->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	// Create the attribute set, this replicates by default
	// Adding it as a subobject of the owning actor of an AbilitySystemComponent
	// automatically registers the AttributeSet with the AbilitySystemComponent
	Attributes = CreateDefaultSubobject<UWarlockAttributes>(TEXT("WarlockAttributes"));

	CurrencyManager = CreateDefaultSubobject<UCurrencyManagerComponent>(TEXT("CurrencyManager"));

	// Set PlayerState's NetUpdateFrequency to the same as the Character.
	// Default is very low for PlayerStates and introduces perceived lag in the ability system.
	// 100 is probably way too high for a shipping game, you can adjust to fit your needs.
	NetUpdateFrequency = 100.0f;

	static ConstructorHelpers::FObjectFinder<UItemSet> DefaultItemSetFinder(TEXT("/Game/Inventory/ItemSets/DefaultItemSet"));
	if (DefaultItemSetFinder.Succeeded())
	{
		StartingItemSet = DefaultItemSetFinder.Object;
	}
}

UAbilitySystemComponent* AWarlockPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystem;
}

UWarlockAttributes* AWarlockPlayerState::GetAttributes() const
{
	return Attributes;
}

void AWarlockPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWarlockPlayerState, bIsPlayerReadyForNextRound);
}

// Replication helpers
void AWarlockPlayerState::SetReplicatedIsPlayerReadyForNextRound_OnClient(bool bIsPlayerReady)
{
	bIsPlayerReadyForNextRound = bIsPlayerReady;
	ServerSetIsPlayerReadyForNextRound(bIsPlayerReady);
}
void AWarlockPlayerState::ServerSetIsPlayerReadyForNextRound_Implementation(bool bIsPlayerReady)
{
	bIsPlayerReadyForNextRound = bIsPlayerReady;
}