// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PlayerControlledCharacter.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"

#include "WarlockPlayerState.h"
#include "WarlocksPlayerController.h"
#include "GameRules/WarlocksBaseGameMode.h"

APlayerControlledCharacter::APlayerControlledCharacter()
{
	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, -1.0f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// This is needed for paragon character to properly use moving animation
	GetCharacterMovement()->bRequestedMoveUseAcceleration = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	CursorToWorld->SetHiddenInGame(true);

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

// This is called only on the server
void APlayerControlledCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void APlayerControlledCharacter::InitFromPlayerState(class AWarlockPlayerState* InPlayerState)
{
	AbilitySystem = InPlayerState->GetAbilitySystemComponent();
	check(AbilitySystem.IsValid());
	// AI won't have PlayerControllers so we can init again here just to be sure. No harm in initing twice for heroes that have PlayerControllers.
	InPlayerState->GetAbilitySystemComponent()->InitAbilityActorInfo(InPlayerState, this);
	// Set the Attributes for convenience attribute functions
	Attributes = InPlayerState->GetAttributes();
	check(Attributes.IsValid());
}

void APlayerControlledCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	CursorToWorld->SetHiddenInGame(false);

	// Bind player input to the AbilitySystemComponent. Also called in OnRep_PlayerState because of a potential race condition.
	BindAbilitySystemInput();
}

void APlayerControlledCharacter::BindAbilitySystemInput()
{
	if (AbilitySystem.IsValid() && InputComponent && !bIsInputBound)
	{
		AbilitySystem->BindAbilityActivationToInputComponent(InputComponent, FGameplayAbilityInputBinds("ConfirmInput", "CancelInput", "ECharacterSlot"));
		bIsInputBound = true;
	}
}

void APlayerControlledCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			PC->GetHitResultUnderCursor(ECC_Visibility, true, CurrentHitResult);
			FVector CursorFV = CurrentHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(CurrentHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void APlayerControlledCharacter::OnDeath()
{
	CursorToWorld->SetHiddenInGame(true);

	if (AWarlocksPlayerController* PC = Cast<AWarlocksPlayerController>(GetController()))
	{
		PC->OnDeath();
		// Tell the game mode the user died so it distributes rune power (GetAuthGameMode only returns a valid ptr on the server)
		if (AWarlocksBaseGameMode* GameMode = GetWorld()->GetAuthGameMode<AWarlocksBaseGameMode>())
		{
			GameMode->OnPlayerDeath(PC);
		}
	}
}

void APlayerControlledCharacter::OnStunStarted()
{
	if (AWarlocksPlayerController* PC = Cast<AWarlocksPlayerController>(GetController()))
	{
		PC->OnStunStarted();
	}
}

void APlayerControlledCharacter::OnStunEnded()
{
	if (AWarlocksPlayerController* PC = Cast<AWarlocksPlayerController>(GetController()))
	{
		PC->OnStunEnded();
	}
}

FHitResult APlayerControlledCharacter::ProvideLocation()
{
	return CurrentHitResult;
}
