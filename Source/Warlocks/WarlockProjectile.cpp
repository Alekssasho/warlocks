// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlockProjectile.h"

#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "Warlocks/Warlocks.h"
#include "Warlocks/WarlocksGameplayTagCache.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"
#include "Warlocks/Abilities/AbilityCollisionResponseSettings.h"
#include "Warlocks/Abilities/ProjectileAbility.h"
#include "Warlocks/Abilities/ModifyFloorAbility.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Abilities/SpellCastingUtilities.h"


// Sets default values
AWarlockProjectile::AWarlockProjectile()
{
	bReplicates = true;
	SetReplicateMovement(true);

	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	Movement->ProjectileGravityScale = 0.0f; // Disable gravity for projectiles

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	RootComponent = Collision;

	Collision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	Collision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	Collision->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Overlap);
	Collision->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Overlap);

	Collision->OnComponentBeginOverlap.AddUniqueDynamic(this, &AWarlockProjectile::OnOverlap);

	ProjectileVisualParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile Visual Particle System"));
	ProjectileVisualParticleSystem->SetupAttachment(RootComponent);
}

void AWarlockProjectile::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Apply Effects only on the Server
	if (!HasAuthority())
	{
		return;
	}
	// Projectile might already have been marked for destruction e.g. if it just collided with another projectile
	if (IsActorBeingDestroyed())
	{
		return;
	}
	if (AWarlockProjectile* OtherProjectile = Cast<AWarlockProjectile>(Other))
	{
		RespondToProjectileCollision(OtherProjectile);
	}
	else if (Other->IsA<ACharacter>())
	{
		RespondToCharacterCollision(Other);
	}
}

void AWarlockProjectile::RespondToProjectileCollision(AWarlockProjectile* OtherProjectile)
{
	const UAbilityCollisionResponseSettings* Settings = GetDefault<UAbilityCollisionResponseSettings>();
	const FAbilityCollisionResponse& Response = Settings->GetProjectileCollisionResponseForSchools(OwnAbilitySchool, OtherProjectile->OwnAbilitySchool);
	// Switch on the response class
	if (Response.ShouldDestroyAbilities())
	{
		Destroy();
		OtherProjectile->Destroy();
	}

	if (Response.ShouldCastNewAbility())
	{
		const UAbilityDescriptor* Descriptor = Response.GetAbilityToCast();
		check(IsValid(Descriptor));
		UWarlockSlotAbility* Ability = Cast<UWarlockSlotAbility>(Descriptor->AbilityDefinition->GetDefaultObject());
		Ability->SetupAbilityFromDescriptor(Descriptor);

		if (UProjectileAbility* ProjectileAbility = Cast<UProjectileAbility>(Ability))
		{
			CollisionCastProjectile(ProjectileAbility, OtherProjectile);
		}
		else if (UModifyFloorAbility* FloorAbility = Cast<UModifyFloorAbility>(Ability))
		{
			CollisionCastModifyFloor(FloorAbility, OtherProjectile);
		}
		else
		{
			check(false);
		}
	}
}

void AWarlockProjectile::CollisionCastProjectile(UProjectileAbility* AbilityToFire, AWarlockProjectile* OtherProjectile)
{
	const float CosAngle2D = Movement->Velocity.CosineAngle2D(OtherProjectile->Movement->Velocity);
	const float Angle2D = FMath::Acos(CosAngle2D);
	// Magic number - any angle smaller than 15deg will result in annihilation as the resulting velocity will be too small
	const float MaxMergeAngle = PI / 12;
	if (Angle2D >= MaxMergeAngle)
	{
		FTransform NewProjectileTransform = GetTransform();
		FVector ResultingVelocity = (Movement->Velocity + OtherProjectile->Movement->Velocity) / 2;
		FVector Direction = ResultingVelocity;
		Direction.Normalize();
		// avoid a collision between the newly spawned projectile and the other 2
		NewProjectileTransform.AddToTranslation(Direction * Collision->GetScaledSphereRadius() * 2.50); // 250% is empirically chosen to look good

		AWarlockProjectile* ResultingProjectile = FSpellCastingUtilities::SpawnProjectile(
			AbilityToFire->AbilityDescriptor,
			AbilityToFire->GetSchoolData(),
			AbilityToFire->GetAbilityLevel(),
			GetInstigator(), NewProjectileTransform);
		check(ResultingProjectile);

		ResultingProjectile->Movement->bInitialVelocityInLocalSpace = false;
		ResultingProjectile->Movement->Velocity = ResultingVelocity;
	}
}

void AWarlockProjectile::CollisionCastModifyFloor(UModifyFloorAbility* AbilityToFire, AWarlockProjectile* OtherProjectile)
{
	FVector ResultingDirection = (Movement->Velocity + OtherProjectile->Movement->Velocity) / 2;

	FQuat Orientation = FQuat::FindBetweenVectors(FVector::RightVector, ResultingDirection);
	FVector Location = (GetActorLocation() + OtherProjectile->GetActorLocation()) / 2;
	FTransform NewProjectileTransform(Orientation, Location);

	ABaseFloor* ResultingFloor = FSpellCastingUtilities::SpawnModifyingFloor(
		AbilityToFire->AbilityDescriptor,
		AbilityToFire->GetSchoolData(),
		AbilityToFire->GetAbilityLevel(),
		GetInstigator(), NewProjectileTransform);
	check(ResultingFloor);
}

void AWarlockProjectile::RespondToCharacterCollision(AActor* Other)
{
	IAbilitySystemInterface* AbilityInterface = Cast<IAbilitySystemInterface>(Other);

	if (AbilityInterface)
	{
		UAbilitySystemComponent* AbilitySystem = AbilityInterface->GetAbilitySystemComponent();

		// First apply the damage effect
		if (Damage > 0.0f)
		{
			FGameplayEffectSpec DamageSpec(UWarlockDamageEffect::StaticClass()->GetDefaultObject<UGameplayEffect>(), AbilitySystem->MakeEffectContext(), 1);
			DamageSpec.SetSetByCallerMagnitude(
				FWarlockGameplayTagCache::Get()->DamageAmountTag,
				Damage
			);
			AbilitySystem->ApplyGameplayEffectSpecToSelf(DamageSpec);
		}

		// Second apply Effects from Ability
		for (TSubclassOf<UWarlockGameplayEffectContainer>& Effect : GameplayEffectsToApply)
		{
			UWarlockGameplayEffectContainer* DefaultObject = Effect.GetDefaultObject();
			// TODO: Maybe level should be part of the container or should be set inside the projectile
			FGameplayEffectSpec Spec(DefaultObject, AbilitySystem->MakeEffectContext(), 1);
			DefaultObject->ApplyToSpec(&Spec);

			AbilitySystem->ApplyGameplayEffectSpecToSelf(Spec);
		}

		// Apply knockback event
		if (KnockbackForce > 0.0f)
		{
			FGameplayEventData EventData;
			EventData.Instigator = this;
			EventData.EventMagnitude = KnockbackForce;
			AbilitySystem->HandleGameplayEvent(FWarlockGameplayTagCache::Get()->KnockbackTag, &EventData);
		}
	}

	Destroy();
}
