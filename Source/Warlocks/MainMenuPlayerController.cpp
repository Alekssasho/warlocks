// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuPlayerController.h"
#include "Warlocks/UI/HUD/MainMenuHUD.h"

AMainMenuPlayerController::AMainMenuPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

// TODO: SpawnDefaultHUD & InitFromPlayerState_OnClient share implementation with the standard player controller,
// consider merging them
void AMainMenuPlayerController::SpawnDefaultHUD()
{
	if (GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
	{
		// This is called multiple times when we are a ListenServer so avoid recreating the HUD
		if (IsValid(MyHUD))
		{
			return;
		}
	}
	ClientSetHUD(AMainMenuHUD::StaticClass());
}

void AMainMenuPlayerController::InitFromPlayerState_OnClient()
{
	auto HUDInstance = CastChecked<AWarlocksBaseHUD>(MyHUD);
	HUDInstance->UIControllerWidget->InitializeFromPlayerController(this);
}