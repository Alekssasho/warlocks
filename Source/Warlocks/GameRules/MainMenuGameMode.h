// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Warlocks/GameRules/WarlocksBaseGameMode.h"
#include "MainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AMainMenuGameMode : public AWarlocksBaseGameMode
{
	GENERATED_BODY()

public:
	AMainMenuGameMode();
	void HandleMatchIsWaitingToStart();
};
