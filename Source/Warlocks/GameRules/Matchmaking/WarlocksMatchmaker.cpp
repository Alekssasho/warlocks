// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlocksMatchmaker.h"

#include "Engine/Engine.h"
#include "Engine/GameInstance.h"
#include "GameFramework/OnlineSession.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/GameSession.h"
#include "Online.h"

#include "Warlocks/Warlocks.h"

const static FName SESSION_NAME = TEXT("WarlocksGameSession");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("WARLOCKS_SERVER_NAME");
const static FString WARLOCKS_SERVER_KEYWORD = TEXT("WARLOCKS");

void FWarlocksMatchmaker::Init(UGameInstance* GameInstance)
{
	GameInstancePtr = GameInstance;
	SessionInterface = Online::GetSessionInterfaceChecked();

	OnCreateSessionHandle = SessionInterface->OnCreateSessionCompleteDelegates.AddRaw(this, &FWarlocksMatchmaker::OnCreateSessionComplete);
	OnDestroySessionHandle = SessionInterface->OnDestroySessionCompleteDelegates.AddRaw(this, &FWarlocksMatchmaker::OnDestroySessionComplete);
	OnFindSessionsHandle = SessionInterface->OnFindSessionsCompleteDelegates.
		AddRaw(this, &FWarlocksMatchmaker::OnFindSessionsComplete);
	OnJoinSessionHandle = SessionInterface->OnJoinSessionCompleteDelegates.AddRaw(this, &FWarlocksMatchmaker::OnJoinSessionComplete);
}

void FWarlocksMatchmaker::Shutdown()
{
	check(SessionInterface.IsValid());

	SessionInterface->OnCreateSessionCompleteDelegates.Remove(OnCreateSessionHandle);
	SessionInterface->OnDestroySessionCompleteDelegates.Remove(OnDestroySessionHandle);
	SessionInterface->OnFindSessionsCompleteDelegates.Remove(OnFindSessionsHandle);
	SessionInterface->OnJoinSessionCompleteDelegates.Remove(OnJoinSessionHandle);
}

void FWarlocksMatchmaker::Tick()
{
	if (ActivelyHostedSession)
	{
		if (ActivelyHostedSession->SessionState == EOnlineSessionState::Pending && ActivelyHostedSession->NumOpenPublicConnections == 0)
		{
			SessionInterface->StartSession(ActivelyHostedSession->SessionName);
			check(GameInstancePtr.IsValid());
			UWorld* World = GameInstancePtr->GetWorld();
			check(IsValid(World));
			World->ServerTravel("/Game/Maps/MultiRoundPrototypeMap?listen");
		}
	}
}

void FWarlocksMatchmaker::Async_JoinNextAvailableGame()
{
	check(State == EMatchmakerState::Idle);
	Async_UpdateSessionList();
}

const FUniqueNetId* FWarlocksMatchmaker::GetPlayerNetId() const
{
	APlayerController* PlayerController = GameInstancePtr->GetFirstLocalPlayerController();
	if (APlayerState* PlayerState = (PlayerController != NULL) ? PlayerController->PlayerState : NULL)
	{
		return PlayerState->GetUniqueId().GetUniqueNetId().Get();
	}
	return nullptr;
}

void FWarlocksMatchmaker::Async_HostSession()
{
	State = EMatchmakerState::CreatingNewSession;

	check(SessionInterface.IsValid());

	constexpr int32 MaxServerNameIdx = 1000000;
	const int32 ServerIdx = FMath::RandHelper(MaxServerNameIdx);
	const FString ServerName = FString::Printf(TEXT("AUTO_MATCHMAKING_SERVER %d"), ServerIdx);
	UE_LOG(LogWarlocks, Log, TEXT("Creating session %s"), *ServerName);

	FOnlineSessionSettings SessionSettings;
	SessionSettings.bIsLANMatch = false;
	SessionSettings.NumPublicConnections = 2; // TODO: READ FROM DATA
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.bUsesPresence = true;
	SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, ServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	SessionSettings.Set(SEARCH_KEYWORDS, WARLOCKS_SERVER_KEYWORD, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	check(GameInstancePtr.IsValid());
	const UWorld* World = GameInstancePtr->GetWorld();
	check(IsValid(World));
	SessionSettings.Set(SETTING_MAPNAME, World->GetMapName(), EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	// TODO: These 2 should probably be separate?
	SessionSettings.Set(SETTING_NEEDS, SessionSettings.NumPublicConnections, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	if (const FUniqueNetId* PlayerNetId = GetPlayerNetId())
	{
		FName SessionName(ServerName);
		const bool bWasSessionCreated = SessionInterface->CreateSession(*PlayerNetId, SessionName, SessionSettings);
		check(bWasSessionCreated);
		ActivelyHostedSession = SessionInterface->GetNamedSession(SessionName);
	}
}

void FWarlocksMatchmaker::Async_JoinSession(const FOnlineSessionSearchResult& Server)
{
	State = EMatchmakerState::JoiningNewSession;

	FString ServerName;
	const bool bIsNameFound = Server.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName);
	verify(bIsNameFound);

	if (const FUniqueNetId* PlayerNetId = GetPlayerNetId())
	{
		SessionInterface->JoinSession(*PlayerNetId, FName(ServerName), Server);
	}
}

void FWarlocksMatchmaker::Async_UpdateSessionList()
{
	State = EMatchmakerState::UpdatingSessionList;
	ActiveSessionSearch = MakeShareable(new FOnlineSessionSearch());
	check(ActiveSessionSearch.IsValid());
	UE_LOG(LogWarlocks, Log, TEXT("Updating session list"));
	ActiveSessionSearch->bIsLanQuery = false;
	ActiveSessionSearch->MaxSearchResults = 100;
	ActiveSessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	ActiveSessionSearch->QuerySettings.Set(SEARCH_KEYWORDS, WARLOCKS_SERVER_KEYWORD, EOnlineComparisonOp::Equals);


	if (const FUniqueNetId* PlayerNetId = GetPlayerNetId())
	{
		SessionInterface->FindSessions(*PlayerNetId, ActiveSessionSearch.ToSharedRef());
	}
}

void FWarlocksMatchmaker::OnCreateSessionComplete(FName SessionName, bool Success)
{
#if WITH_EDITOR
	if (State != EMatchmakerState::CreatingNewSession)
	{
		// It's possible we are running 2 game instances and 2 matchmakers in the editor in the same process
		// This will cause this callback to be called even when unprovoked so return immediately.
		// TODO: Turns this into an assert that checks if we have multiple game instances in the same process
		return;
	}
#else
	check(State == EMatchmakerState::CreatingNewSession);
#endif

	// It will not be success if there are more than one session with the same name already created
	if (!Success)
	{
		UE_LOG(LogWarlocks, Error, TEXT("Session creation failed!"));
		State = EMatchmakerState::Idle;
		return;
	}

	State = EMatchmakerState::InSession;
	UE_LOG(LogWarlocks, Warning, TEXT("Session created"));
}

void FWarlocksMatchmaker::OnDestroySessionComplete(FName SessionName, bool Success)
{

}

void FWarlocksMatchmaker::OnFindSessionsComplete(bool Success)
{
#if WITH_EDITOR
	if (State != EMatchmakerState::UpdatingSessionList)
	{
		// It's possible we are running 2 game instances and 2 matchmakers in the editor in the same process
		// This will cause this callback to be called even when unprovoked so return immediately.
		// TODO: Turns this into an assert that checks if we have multiple game instances in the same process
		return;
	}
#else
	check(State == EMatchmakerState::UpdatingSessionList);
#endif
	if (!Success)
	{
		State = EMatchmakerState::Idle;
		UE_LOG(LogWarlocks, Error, TEXT("Find sessions failed!"));
		return;
	}
	check(ActiveSessionSearch.IsValid());

	if (ActiveSessionSearch->SearchResults.Num() == 0)
	{
		UE_LOG(LogWarlocks, Warning, TEXT("No Online Sessions Found. Creating a new one"));
		Async_HostSession();
		return;
	}

	// Pick the first available session
	// TODO: in the future do fancy math to group players of similar skill and level
	Async_JoinSession(ActiveSessionSearch->SearchResults[0]);
}

void FWarlocksMatchmaker::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
#if WITH_EDITOR
	if (State != EMatchmakerState::JoiningNewSession)
	{
		// It's possible we are running 2 game instances and 2 matchmakers in the editor in the same process
		// This will cause this callback to be called even when unprovoked so return immediately.
		// TODO: Turns this into an assert that checks if we have multiple game instances in the same process
		return;
	}
#else
	check(State == EMatchmakerState::JoiningNewSession);
#endif

	const FUniqueNetId* PlayerNetId = GetPlayerNetId();
	check(PlayerNetId);
	SessionInterface->RegisterPlayer(SessionName, *PlayerNetId, false);

	State = EMatchmakerState::InSession;

	// Get the url for that session first
	FString Url;
	if (!SessionInterface->GetResolvedConnectString(SessionName, Url))
	{
		return;
	}
	// The player controller travels to that url on that specific 
	// session
	FString MapName;
	bool bGotMapName = SessionInterface->GetSessionSettings(SessionName)->Get(SETTING_MAPNAME, MapName);
	check(bGotMapName);
	Url += TEXT("/") + MapName;
	APlayerController* PlayerController = GameInstancePtr->GetFirstLocalPlayerController();
	PlayerController->ClientTravel(Url, ETravelType::TRAVEL_Absolute);
}