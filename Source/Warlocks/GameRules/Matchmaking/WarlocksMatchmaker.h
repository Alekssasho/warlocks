#pragma once

#include "CoreMinimal.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "UObject/WeakObjectPtr.h"

enum class EMatchmakerState
{
	Idle,
	UpdatingSessionList,
	CreatingNewSession,
	JoiningNewSession,
	InSession
};

// This class takes care of matching people to multiplayer games as the name suggests.
// It makes use of the built-in OnlineSubsystem and operates as a state machine:
//                                /--> CreatingNewSession  \
// Idle --> UpdatingSessionList -                           --> InSession
//                                \-> JoiningNewSession    /
// Basically - we try to find a free session and if one exists, it's joined. If it doesn't it's created.
class WARLOCKS_API FWarlocksMatchmaker
{
public:
	void Init(class UGameInstance* GameInstance);
	void Tick();
	void Shutdown();
	void Async_JoinNextAvailableGame();

private:
	// Create a new session for other players to join
	void Async_HostSession();
	// Join the given existing session 
	void Async_JoinSession(const FOnlineSessionSearchResult& Server);
	// Asks the OnlineSubsystem about available games to be joined.
	void Async_UpdateSessionList();
	const FUniqueNetId* GetPlayerNetId() const;

	// Callbacks called when any of the async calls above complete
	void OnCreateSessionComplete(FName SessionName, bool Success);
	void OnDestroySessionComplete(FName SessionName, bool Success);
	void OnFindSessionsComplete(bool Success);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	IOnlineSessionPtr SessionInterface;
	TSharedPtr<class FOnlineSessionSearch> ActiveSessionSearch;
	EMatchmakerState State;
	class FNamedOnlineSession* ActivelyHostedSession;

	TWeakObjectPtr<class UGameInstance> GameInstancePtr;

	// Delegate handles to remove delegates
	FDelegateHandle OnCreateSessionHandle;
	FDelegateHandle OnDestroySessionHandle;
	FDelegateHandle OnFindSessionsHandle;
	FDelegateHandle OnJoinSessionHandle;
};
