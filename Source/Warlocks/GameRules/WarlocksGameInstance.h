// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Matchmaking/WarlocksMatchmaker.h"
#include "WarlocksGameInstance.generated.h"


/**
 * 
 */
UCLASS()
class WARLOCKS_API UWarlocksGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init();
	virtual void Shutdown();
	bool Tick(float Delta);

	UFUNCTION(BlueprintCallable)
	void JoinNextAvailableGame();

	FWarlocksMatchmaker Matchmaker;

private:
	FTSTicker::FDelegateHandle TickDelegateHandle;
};
