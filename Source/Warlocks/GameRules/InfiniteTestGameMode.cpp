// Fill out your copyright notice in the Description page of Project Settings.


#include "InfiniteTestGameMode.h"
#include "EngineUtils.h"
#if WITH_EDITOR
#include "Editor/UnrealEd/Classes/Settings/LevelEditorPlaySettings.h"
#endif

#include "Warlocks/WarlocksPlayerController.h"
#include "Warlocks/GameRules/StandardMatchGameState.h"

AInfiniteTestGameMode::AInfiniteTestGameMode()
{
	GameStateClass = AStandardMatchGameState::StaticClass();
}

void AInfiniteTestGameMode::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchHasStarted();
	if (GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
	{
		// Init the owned HUD immediately so we can send events to it if we are a listen server
		AWarlocksPlayerController* LocalController = CastChecked<AWarlocksPlayerController>(GetWorld()->GetFirstPlayerController());
		check(LocalController->IsLocalController());
		LocalController->SpawnDefaultHUD();
		LocalController->InitFromPlayerState_OnClient();
		LocalController->ServerAcknowledgePlayerState_Implementation();
	}
}

void AInfiniteTestGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);
		PlayerController->ClientNotifyRoundHasStarted();
	}
}

bool AInfiniteTestGameMode::ReadyToStartMatch_Implementation()
{
#if WITH_EDITOR
	const ULevelEditorPlaySettings* PlayInSettings = GetDefault<ULevelEditorPlaySettings>();
	int32 PlayNumberOfClients;
	PlayInSettings->GetPlayNumberOfClients(PlayNumberOfClients);
	const bool bEnoughPlayersLoggedIn = GetNumPlayers() >= PlayNumberOfClients;
#else
	const bool bEnoughPlayersLoggedIn = true;
#endif

	return bEnoughPlayersLoggedIn && AreAllPlayerStatesAcknowledged();
}