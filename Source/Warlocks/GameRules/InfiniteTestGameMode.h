// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Warlocks/GameRules/WarlocksBaseGameMode.h"
#include "InfiniteTestGameMode.generated.h"

/** Used for development testing. This GameMode immediately starts a single-player match.
 * Use on maps where you can run around and just cast abilities
 */
UCLASS()
class WARLOCKS_API AInfiniteTestGameMode : public AWarlocksBaseGameMode
{
	GENERATED_BODY()
public:
    AInfiniteTestGameMode();
    virtual void HandleMatchIsWaitingToStart() override;
    virtual void HandleMatchHasStarted() override;
    virtual bool ReadyToStartMatch_Implementation() override;
};
