// Fill out your copyright notice in the Description page of Project Settings.


#include "StandardMatchGameMode.h"
#include "GameFramework/PlayerStart.h"
#include "EngineUtils.h"
#include "Engine/Public/TimerManager.h"
#include "Core/Public/Misc/DefaultValueHelper.h"

#if WITH_EDITOR
#include "Editor/UnrealEd/Classes/Settings/LevelEditorPlaySettings.h"
#endif

#include "Warlocks/Warlocks.h"
#include "Warlocks/GameRules/StandardMatchGameState.h"
#include "Warlocks/WarlockPlayerState.h"
#include "Warlocks/WarlocksPlayerController.h"

namespace MatchInProgressState
{
	const FName PreRoundStart = FName(TEXT("PreRoundStart"));
	const FName RoundInProgress = FName(TEXT("RoundInProgress"));
	const FName RoundEnded = FName(TEXT("RoundEnded"));
}

AStandardMatchGameMode::AStandardMatchGameMode()
{
	GameStateClass = AStandardMatchGameState::StaticClass();
	InProgressState = MatchInProgressState::PreRoundStart;
}

bool AStandardMatchGameMode::ReadyToStartMatch_Implementation()
{
#if WITH_EDITOR
	const ULevelEditorPlaySettings* PlayInSettings = GetDefault<ULevelEditorPlaySettings>();
	int32 PlayNumberOfClients = GetWorld()->IsPlayInEditor() ? PlayInSettings->GetPlayNumberOfClients(PlayNumberOfClients) : 2;
	const bool bEnoughPlayersLoggedIn = GetNumPlayers() >= PlayNumberOfClients;
	const bool bIsMatchMultiRound = SettingsInCurrentMap->NumberOfRounds > 1;
	// Start ASAP in single-round maps. This is probably the prototype map and we don't want to wait
	if (!bIsMatchMultiRound)
	{
		ForceSetAllPlayersReadinessFlag(true);
	}
#else
	const bool bEnoughPlayersLoggedIn = true;
#endif

	return bEnoughPlayersLoggedIn && AreAllPlayerStatesAcknowledged() && ReadyToStartRound();
}

bool AStandardMatchGameMode::ReadyToEndMatch_Implementation()
{
	const AStandardMatchGameState* State = GetGameState<AStandardMatchGameState>();
	const bool bIsLastRoundActive = 
		State->CurrentRoundIndex == SettingsInCurrentMap->NumberOfRounds;
	const bool bHasRoundEnded = InProgressState == MatchInProgressState::RoundEnded;

	return bIsLastRoundActive && bHasRoundEnded;
}

void AStandardMatchGameMode::HandleMatchIsWaitingToStart()
{
	if (GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
	{
		// Init the owned HUD immediately so we can send events to it if we are a listen server
		AWarlocksPlayerController* LocalController = CastChecked<AWarlocksPlayerController>(GetWorld()->GetFirstPlayerController());
		check(LocalController->IsLocalController());
		LocalController->SpawnDefaultHUD();
		// Also, immediately acknowledge that controller's player state
		LocalController->InitFromPlayerState_OnClient();
		LocalController->ServerAcknowledgePlayerState_Implementation();
	}

	constexpr int32 SecondsBeforeFirstRound = 3;
	StartRoundTimer(SecondsBeforeFirstRound);

	// Update the PCs the match is in the waiting phase
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = CastChecked<AWarlocksPlayerController>(Iterator->Get());
		PlayerController->ClientNotifyWaitingForMatchToStart();
	}
}

void AStandardMatchGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();
	check(SettingsInCurrentMap);

	GetGameState<AStandardMatchGameState>()->CurrentRoundIndex = 1;

	// Find all player starts and spawn a bot for each unoccupied one
	if (!SettingsInCurrentMap->bDisableBots)
	{
		// TODO: SPAWN BOTS WHICH ACT AS ACTUAL PLAYERS AND HAVE THEIR OWN APLAYERSTATE
		UWorld* World = GetWorld();
		constexpr int32 MaxPlayerCount = 8;
		TArray<APlayerStart*, TFixedAllocator<MaxPlayerCount>> AllPlayerStarts;
		for (TActorIterator<APlayerStart> It(World); It; ++It)
		{
			AllPlayerStarts.Add(*It);
		}

		UClass* BotClass = SettingsInCurrentMap->DefaultBotCharacter;
		for (int32 i = GetNumPlayers(); i < SettingsInCurrentMap->NumberOfPlayers; i++)
		{
			World->SpawnActor<APawn>(BotClass, AllPlayerStarts[i]->GetTransform());
		}
	}

	// Tell PCs the match is starting
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);

		AWarlockPlayerState* PlayerState = PlayerController->GetPlayerState<AWarlockPlayerState>();
		check(PlayerState);
		PlayerState->CurrencyManager->LoadRunePower_OnServer(Iterator.GetIndex());

		PlayerController->ClientNotifyMatchHasStarted();
	}
}

void AStandardMatchGameMode::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);

		AWarlockPlayerState* PlayerState = PlayerController->GetPlayerState<AWarlockPlayerState>();
		check(PlayerState);
		PlayerState->CurrencyManager->SaveRunePower_OnServer(Iterator.GetIndex());

		PlayerController->ClientNotifyMatchHasEnded();
	}
}

AActor* AStandardMatchGameMode::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName)
{
	UWorld* World = GetWorld();
	constexpr int32 MaxPlayerCount = 8;
	TArray<APlayerStart*, TFixedAllocator<MaxPlayerCount>> AllPlayerStarts;
	for (TActorIterator<APlayerStart> It(World); It; ++It)
	{
		AllPlayerStarts.Add(*It);
	}
	// Player Controller's name are by default <PlayerControllerClassName>_<Index>
	// so parse the index
	FString ControllerName = Player->GetFName().ToString();
	int32 LastUnderscorePos;
	const bool bNameFollowsConvention = ControllerName.FindLastChar(TEXT('_'), LastUnderscorePos);
	if (!bNameFollowsConvention)
	{
		return AllPlayerStarts[0];
	}
	FString IndexStr = ControllerName.RightChop(LastUnderscorePos + 1);
	int32 ControllerIndex;
	bool bSuccessfulParse = FDefaultValueHelper::ParseInt(IndexStr, ControllerIndex);
	check(bSuccessfulParse);
	ControllerIndex = AllPlayerStarts.IsValidIndex(ControllerIndex) ? ControllerIndex : 0;
	return AllPlayerStarts[ControllerIndex];
}

void AStandardMatchGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
	// Notify the client to the state we are currenty in
	// Right now we only support joining players in WaitingToStart so nothing else needs to be done
	if (GetMatchState() == MatchState::WaitingToStart)
	{
		CastChecked<AWarlocksPlayerController>(NewPlayer)->ClientNotifyWaitingForMatchToStart();
	}
	else
	{
		UE_LOG(LogGameMode, Warning,
			TEXT("New Player <%s> connected in a currently unsupported game state <%s>."),
			*NewPlayer->GetName(), *GetMatchState().ToString());
	}
}

bool AStandardMatchGameMode::PlayerCanRestart_Implementation(APlayerController* Player)
{
	// PlayerCanRestart is used by the GameMode to determine if it should restart the player
	// at several times in its state machine. Currently, we do not care about the base class
	// ever restarting the players since we explicitly control when players restart in this class.
	return false;
}

void AStandardMatchGameMode::SetInProgressState(const FName& NewStateName)
{
	UE_LOG(LogGameMode, Display, TEXT("Setting InProgress GameMode State to <%s>"), *NewStateName.ToString());
	InProgressState = NewStateName;

	int32 RoundIndex = GetGameState<AStandardMatchGameState>()->CurrentRoundIndex;
	if (NewStateName == MatchInProgressState::PreRoundStart)
	{
		UE_LOG(LogGameMode, Log, TEXT("Preparing for next round. Round Index: <%d>"), RoundIndex);
		HandleRoundPreparation();
	}
	else if (NewStateName == MatchInProgressState::RoundInProgress)
	{
		UE_LOG(LogGameMode, Log, TEXT("Starting next round. Round Index: <%d>"), RoundIndex);
		HandleRoundHasStarted();
	}
	else if (NewStateName == MatchInProgressState::RoundEnded)
	{
		UE_LOG(LogGameMode, Log, TEXT("Ending current round. Round Index: <%d>"), RoundIndex);
		HandleRoundHasEnded();
	}
	else
	{
		checkNoEntry();
	}
}

void AStandardMatchGameMode::HandleRoundPreparation()
{
	GetGameState<AStandardMatchGameState>()->CurrentRoundIndex++;
	ForceSetAllPlayersReadinessFlag(false);
	ResetPlayerAttributes();

	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);
		PlayerController->ClientNotifyPrepareNextRound();
	}
	constexpr int32 SecondsBetweenRounds = 30;
	StartRoundTimer(SecondsBetweenRounds);
}

void AStandardMatchGameMode::HandleRoundHasStarted()
{
	check(AreAllPlayerStatesAcknowledged());
	CancelRoundTimer();

	PlayerDeathOrderInCurrentRound.Empty();

	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);
		RestartPlayer(PlayerController);
		PlayerController->ClientNotifyRoundHasStarted();
	}
}

void AStandardMatchGameMode::HandleRoundHasEnded()
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);

		AWarlockPlayerState* PlayerState = PlayerController->GetPlayerState<AWarlockPlayerState>();
		check(PlayerState);

		const int32 RunePowerToGain = ComputeRunePowerGainInCurrentRoundForPlayer(PlayerState);
		PlayerState->CurrencyManager->GainRunePower_OnServer(RunePowerToGain);
		UE_LOG(LogWarlocks, Display, TEXT("Player <%d> gains <%d> Rune Power."), PlayerState->GetPlayerId(), RunePowerToGain);

		PlayerController->ClientNotifyRoundHasEnded();
	}
}

bool AStandardMatchGameMode::ReadyToStartRound()
{
	const TArray<APlayerState*>& PlayerData = GetGameState<AStandardMatchGameState>()->PlayerArray;

	auto IsPlayerReady = [](APlayerState* PS) { return Cast<AWarlockPlayerState>(PS)->bIsPlayerReadyForNextRound; };
	int32 NumberOfReadyPlayers = PlayerData.FilterByPredicate(IsPlayerReady).Num();
	const bool bAreAllPlayersReady = NumberOfReadyPlayers == GetNumPlayers();
	return bAreAllPlayersReady;
}

bool AStandardMatchGameMode::ReadyToEndRound()
{
	return GetAlivePlayersCount() <= 1;
}

void AStandardMatchGameMode::OnPlayerDeath(APlayerController* Player)
{
	PlayerDeathOrderInCurrentRound.Add(Player->GetPlayerState<AWarlockPlayerState>());
}

void AStandardMatchGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AStandardMatchGameState* State = GetGameState<AStandardMatchGameState>();

	if (GetMatchState() == MatchState::WaitingToStart)
	{
		float TimeLeftUntilRound = GetWorldTimerManager().GetTimerRemaining(RoundTimerHandle);
		State->SecondsUntilNextRound = static_cast<int>(TimeLeftUntilRound);
	}

	// Manage InProgress only if we are actually in progress
	if (GetMatchState() != MatchState::InProgress)
	{
		return;
	}
	if (InProgressState == MatchInProgressState::RoundEnded)
	{
		if (State->CurrentRoundIndex < SettingsInCurrentMap->NumberOfRounds)
		{
			SetInProgressState(MatchInProgressState::PreRoundStart);
		}
	}
	if (InProgressState == MatchInProgressState::PreRoundStart)
	{
		float TimeLeftUntilRound = GetWorldTimerManager().GetTimerRemaining(RoundTimerHandle);
		State->SecondsUntilNextRound = static_cast<int>(TimeLeftUntilRound);
		if (ReadyToStartRound())
		{
			SetInProgressState(MatchInProgressState::RoundInProgress);
		}
	}
	if (InProgressState == MatchInProgressState::RoundInProgress)
	{
		if (ReadyToEndRound())
		{
			SetInProgressState(MatchInProgressState::RoundEnded);
		}
	}
}

void AStandardMatchGameMode::StartRoundTimer(int32 SecondsUntilRound)
{
	GetWorldTimerManager().SetTimer(
		RoundTimerHandle,
		this, &AStandardMatchGameMode::OnRoundTimerElapsed,
		SecondsUntilRound,
		false /* bLoop */);
}

void AStandardMatchGameMode::CancelRoundTimer()
{
	GetWorldTimerManager().ClearTimer(RoundTimerHandle);
}

void AStandardMatchGameMode::OnRoundTimerElapsed()
{
	ForceSetAllPlayersReadinessFlag(true);
}

int32 AStandardMatchGameMode::ComputeRunePowerGainInCurrentRoundForPlayer(AWarlockPlayerState* Player) const
{
	check(InProgressState == MatchInProgressState::RoundEnded);

	int32 DeathIndex = PlayerDeathOrderInCurrentRound.Find(Player);
	if (DeathIndex == INDEX_NONE)
	{
		// Must be the winner as he hasn't died!
		DeathIndex = PlayerDeathOrderInCurrentRound.Num();
	}
	// TODO: MAGIC NUMBERS
	return (DeathIndex + 1) * 10;
}

int32 AStandardMatchGameMode::GetAlivePlayersCount() const
{
	const TArray<APlayerState*>& PlayerData = GetGameState<AStandardMatchGameState>()->PlayerArray;

	auto IsPlayerAlive = [](APlayerState* PS) { return Cast<AWarlockPlayerState>(PS)->GetAttributes()->GetHealth() > 0; };
	int32 NumberOfAlivePlayers = PlayerData.FilterByPredicate(IsPlayerAlive).Num();
	return NumberOfAlivePlayers;
}

void AStandardMatchGameMode::ForceSetAllPlayersReadinessFlag(bool bArePlayersReady)
{
	const TArray<APlayerState*>& PlayerData = GetGameState<AStandardMatchGameState>()->PlayerArray;

	for (APlayerState* PlayerState : PlayerData)
	{
		AWarlockPlayerState* WarlockPlayerState = CastChecked<AWarlockPlayerState>(PlayerState);
		WarlockPlayerState->bIsPlayerReadyForNextRound = bArePlayersReady;
	}
}

void AStandardMatchGameMode::ResetPlayerAttributes()
{
	const TArray<APlayerState*>& PlayerData = GetGameState<AStandardMatchGameState>()->PlayerArray;
	for (APlayerState* PlayerState : PlayerData)
	{
		AWarlockPlayerState* WarlockPlayerState = CastChecked<AWarlockPlayerState>(PlayerState);
		// TODO: Move DefaultInit to the PlayerState to make the implementation more flexible?
		WarlockPlayerState->GetAttributes()->DefaultInit();
	}
}
