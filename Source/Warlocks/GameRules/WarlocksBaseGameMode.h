// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "WarlocksBaseGameMode.generated.h"

UCLASS(minimalapi)
class AWarlocksBaseGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AWarlocksBaseGameMode();

	// UI management
	// Do nothing on initialize HUD - the player controller will spawn the default UI and we don't care about replacing
	// it as it would normally happen
	virtual void InitializeHUDForPlayer_Implementation(APlayerController* NewPlayer) override {}
	virtual void OnPlayerDeath(APlayerController* Player) {}
protected:
	bool AreAllPlayerStatesAcknowledged() const;
};



