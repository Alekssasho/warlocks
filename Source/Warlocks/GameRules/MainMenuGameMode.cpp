// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuGameMode.h"
#include "EngineUtils.h"
#include "Warlocks/MainMenuPlayerController.h"

AMainMenuGameMode::AMainMenuGameMode()
{
	PlayerControllerClass = AMainMenuPlayerController::StaticClass();
}

void AMainMenuGameMode::HandleMatchIsWaitingToStart()
{
	Super::HandleMatchHasStarted();
	if (GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
	{
		// Init the owned HUD immediately so we can send events to it if we are a listen server
		AMainMenuPlayerController* LocalController = CastChecked<AMainMenuPlayerController>(GetWorld()->GetFirstPlayerController());
		check(LocalController->IsLocalController());
		LocalController->SpawnDefaultHUD();
	}
}