// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "WarlocksBaseGameMode.h"
#include "Warlocks/WarlocksPlayerController.h"
#include "Warlocks/WarlockPlayerState.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/HUD.h"
#include "Warlocks/Warlocks.h"
#include "Warlocks/UI/HUD/WarlocksBaseHUD.h"

AWarlocksBaseGameMode::AWarlocksBaseGameMode()
{
	PlayerControllerClass = AWarlocksPlayerController::StaticClass();
	PlayerStateClass = AWarlockPlayerState::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// HUD class
	HUDClass = AWarlocksBaseHUD::StaticClass();
}

bool AWarlocksBaseGameMode::AreAllPlayerStatesAcknowledged() const
{
	bool bAreAllPlayerStatesAcknowledged = true;

	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		AWarlocksPlayerController* PlayerController = Cast<AWarlocksPlayerController>(Iterator->Get());
		check(PlayerController);
		bAreAllPlayerStatesAcknowledged &= PlayerController->HasAcknowledgedPlayerState_OnServer();
	}
	return bAreAllPlayerStatesAcknowledged;
}