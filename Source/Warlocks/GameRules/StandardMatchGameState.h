// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "StandardMatchGameState.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AStandardMatchGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	AStandardMatchGameState();
	// Replication
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// One-based index of the current round. A value of 0 means no round has started yet.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	int32 CurrentRoundIndex;

	// Seconds until next round starts. A value of -1 means no timer is ticking.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	int32 SecondsUntilNextRound;
};
