// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WarlocksBaseGameMode.h"
#include "MapSettings.h"
#include "StandardMatchGameMode.generated.h"

namespace MatchInProgressState
{
	extern const FName PreRoundStart;
	extern const FName RoundInProgress;
	extern const FName RoundEnded;
}

class AWarlockPlayerState;

/**
 * 
 */
UCLASS()
class WARLOCKS_API AStandardMatchGameMode : public AWarlocksBaseGameMode
{
	GENERATED_BODY()
public:
	AStandardMatchGameMode();
	// Standard GameMode State Machine
	virtual bool ReadyToStartMatch_Implementation() override;
	virtual bool ReadyToEndMatch_Implementation() override;
	virtual void HandleMatchIsWaitingToStart() override;
	virtual void HandleMatchHasStarted() override;
	virtual void HandleMatchHasEnded() override;
	virtual AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName) override;
	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;
	virtual bool PlayerCanRestart_Implementation(APlayerController* Player) override;

	// Extensions to support rounds; we basically implement an internal state machine akin to the one driving AGameMode
	// which is nested in the InProgress state of the base class
	void SetInProgressState(const FName& StateName);
	void HandleRoundPreparation();
	void HandleRoundHasStarted();
	void HandleRoundHasEnded();
	bool ReadyToStartRound();
	bool ReadyToEndRound();

	// Base Warlocks Game Mode overrides
	virtual void OnPlayerDeath(APlayerController* Player) override;

	// Controls the inner state machine
	virtual void Tick(float DeltaSeconds) override;
	void StartRoundTimer(int32 SecondsUntilRound);
	void CancelRoundTimer();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MapSettings)
	TObjectPtr<UMapSettings> SettingsInCurrentMap;

private:
	// Must always be called after the round has ended!
	int32 ComputeRunePowerGainInCurrentRoundForPlayer(AWarlockPlayerState* Player) const;
	int32 GetAlivePlayersCount() const;
	void ForceSetAllPlayersReadinessFlag(bool bArePlayersReady);
	void ResetPlayerAttributes();
	void OnRoundTimerElapsed();

	FName InProgressState;
	FTimerHandle RoundTimerHandle;
	// Stores the order in which player died in the current round; the winner will probably not be in the list as he didn't die;
	// check the length of the array to confirm that
	TArray<AWarlockPlayerState*> PlayerDeathOrderInCurrentRound;
};
