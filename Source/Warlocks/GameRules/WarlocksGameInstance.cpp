// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlocksGameInstance.h"

#include "Containers/Ticker.h"
#include "GameFramework/OnlineSession.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/GameSession.h"

#include "Warlocks/Warlocks.h"

void UWarlocksGameInstance::Init()
{
	Super::Init();

	Matchmaker.Init(this);
	TickDelegateHandle = FTSTicker::GetCoreTicker().AddTicker(FTickerDelegate::CreateUObject(this, &UWarlocksGameInstance::Tick));
}

void UWarlocksGameInstance::Shutdown()
{
	FTSTicker::GetCoreTicker().RemoveTicker(TickDelegateHandle);
	Matchmaker.Shutdown();

	Super::Shutdown();
}

bool UWarlocksGameInstance::Tick(float DeltaSeconds)
{
	Matchmaker.Tick();

	return true;
}

void UWarlocksGameInstance::JoinNextAvailableGame()
{
	Matchmaker.Async_JoinNextAvailableGame();
}
