// Fill out your copyright notice in the Description page of Project Settings.


#include "StandardMatchGameState.h"
#include "Net/UnrealNetwork.h"

AStandardMatchGameState::AStandardMatchGameState()
	: CurrentRoundIndex(0)
	, SecondsUntilNextRound(-1)
{}

void AStandardMatchGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AStandardMatchGameState, CurrentRoundIndex);
	DOREPLIFETIME(AStandardMatchGameState, SecondsUntilNextRound);
}