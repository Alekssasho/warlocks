// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MapSettings.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UMapSettings : public UDataAsset
{
	GENERATED_BODY()

public:
	UMapSettings();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MapSettings, meta=(ClampMin=1))
	int32 NumberOfPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MapSettings, meta = (ClampMin = 1))
	int32 NumberOfRounds;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MapSettings)
	TObjectPtr<UClass> DefaultBotCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Development)
	bool bDisableBots;
};
