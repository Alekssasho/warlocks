#pragma once

#include "CoreMinimal.h"
#include "Subsystems/EngineSubsystem.h"
#include "WarlocksEngineSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UWarlocksEngineSubsystem : public UEngineSubsystem
{
	GENERATED_BODY()
	
public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
};
