// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WarlockSlotAbility.h"
#include "AuraAbility.generated.h"

class UAuraComponent;
UCLASS()
class WARLOCKS_API UAuraAbility : public UWarlockSlotAbility
{
	GENERATED_UCLASS_BODY()
public:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* OwnerInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
private:
	UFUNCTION()
	void MontageCancel();
	UFUNCTION()
	void MontageEffectCast();
	UFUNCTION()
	void TimerFinish();

	UPROPERTY()
	TObjectPtr<UAuraComponent> AvatarAuraComponent;
};
