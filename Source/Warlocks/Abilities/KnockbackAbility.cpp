// Fill out your copyright notice in the Description page of Project Settings.


#include "KnockbackAbility.h"
#include "AbilitySystemComponent.h"
#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_ApplyRootMotionConstantForce.h"
#include "GameFramework/RootMotionSource.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Warlocks/Warlocks.h"
#include "Warlocks/WarlocksGameplayTagCache.h"

UKnockbackAbility::UKnockbackAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::ServerInitiated;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	// This ability is triggered with an Event
	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = FWarlockGameplayTagCache::Get()->KnockbackTag;
	AbilityTriggers.Push(TriggerData);

	// This ability is blocked from immunity
	ActivationBlockedTags.AddTag(FWarlockGameplayTagCache::Get()->ImmunityKnockback);
}

void UKnockbackAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	ensure(TriggerEventData);
	if (!IsValid(TriggerEventData->Instigator))
	{
		UE_LOG(LogWarlocks, Warning, TEXT("Knockback without proper instigator"));
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
		return;
	}

	FVector KnockbackDirection = GetAvatarActorFromActorInfo()->GetActorLocation() - TriggerEventData->Instigator->GetActorLocation();
	// We don't want any Z movement
	KnockbackDirection = KnockbackDirection.GetUnsafeNormal2D();
	UAbilityTask_ApplyRootMotionConstantForce* Task = UAbilityTask_ApplyRootMotionConstantForce::ApplyRootMotionConstantForce(
		this,
		TEXT("Knockback Task"),
		KnockbackDirection,
		TriggerEventData->EventMagnitude,
		0.2f,
		true,
		nullptr,
		ERootMotionFinishVelocityMode::SetVelocity,
		FVector::ZeroVector,
		0.0f,
		false
	);
	Task->OnFinish.AddDynamic(this, &UKnockbackAbility::OnFinished);
	Task->ReadyForActivation();
}

void UKnockbackAbility::OnFinished()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
