#include "PlayWarlockAbilityCastingMontageTask.h"
#include "GameFramework/Character.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"

#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_NetworkSyncPoint.h"

UPlayWarlockAbilityCastingMontageTask::UPlayWarlockAbilityCastingMontageTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Rate = 1.f;
}

void UPlayWarlockAbilityCastingMontageTask::OnMontageInterrupted()
{
	if (StopPlayingMontage())
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnCancelled.Broadcast();
		}
	}
}

void UPlayWarlockAbilityCastingMontageTask::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (!bInterrupted)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnCompleted.Broadcast();
		}
	}

	EndTask();
}

void UPlayWarlockAbilityCastingMontageTask::OnMontageNotify(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (Ability
		&& Ability->GetCurrentMontage() == MontageToPlay
		&& ShouldBroadcastAbilityTaskDelegates()
		&& NotifyName == FName("EffectCast"))
	{
		UAbilityTask_NetworkSyncPoint* SyncTask = UAbilityTask_NetworkSyncPoint::WaitNetSync(Ability, EAbilityTaskNetSyncType::OnlyServerWait);
		SyncTask->OnSync.AddDynamic(this, &UPlayWarlockAbilityCastingMontageTask::OnSync);
		SyncTask->ReadyForActivation();
	}
}

void UPlayWarlockAbilityCastingMontageTask::OnSync()
{
	OnEffectCast.Broadcast();
}

UPlayWarlockAbilityCastingMontageTask* UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
	UGameplayAbility* OwningAbility,
	FName TaskInstanceName,
	UAnimMontage *MontageToPlay,
	float Rate)
{
	UAbilitySystemGlobals::NonShipping_ApplyGlobalAbilityScaler_Rate(Rate);

	UPlayWarlockAbilityCastingMontageTask* MyObj = NewAbilityTask<UPlayWarlockAbilityCastingMontageTask>(OwningAbility, TaskInstanceName);
	MyObj->MontageToPlay = MontageToPlay;
	MyObj->Rate = Rate;

	return MyObj;
}

void UPlayWarlockAbilityCastingMontageTask::Activate()
{
	if (Ability == nullptr)
	{
		return;
	}

	bool bPlayedMontage = false;

	if (AbilitySystemComponent)
	{
		const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
		UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			if (AbilitySystemComponent->PlayMontage(Ability, Ability->GetCurrentActivationInfo(), MontageToPlay, Rate) > 0.f)
			{
				// Playing a montage could potentially fire off a callback into game code which could kill this ability! Early out if we are  pending kill.
				if (ShouldBroadcastAbilityTaskDelegates() == false)
				{
					return;
				}

				InterruptedHandle = Ability->OnGameplayAbilityCancelled.AddUObject(this, &UPlayWarlockAbilityCastingMontageTask::OnMontageInterrupted);

				MontageEndedDelegate.BindUObject(this, &UPlayWarlockAbilityCastingMontageTask::OnMontageEnded);
				AnimInstance->Montage_SetEndDelegate(MontageEndedDelegate, MontageToPlay);

				AnimInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &UPlayWarlockAbilityCastingMontageTask::OnMontageNotify);

				bPlayedMontage = true;
			}
		}
		else
		{
			ABILITY_LOG(Warning, TEXT("UPlayWarlockAbilityCastingMontageTask call to PlayMontage failed!"));
		}
	}
	else
	{
		ABILITY_LOG(Warning, TEXT("UPlayWarlockAbilityCastingMontageTask called on invalid AbilitySystemComponent"));
	}

	if (!bPlayedMontage)
	{
		ABILITY_LOG(Warning, TEXT("UPlayWarlockAbilityCastingMontageTask called in Ability %s failed to play montage %s; Task Instance Name %s."), *Ability->GetName(), *GetNameSafe(MontageToPlay),*InstanceName.ToString());
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnCancelled.Broadcast();
		}
	}

	SetWaitingOnAvatar();
}

void UPlayWarlockAbilityCastingMontageTask::ExternalCancel()
{
	check(AbilitySystemComponent);

	if (ShouldBroadcastAbilityTaskDelegates())
	{
		OnCancelled.Broadcast();
	}

	Super::ExternalCancel();
}

void UPlayWarlockAbilityCastingMontageTask::OnDestroy(bool AbilityEnded)
{
	// Note: Clearing montage end delegate isn't necessary since its not a multicast and will be cleared when the next montage plays.
	// (If we are destroyed, it will detect this and not do anything)

	// This delegate, however, should be cleared as it is a multicast
	if (Ability)
	{
		Ability->OnGameplayAbilityCancelled.Remove(InterruptedHandle);

		const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
		if (ActorInfo)
		{
			UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();
			if (AnimInstance)
			{
				AnimInstance->OnPlayMontageNotifyBegin.RemoveDynamic(this, &UPlayWarlockAbilityCastingMontageTask::OnMontageNotify);
			}
		}
	}

	Super::OnDestroy(AbilityEnded);
}

bool UPlayWarlockAbilityCastingMontageTask::StopPlayingMontage()
{
	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
	if (!ActorInfo)
	{
		return false;
	}

	UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();
	if (AnimInstance == nullptr)
	{
		return false;
	}

	AnimInstance->OnPlayMontageNotifyBegin.RemoveDynamic(this, &UPlayWarlockAbilityCastingMontageTask::OnMontageNotify);

	// Check if the montage is still playing
	// The ability would have been interrupted, in which case we should automatically stop the montage
	if (AbilitySystemComponent && Ability)
	{
		if (AbilitySystemComponent->GetAnimatingAbility() == Ability
			&& AbilitySystemComponent->GetCurrentMontage() == MontageToPlay)
		{
			// Unbind delegates so they don't get called as well
			FAnimMontageInstance* MontageInstance = AnimInstance->GetActiveInstanceForMontage(MontageToPlay);
			if (MontageInstance)
			{
				MontageInstance->OnMontageBlendingOutStarted.Unbind();
				MontageInstance->OnMontageEnded.Unbind();
			}

			AbilitySystemComponent->CurrentMontageStop();
			return true;
		}
	}

	return false;
}

FString UPlayWarlockAbilityCastingMontageTask::GetDebugString() const
{
	UAnimMontage* PlayingMontage = nullptr;
	if (Ability)
	{
		const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
		UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();

		if (AnimInstance != nullptr)
		{
			PlayingMontage = AnimInstance->Montage_IsActive(MontageToPlay) ? MontageToPlay : AnimInstance->GetCurrentActiveMontage();
		}
	}

	return FString::Printf(TEXT("PlayMontageAndWait. MontageToPlay: %s  (Currently Playing): %s"), *GetNameSafe(MontageToPlay), *GetNameSafe(PlayingMontage));
}
