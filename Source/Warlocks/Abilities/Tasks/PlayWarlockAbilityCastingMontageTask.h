#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Animation/AnimInstance.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "PlayWarlockAbilityCastingMontageTask.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMontageWaitSimpleDelegate);

UCLASS()
class WARLOCKS_API UPlayWarlockAbilityCastingMontageTask : public UAbilityTask
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(BlueprintAssignable)
	FMontageWaitSimpleDelegate OnCompleted;

	UPROPERTY(BlueprintAssignable)
	FMontageWaitSimpleDelegate OnCancelled;

	UPROPERTY(BlueprintAssignable)
	FMontageWaitSimpleDelegate OnEffectCast;

	UFUNCTION(BlueprintCallable, Category="Warlock Ability|Tasks", meta = (DisplayName="Play Warlock Ability Casting Montage Task",
		HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UPlayWarlockAbilityCastingMontageTask* CreatePlayWarlockAbilityCastingMontageTaskProxy(UGameplayAbility* OwningAbility,
		FName TaskInstanceName,
		UAnimMontage *MontageToPlay,
		float Rate = 1.f);

	virtual void Activate() override;

	/** Called when the ability is asked to cancel from an outside node. What this means depends on the individual task. By default, this does nothing other than ending the task. */
	virtual void ExternalCancel() override;

	virtual FString GetDebugString() const override;

private:
	UFUNCTION()
	void OnSync();

	UFUNCTION()
	void OnMontageInterrupted();

	UFUNCTION()
	void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);

	UFUNCTION()
	void OnMontageNotify(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);

	virtual void OnDestroy(bool AbilityEnded) override;

	/** Checks if the ability is playing a montage and stops that montage, returns true if a montage was stopped, false if not. */
	bool StopPlayingMontage();

	FOnMontageEnded MontageEndedDelegate;
	FDelegateHandle InterruptedHandle;

	UPROPERTY()
	TObjectPtr<UAnimMontage> MontageToPlay;

	UPROPERTY()
	float Rate;
};
