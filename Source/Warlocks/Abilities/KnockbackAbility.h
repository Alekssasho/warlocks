// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "KnockbackAbility.generated.h"

// This ability is not using shared cooldowns so it can directly inherint UGameplayAbility instead of UWarlockAbility
UCLASS()
class WARLOCKS_API UKnockbackAbility : public UGameplayAbility
{
	GENERATED_UCLASS_BODY()

public:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* OwnerInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

private:
	UFUNCTION()
	void OnFinished();
};
