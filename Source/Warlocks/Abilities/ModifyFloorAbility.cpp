// Fill out your copyright notice in the Description page of Project Settings.


#include "ModifyFloorAbility.h"

#include <Warlocks/Blueprint/WarlocksBlueprintLibrary.h>

#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include "GameFramework/Character.h"

#include "Warlocks/Abilities/Tasks/PlayWarlockAbilityCastingMontageTask.h"
#include "Warlocks/Abilities/LocationTargetActor.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Abilities/SpellCastingUtilities.h"
#include "Warlocks/Warlocks.h"

UModifyFloorAbility::UModifyFloorAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void UModifyFloorAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	AActor* Avatar = GetAvatarActorFromActorInfo();
	if (!Avatar->Implements<ULocationProviderInterface>())
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
		UE_LOG(LogWarlocks, Error, TEXT("Avatar cannot provide location for targeting"));
		return;
	}

	ALocationTargetActor* TargetingActor = GetWorld()->SpawnActor<ALocationTargetActor>();
	TargetingActor->Provider = Cast<ILocationProviderInterface>(Avatar);
	TargetingActor->MaxRange = AbilityDescriptor->Range.GetValueAtLevel(GetAbilityLevel());
	TargetingActor->bWallOrientation = AbilityDescriptor->bIsWallOrientation;
	if (!AbilityDescriptor->bIsInstantTargeting)
	{
		AActor* VisualizationActor = GetWorld()->SpawnActor<AActor>(GetSchoolData().ModifyFloorActorToSpawn);
		TargetingActor->SetupActorMesh(VisualizationActor, AbilityDescriptor->TargetingActorMaterial);
		GetWorld()->DestroyActor(VisualizationActor);
	}

	UAbilityTask_WaitTargetData* TargetingTask = UAbilityTask_WaitTargetData::WaitTargetDataUsingActor(
		this,
		NAME_None,
		AbilityDescriptor->bIsInstantTargeting ? EGameplayTargetingConfirmation::Instant : EGameplayTargetingConfirmation::UserConfirmed,
		TargetingActor);

	TargetingTask->ValidData.AddDynamic(this, &UModifyFloorAbility::TargetConfirm);
	TargetingTask->Cancelled.AddDynamic(this, &UModifyFloorAbility::TargetCancel);
	TargetingTask->ReadyForActivation();
}

void UModifyFloorAbility::TargetConfirm(const FGameplayAbilityTargetDataHandle& Data)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo)) {
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	// TODO: Check if we can bind it to the delegate and pass it
	// We need to change the delegate to be non dynamic to work properly
	// Probably we don't need it as multicast as well.
	EndLocation = Data.Get(0)->GetHitResult()->Location;

	// TODO: Do we need Turn In Place support for this effect ?

	if (MontageToPlay)
	{
		UPlayWarlockAbilityCastingMontageTask* MontagePlayTask =
			UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
				this,
				NAME_None,
				MontageToPlay
			);

		MontagePlayTask->OnCancelled.AddDynamic(this, &UModifyFloorAbility::MontageCancel);
		MontagePlayTask->OnEffectCast.AddDynamic(this, &UModifyFloorAbility::MontageEffectCast);
		MontagePlayTask->OnCompleted.AddDynamic(this, &UModifyFloorAbility::MontageCompleted);
		MontagePlayTask->ReadyForActivation();
	}
	else
	{
		MontageEffectCast();
	}
}

void UModifyFloorAbility::MontageEffectCast()
{
	if (!GetOwningActorFromActorInfo()->HasAuthority())
	{
		return;
	}

	ACharacter* Avatar = Cast<ACharacter>(GetAvatarActorFromActorInfo());

	// Spawn only on the Server
	EndLocation += FVector::UpVector * 5.0f;
	FRotator Orientation = FRotator::ZeroRotator;
	if(AbilityDescriptor->bIsWallOrientation)
	{
		Orientation = UWarlocksBlueprintLibrary::OrientWall(Avatar->GetActorLocation(), EndLocation);
	}
	FTransform Transform(Orientation, EndLocation);
	FSpellCastingUtilities::SpawnModifyingFloor(AbilityDescriptor, GetSchoolData(), GetAbilityLevel(), Avatar, Transform);
}

void UModifyFloorAbility::MontageCompleted()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UModifyFloorAbility::MontageCancel()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}

void UModifyFloorAbility::TargetCancel(const FGameplayAbilityTargetDataHandle& Data)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}