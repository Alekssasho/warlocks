// Fill out your copyright notice in the Description page of Project Settings.


#include "GroundRadiusEffectAbility.h"
#include "GameFramework/Character.h"
#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include "GameplayAbilities/Public/Abilities/GameplayAbilityTargetActor_Radius.h"
#include "GameplayAbilities/Public/AbilitySystemBlueprintLibrary.h"

#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Abilities/Tasks/PlayWarlockAbilityCastingMontageTask.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"

UGroundRadiusEffectAbility::UGroundRadiusEffectAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void UGroundRadiusEffectAbility::SetupAbilityFromDescriptor(TObjectPtr<const UAbilityDescriptor> Descriptor)
{
	Super::SetupAbilityFromDescriptor(Descriptor);

	SocketOrigin = Descriptor->SocketOrigin;
	DamageValueSet = Descriptor->PerSchoolData[uint8_t(WarlockAbilitySchool)].Damage;
	KnockbackForce = Descriptor->PerSchoolData[uint8_t(WarlockAbilitySchool)].KnockbackForce;
	GameplayCueTagToPlay = Descriptor->GameplayCueTagToPlay;
}

void UGroundRadiusEffectAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo)) {
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	if (MontageToPlay)
	{
		UPlayWarlockAbilityCastingMontageTask* MontagePlayTask =
			UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
				this,
				NAME_None,
				MontageToPlay
			);

		MontagePlayTask->OnCancelled.AddDynamic(this, &UGroundRadiusEffectAbility::MontageCancel);
		MontagePlayTask->OnEffectCast.AddDynamic(this, &UGroundRadiusEffectAbility::MontageEffectCast);
		MontagePlayTask->ReadyForActivation();
	}
	else
	{
		MontageEffectCast();
	}
}

void UGroundRadiusEffectAbility::MontageEffectCast()
{
	float EffectiveRadius = AbilityDescriptor->Range.GetValueAtLevel(GetAbilityLevel());

	ACharacter* Avatar = Cast<ACharacter>(GetAvatarActorFromActorInfo());
	FGameplayCueParameters GameplayCueParameters;
	GameplayCueParameters.RawMagnitude = EffectiveRadius;
	GameplayCueParameters.Location = Avatar->GetMesh()->GetSocketLocation(SocketOrigin);

	UAbilitySystemComponent* const AbilitySystemComponent = GetAbilitySystemComponentFromActorInfo_Checked();
	AbilitySystemComponent->ExecuteGameplayCue(GameplayCueTagToPlay.GameplayCueTag, GameplayCueParameters);

	// Apply the effects only on the server
	if (GetOwningActorFromActorInfo()->HasAuthority())
	{
		AGameplayAbilityTargetActor_Radius* TargetingActor = GetWorld()->SpawnActor<AGameplayAbilityTargetActor_Radius>();
		TargetingActor->Radius = EffectiveRadius;
		TargetingActor->StartLocation = MakeTargetLocationInfoFromOwnerActor();

		FGameplayTargetDataFilter* Filter = new FGameplayTargetDataFilter;
		Filter->SelfFilter = ETargetDataFilterSelf::Type::TDFS_NoSelf;
		Filter->bReverseFilter = false;
		Filter->InitializeFilterContext(Avatar);
		TargetingActor->Filter.Filter = TSharedPtr<FGameplayTargetDataFilter>(Filter);

		UAbilityTask_WaitTargetData* TargetingTask = UAbilityTask_WaitTargetData::WaitTargetDataUsingActor(
			this,
			NAME_None,
			EGameplayTargetingConfirmation::Instant,
			TargetingActor);

		TargetingTask->ValidData.AddDynamic(this, &UGroundRadiusEffectAbility::TargetConfirm);
		TargetingTask->Cancelled.AddDynamic(this, &UGroundRadiusEffectAbility::TargetCancel);
		TargetingTask->ReadyForActivation();
	}
	else
	{
		// End the ability on the client
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
	}
}

void UGroundRadiusEffectAbility::TargetConfirm(const FGameplayAbilityTargetDataHandle& TargetData)
{
	if (GetOwningActorFromActorInfo()->HasAuthority())
	{
		FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpec(UWarlockDamageEffect::StaticClass(), GetAbilityLevel());
		SpecHandle.Data.Get()->SetSetByCallerMagnitude(
			FWarlockGameplayTagCache::Get()->DamageAmountTag,
			DamageValueSet.GetValueAtLevel(GetAbilityLevel())
		);

		ApplyGameplayEffectSpecToTarget(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, SpecHandle, TargetData);

		float KnockbackAmount = KnockbackForce.GetValueAtLevel(GetAbilityLevel());
		if (KnockbackAmount > 0.0f)
		{
			FGameplayEventData EventData;
			EventData.Instigator = GetAvatarActorFromActorInfo();
			EventData.EventMagnitude = KnockbackAmount;
			FGameplayTag KnockbackTag = FWarlockGameplayTagCache::Get()->KnockbackTag;

			for (TWeakObjectPtr<AActor>& Actor : TargetData.Get(0)->GetActors())
			{
				UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Actor.Get(), KnockbackTag, EventData);
			}
		}

		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
	}
}

void UGroundRadiusEffectAbility::MontageCancel()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}

void UGroundRadiusEffectAbility::TargetCancel(const FGameplayAbilityTargetDataHandle& Data)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}