#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Abilities/GameplayAbilityTargetTypes.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "LocationProviderInterface.h"
#include "LocationTargetActor.generated.h"

UENUM()
enum class EReticlePosition : uint8
{
	AttachToCaster,
	AttachToLocation
};

UCLASS(Blueprintable, notplaceable)
class WARLOCKS_API ALocationTargetActor : public AGameplayAbilityTargetActor
{
	GENERATED_UCLASS_BODY()
public:
	virtual void EndPlay(EEndPlayReason::Type Reason) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void StartTargeting(class UGameplayAbility* Ability) override;

	virtual void ConfirmTargetingAndContinue() override;

	ILocationProviderInterface* Provider;
	float MaxRange;

	bool bWallOrientation;

	EReticlePosition ReticlePosition;

	void SetupActorMesh(AActor* Actor, UMaterial* Material);
private:
	FVector ClampToRange(FVector DesiredLocation);

	TWeakObjectPtr<class AGameplayAbilityWorldReticle> ReticleActor;

	UPROPERTY()
	TObjectPtr<class UCapsuleComponent> CollisionComponent;

	UPROPERTY()
	TObjectPtr<class UMeshComponent> MeshComponent;

	UPROPERTY()
	TObjectPtr<class UDecalComponent> DecalComponent;
};
