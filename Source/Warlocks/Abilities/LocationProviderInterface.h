#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Engine/EngineTypes.h"
#include "LocationProviderInterface.generated.h"

UINTERFACE(MinimalAPI)
class ULocationProviderInterface : public UInterface
{
	GENERATED_BODY()
};

class WARLOCKS_API ILocationProviderInterface
{
	GENERATED_BODY()
public:
	virtual FHitResult ProvideLocation() = 0;
};
