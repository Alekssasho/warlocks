// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Warlocks/CommonTypes.h"
#include "Warlocks/WarlockProjectile.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "AbilityCollisionResponseSettings.generated.h"

UENUM(meta=(Bitflags))
enum ECollisionInteraction
{
	DestroySourceAbilities = 1 << 1,
	CastNewAbility = 1 << 2,
};

USTRUCT(BlueprintType, meta=(UsesHierarchy))
struct FAbilityCollisionResponse
{
	GENERATED_BODY()

public:
	bool ShouldDestroyAbilities() const
	{
		return CollisionInteractionBitset & ECollisionInteraction::DestroySourceAbilities;
	}

	bool ShouldCastNewAbility() const
	{
		return CollisionInteractionBitset & ECollisionInteraction::CastNewAbility;
	}

	virtual const UAbilityDescriptor* GetAbilityToCast() const
	{
		return ResultingAbilityDescriptor;
	}

//protected:
	UPROPERTY(EditAnywhere, meta=(Bitmask, BitmaskEnum=ECollisionInteraction))
	int32 CollisionInteractionBitset;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "ShouldCastNewAbility()"))
	TObjectPtr<UAbilityDescriptor> ResultingAbilityDescriptor;
};

/**
 * Ability schools are placed on a regular polygon, the collision response between each of them
 * is dependent on the angle between the points on the polygon
 */
UCLASS(Config=Game, DefaultConfig)
class WARLOCKS_API UAbilityCollisionResponseSettings : public UObject
{
	GENERATED_BODY()

public:
	// Element 0 is placed at (1, 0), every subsequent is placed on the next available polygon point
	// clockwise
	UPROPERTY(Config, EditAnywhere)
	TArray<EAbilitySchool> SchoolOrder;

	// The response which should be triggered whenever 2 abilities of the given distance collide
	// Distance 0 means 2 abilities of the same school
	// Distance 1 means neighbours & etc.
	UPROPERTY(Config, EditAnywhere)
	TArray<FAbilityCollisionResponse> ProjectileCollisionResponsesPerDistance;

	const FAbilityCollisionResponse& GetProjectileCollisionResponseForSchools(EAbilitySchool First, EAbilitySchool Second) const;
};
