// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportAbility.h"
#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include "GameFramework/Character.h"

#include "Warlocks/Abilities/Tasks/PlayWarlockAbilityCastingMontageTask.h"
#include "Warlocks/Abilities/LocationTargetActor.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Warlocks.h"

UTeleportAbility::UTeleportAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void UTeleportAbility::SetupAbilityFromDescriptor(TObjectPtr<const class UAbilityDescriptor> Descriptor)
{
	Super::SetupAbilityFromDescriptor(Descriptor);

	bIsInstantTargeting = Descriptor->bIsInstantTargeting;
	Range = Descriptor->Range;
	ReticleClass = Descriptor->ReticleClass;
}

void UTeleportAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	AActor* Avatar = GetAvatarActorFromActorInfo();
	if (!Avatar->Implements<ULocationProviderInterface>())
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
		UE_LOG(LogWarlocks, Error, TEXT("Avatar cannot provide location for targeting"));
		return;
	}

	ALocationTargetActor* TargetingActor = GetWorld()->SpawnActor<ALocationTargetActor>();
	TargetingActor->Provider = Cast<ILocationProviderInterface>(Avatar);
	TargetingActor->MaxRange = Range.GetValueAtLevel(GetAbilityLevel());
	if (!bIsInstantTargeting)
	{
		TargetingActor->ReticlePosition = EReticlePosition::AttachToLocation;
		TargetingActor->ReticleClass = ReticleClass;
	}

	UAbilityTask_WaitTargetData* TargetingTask = UAbilityTask_WaitTargetData::WaitTargetDataUsingActor(
		this,
		NAME_None,
		bIsInstantTargeting ? EGameplayTargetingConfirmation::Instant : EGameplayTargetingConfirmation::UserConfirmed,
		TargetingActor);

	TargetingTask->ValidData.AddDynamic(this, &UTeleportAbility::TargetConfirm);
	TargetingTask->Cancelled.AddDynamic(this, &UTeleportAbility::TargetCancel);
	TargetingTask->ReadyForActivation();
}

void UTeleportAbility::TargetConfirm(const FGameplayAbilityTargetDataHandle& Data)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo)) {
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	// TODO: Check if we can bind it to the delegate and pass it
	// We need to change the delegate to be non dynamic to work properly
	// Probably we don't need it as multicast as well.
	EndLocation = Data.Get(0)->GetHitResult()->Location;

	// TODO: Do we need Turn In Place support for this effect ?

	if (MontageToPlay)
	{
		UPlayWarlockAbilityCastingMontageTask* MontagePlayTask =
			UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
				this,
				NAME_None,
				MontageToPlay
			);

		MontagePlayTask->OnCancelled.AddDynamic(this, &UTeleportAbility::MontageCancel);
		MontagePlayTask->OnEffectCast.AddDynamic(this, &UTeleportAbility::MontageEffectCast);
		MontagePlayTask->OnCompleted.AddDynamic(this, &UTeleportAbility::MontageCompleted);
		MontagePlayTask->ReadyForActivation();
	}
	else
	{
		MontageEffectCast();
	}
}

void UTeleportAbility::MontageEffectCast()
{
	if (!GetOwningActorFromActorInfo()->HasAuthority())
	{
		return;
	}

	// Teleport Only on Server
	ACharacter* Avatar = Cast<ACharacter>(GetAvatarActorFromActorInfo());
	Avatar->TeleportTo(EndLocation, Avatar->GetActorRotation());
}

void UTeleportAbility::MontageCompleted()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UTeleportAbility::MontageCancel()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}

void UTeleportAbility::TargetCancel(const FGameplayAbilityTargetDataHandle& Data)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}