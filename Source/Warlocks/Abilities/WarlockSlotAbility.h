// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"

#include "Warlocks/CommonTypes.h"

#include "WarlockSlotAbility.generated.h"

class UAbilityDescriptor;
struct FAbilityDescriptorData;

UCLASS()
class WARLOCKS_API UWarlockSlotAbility : public UGameplayAbility
{
	GENERATED_UCLASS_BODY()
public:
	virtual const FGameplayTagContainer* GetCooldownTags() const override;
	virtual void ApplyCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const override;
	virtual void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;

	virtual void SetupAbilityFromDescriptor(TObjectPtr<const UAbilityDescriptor> Descriptor);


	UPROPERTY()
	TObjectPtr<const UAbilityDescriptor> AbilityDescriptor;

	const FAbilityDescriptorData& GetSchoolData() const;

protected:
	EAbilitySchool WarlockAbilitySchool;
	EAbilityLevel WarlockAbilityLevel;

	UPROPERTY()
	TObjectPtr<UAnimMontage> MontageToPlay;
private:
	UPROPERTY()
	FScalableFloat CooldownDuration;
	UPROPERTY()
	FGameplayTagContainer CooldownTags;
	UPROPERTY()
	TObjectPtr<UGameplayEffect> CooldownEffect;
};
