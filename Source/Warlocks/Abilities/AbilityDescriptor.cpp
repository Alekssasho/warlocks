// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AbilityDescriptor.h"
#include "Warlocks/Abilities/Effects/WarlockEffectContainer.h"

UAbilityDescriptor::UAbilityDescriptor()
{
	PerSchoolData.SetNum(uint8_t(EAbilitySchool::Count));
}
