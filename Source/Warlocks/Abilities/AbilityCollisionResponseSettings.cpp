// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityCollisionResponseSettings.h"


const FAbilityCollisionResponse& UAbilityCollisionResponseSettings::GetProjectileCollisionResponseForSchools(EAbilitySchool First, EAbilitySchool Second) const
{
    // d - absolute distance
    // c - school count (ring size)
    // distanceInRing = c/2 - abs(c/2 - d); check in wolfram alpha to confirm this function results in what we need
    const int SchoolDistanceAbsolute = SchoolOrder.Find(First) - SchoolOrder.Find(Second);
    const int HalfRingSize = static_cast<int>(EAbilitySchool::Count) / 2;
    const int SchoolDistanceInRing = HalfRingSize - FMath::Abs(SchoolDistanceAbsolute - HalfRingSize);
    const int ResponseIndex = FMath::Min(SchoolDistanceInRing, ProjectileCollisionResponsesPerDistance.Num());
    return ProjectileCollisionResponsesPerDistance[ResponseIndex];
}