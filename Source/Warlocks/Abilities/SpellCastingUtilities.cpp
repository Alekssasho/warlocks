#include "Warlocks/Abilities/SpellCastingUtilities.h"


ABaseFloor* FSpellCastingUtilities::SpawnModifyingFloor(const UAbilityDescriptor* Descriptor, const FAbilityDescriptorData& SchoolData, int32 Level, APawn* Avatar, const FTransform& Transform)
{
	ABaseFloor* Floor = Avatar->GetWorld()->SpawnActorDeferred<ABaseFloor>(
		SchoolData.ModifyFloorActorToSpawn,
		Transform,
		Avatar,
		Cast<APawn>(Avatar),
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	Floor->InitialLifeSpan = SchoolData.EffectDuration.GetValueAtLevel(Level);
	Floor->FinishSpawning(Transform);

	return Floor;
}

AWarlockProjectile* FSpellCastingUtilities::SpawnProjectile(const UAbilityDescriptor* Descriptor, const FAbilityDescriptorData& SchoolData, int32 Level, APawn* Avatar, const FTransform& Transform)
{
	AWarlockProjectile* Projectile = Avatar->GetWorld()->SpawnActorDeferred<AWarlockProjectile>(
		SchoolData.ProjectileClass,
		Transform,
		Avatar,
		Cast<APawn>(Avatar),
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

	// Damage and School special effect
	Projectile->Damage = SchoolData.Damage.GetValueAtLevel(Level);
	if (SchoolData.ProjectileHitEffectToApply)
	{
		Projectile->GameplayEffectsToApply.Add(SchoolData.ProjectileHitEffectToApply);
	}
	Projectile->KnockbackForce = SchoolData.KnockbackForce.GetValueAtLevel(Level);
	Projectile->GetMovement()->InitialSpeed = Descriptor->ProjectileSpeed.GetValueAtLevel(Level);
	Projectile->InitialLifeSpan = Descriptor->Range.GetValueAtLevel(Level) / Projectile->GetMovement()->InitialSpeed;

	Projectile->FinishSpawning(Transform);

	return Projectile;
}