// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include <GameplayEffectTypes.h>
#include <GameplayTagContainer.h>
#include <Components/SphereComponent.h>

#include "AuraComponent.generated.h"

struct FGameplayTag;
UCLASS()
class WARLOCKS_API UAuraComponent : public USphereComponent
{
	GENERATED_BODY()
public:
	UAuraComponent();
	~UAuraComponent();

	// Should be called before FinishAddComponent
	void InitializeAuraData(float Range, FGameplayTag TagToApply);
	void RemoveAllActiveEffects();

private:
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	FGameplayTag TagToApply;

	UPROPERTY()
	TMap<AActor*, FActiveGameplayEffectHandle> ActiveInfiniteDurationEffects;
};
