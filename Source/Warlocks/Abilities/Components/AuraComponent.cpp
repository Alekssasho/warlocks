// Fill out your copyright notice in the Description page of Project Settings.


#include "AuraComponent.h"

#include <AbilitySystemInterface.h>
#include <GameplayEffect.h>
#include <Warlocks/Abilities/Effects/WarlockCoreEffects.h>

UAuraComponent::UAuraComponent() {
	SetIsReplicatedByDefault(true);

	SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	OnComponentBeginOverlap.AddUniqueDynamic(this, &UAuraComponent::OnOverlapBegin);
	OnComponentEndOverlap.AddUniqueDynamic(this, &UAuraComponent::OnOverlapEnd);
}

UAuraComponent::~UAuraComponent()
{
	ensure(ActiveInfiniteDurationEffects.Num() == 0);
}

void UAuraComponent::InitializeAuraData(float Range, FGameplayTag Tag)
{
	InitSphereRadius(Range);

	TagToApply = Tag;
}

void UAuraComponent::RemoveAllActiveEffects()
{
	for(TPair<AActor*, FActiveGameplayEffectHandle> Handle : ActiveInfiniteDurationEffects)
	{
		IAbilitySystemInterface* Interface = Cast<IAbilitySystemInterface>(Handle.Key);
		UAbilitySystemComponent* AbilitySystem = Interface->GetAbilitySystemComponent();
		AbilitySystem->RemoveActiveGameplayEffect(Handle.Value);
	}
	ActiveInfiniteDurationEffects.Empty();
}

void UAuraComponent::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Don't do anything on the clients
	if (!GetOwner()->HasAuthority())
	{
		return;
	}

	// Don't overlap with self
	if(OtherActor == GetOwner())
	{
		return;
	}

	IAbilitySystemInterface* Interface = Cast<IAbilitySystemInterface>(OtherActor);
	// If the other actor doesn't have AbilitySystemInterface just return
	if (!Interface)
	{
		return;
	}

	UAbilitySystemComponent* AbilitySystem = Interface->GetAbilitySystemComponent();
	if (!AbilitySystem)
	{
		return;
	}

	float Level = 1;

	UWarlockTagApplyEffect* Effect = UWarlockTagApplyEffect::StaticClass()->GetDefaultObject<UWarlockTagApplyEffect>();
	FGameplayEffectSpec Spec(Effect, AbilitySystem->MakeEffectContext(), Level);
	Spec.DynamicGrantedTags.AddTag(TagToApply);

	FActiveGameplayEffectHandle Handle = AbilitySystem->ApplyGameplayEffectSpecToSelf(Spec);
	ActiveInfiniteDurationEffects.Add(OtherActor, Handle);
}

void UAuraComponent::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// Don't do anything on the clients
	if (!GetOwner()->HasAuthority())
	{
		return;
	}

	// Don't overlap with self
	if (OtherActor == GetOwner())
	{
		return;
	}

	IAbilitySystemInterface* Interface = Cast<IAbilitySystemInterface>(OtherActor);
	// If the other actor doesn't have AbilitySystemInterface just return
	if (!Interface)
	{
		return;
	}

	UAbilitySystemComponent* AbilitySystem = Interface->GetAbilitySystemComponent();
	if (!AbilitySystem)
	{
		return;
	}

	FActiveGameplayEffectHandle Handle = ActiveInfiniteDurationEffects.FindAndRemoveChecked(OtherActor);
	AbilitySystem->RemoveActiveGameplayEffect(Handle);
}
