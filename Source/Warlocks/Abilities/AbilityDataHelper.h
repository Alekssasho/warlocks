// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Warlocks/Abilities/AbilityDescriptor.h"

#include "AbilityDataHelper.generated.h"

// This class is used only for the editor to generate automatically the UI for configuring data inside AbilityDescriptor
UCLASS(BlueprintType)
class UAbilityDataHelper : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FAbilityDescriptorData Data;
};


