// Fill out your copyright notice in the Description page of Project Settings.


#include "Warlocks/Abilities/SelfBuffAbility.h"

#include <AbilitySystemComponent.h>
#include <Warlocks/WarlocksGameplayTagCache.h>


#include "Warlocks/Abilities/Tasks/PlayWarlockAbilityCastingMontageTask.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"


USelfBuffAbility::USelfBuffAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void USelfBuffAbility::SetupAbilityFromDescriptor(TObjectPtr<const UAbilityDescriptor> Descriptor)
{
	Super::SetupAbilityFromDescriptor(Descriptor);

	BuffEffectClass = Descriptor->PerSchoolData[uint8_t(WarlockAbilitySchool)].SelfBuffToApply;
	EffectDuration = Descriptor->PerSchoolData[uint8_t(WarlockAbilitySchool)].EffectDuration;
}

void USelfBuffAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo)) {
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	if (MontageToPlay)
	{
		UPlayWarlockAbilityCastingMontageTask* MontagePlayTask =
			UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
				this,
				NAME_None,
				MontageToPlay
			);

		MontagePlayTask->OnCancelled.AddDynamic(this, &USelfBuffAbility::MontageCancel);
		MontagePlayTask->OnEffectCast.AddDynamic(this, &USelfBuffAbility::MontageEffectCast);
		MontagePlayTask->ReadyForActivation();
	}
	else
	{
		MontageEffectCast();
	}
}

void USelfBuffAbility::MontageEffectCast()
{
	FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpec(BuffEffectClass, GetAbilityLevel());
	SpecHandle.Data.Get()->SetSetByCallerMagnitude(
		FWarlockGameplayTagCache::Get()->DurationTag,
		EffectDuration.GetValueAtLevel(GetAbilityLevel())
	);

	ApplyGameplayEffectSpecToOwner(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, SpecHandle);

	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void USelfBuffAbility::MontageCancel()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}
