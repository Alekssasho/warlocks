// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WarlockSlotAbility.h"
#include "Warlocks/WarlockProjectile.h"
#include "ProjectileAbility.generated.h"

UCLASS()
class WARLOCKS_API UProjectileAbility : public UWarlockSlotAbility
{
	GENERATED_UCLASS_BODY()
public:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* OwnerInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void SetupAbilityFromDescriptor(TObjectPtr<const class UAbilityDescriptor> Descriptor) override;

	AWarlockProjectile* SpawnProjectile(APawn* Avatar, const FTransform& Transform);
private:
	UFUNCTION()
	void TargetConfirm(const FGameplayAbilityTargetDataHandle& Data);
	UFUNCTION()
	void TargetCancel(const FGameplayAbilityTargetDataHandle& Data);
	UFUNCTION()
	void MontageCancel();
	UFUNCTION()
	void MontageEffectCast();

	UFUNCTION()
	void MontageCompleted();

	FVector EndLocation;
};
