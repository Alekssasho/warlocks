// Fill out your copyright notice in the Description page of Project Settings.


#include "AuraAbility.h"

#include <Components/SphereComponent.h>
#include <Warlocks/Abilities/Components/AuraComponent.h>


#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "GameFramework/Character.h"

#include "Warlocks/Abilities/Tasks/PlayWarlockAbilityCastingMontageTask.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"

UAuraAbility::UAuraAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void UAuraAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo)) {
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	if (MontageToPlay)
	{
		UPlayWarlockAbilityCastingMontageTask* MontagePlayTask =
			UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
				this,
				NAME_None,
				MontageToPlay
			);

		MontagePlayTask->OnCancelled.AddDynamic(this, &UAuraAbility::MontageCancel);
		MontagePlayTask->OnEffectCast.AddDynamic(this, &UAuraAbility::MontageEffectCast);
		// We don't want to end the ability when the montage is done, as this will kill the spawned task.
		//MontagePlayTask->OnCompleted.AddDynamic(this, &UAuraAbility::MontageCompleted);
		MontagePlayTask->ReadyForActivation();
	}
	else
	{
		MontageEffectCast();
	}
}

void UAuraAbility::MontageEffectCast()
{
	if (!GetOwningActorFromActorInfo()->HasAuthority())
	{
		return;
	}

	AActor* Avatar = GetAvatarActorFromActorInfo();

	// TODO: if both this actor and another actor are not moving and we spawn this component it will not receive on overlap
	// However this looks like a unusual case, as the game is very dynamic
	AvatarAuraComponent = Cast<UAuraComponent>(Avatar->AddComponentByClass(UAuraComponent::StaticClass(), false, FTransform(), true));
	AvatarAuraComponent->InitializeAuraData(AbilityDescriptor->Range.GetValueAtLevel(GetAbilityLevel()), GetSchoolData().AuraTagToApply);
	Avatar->FinishAddComponent(AvatarAuraComponent, false, FTransform());

	UAbilityTask_WaitDelay* WaitTask = UAbilityTask_WaitDelay::WaitDelay(this, GetSchoolData().EffectDuration.GetValueAtLevel(GetAbilityLevel()));
	WaitTask->OnFinish.AddDynamic(this, &UAuraAbility::TimerFinish);
	WaitTask->ReadyForActivation();
}

void UAuraAbility::TimerFinish()
{
	// Aura API, remove all active effects
	AvatarAuraComponent->RemoveAllActiveEffects();

	// Remove component
	AvatarAuraComponent->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	AvatarAuraComponent->UnregisterComponent();
	AvatarAuraComponent->MarkAsGarbage();
	AvatarAuraComponent = nullptr;

	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void UAuraAbility::MontageCancel()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}
