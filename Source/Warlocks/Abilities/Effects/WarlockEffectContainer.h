#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/Public/GameplayEffect.h"
#include "WarlockEffectContainer.generated.h"

USTRUCT(BlueprintType)
struct WARLOCKS_API FWarlockSetByCallerCapture
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FGameplayTag SetByCallerTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FScalableFloat FloatValue;
};

UCLASS(Blueprintable)
class WARLOCKS_API UWarlockGameplayEffectContainer : public UGameplayEffect
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<FWarlockSetByCallerCapture> SetByCallerCaptures;

	void ApplyToSpec(FGameplayEffectSpec* Spec)
	{
		for (const FWarlockSetByCallerCapture& Capture : SetByCallerCaptures)
		{
			Spec->SetSetByCallerMagnitude(
				Capture.SetByCallerTag,
				Capture.FloatValue.GetValueAtLevel(Spec->GetLevel())
			);
		}
	}
};
