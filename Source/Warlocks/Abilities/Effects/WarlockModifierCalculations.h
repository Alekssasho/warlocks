// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/Public/GameplayModMagnitudeCalculation.h"
#include "GameplayAbilities/Public/GameplayEffectExecutionCalculation.h"
#include "Warlocks/WarlockAttributes.h"
#include "Warlocks/WarlocksGameplayTagCache.h"
#include "WarlockModifierCalculations.generated.h"

// This could potentially capture Source caster attribues in order to modulate the results
// However in order to support spawning random actors carrying effects with them we cannot
// count on there being an actual caster for abilities. Source modification should be calculated
// up front and applied to the proper magnitudes.
UCLASS()
class WARLOCKS_API UWarlockDamageModMagnitudeCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

	DECLARE_ATTRIBUTE_CAPTUREDEF(Armor);
public:
	UWarlockDamageModMagnitudeCalculation()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UWarlockAttributes, Armor, Target, false);

		RelevantAttributesToCapture.Add(ArmorDef);
	}

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
	{
		// Gather the tags from the source and target as that can affect which buffs should be used
		const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
		const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

		FAggregatorEvaluateParameters EvaluationParameters;
		EvaluationParameters.SourceTags = SourceTags;
		EvaluationParameters.TargetTags = TargetTags;

		float Armor = 0.f;
		GetCapturedAttributeMagnitude(ArmorDef, Spec, EvaluationParameters, Armor);

		float DamageAmount = Spec.GetSetByCallerMagnitude(FWarlockGameplayTagCache::Get()->DamageAmountTag, true, 0.0f);

		// Scale DamageAmount if enhanced (without Armor being applied)
		if(TargetTags->HasTagExact(FWarlockGameplayTagCache::Get()->EnhanceDamage))
		{
			DamageAmount *= 2.0f; // TODO: Maybe this needs to be taken from somewhere data defined
		}

		// Simple formula - Armor is percentage value between 0 and 1 - reduce the damage by that amount
		float FinalDamage = DamageAmount * (1.0f - Armor);

		return FinalDamage;
	}
};

