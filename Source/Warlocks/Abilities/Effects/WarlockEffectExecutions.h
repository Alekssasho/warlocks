// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/Public/GameplayEffectExecutionCalculation.h"
#include "Warlocks/WarlockAttributes.h"
#include "Warlocks/WarlocksGameplayTagCache.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "WarlockEffectExecutions.generated.h"

UCLASS()
class WARLOCKS_API UWarlockStunExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:
	UWarlockStunExecution()
	{
	}

	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
	{
		AWarlocksBaseCharacter* TargetCharacter = Cast<AWarlocksBaseCharacter>(ExecutionParams.GetTargetAbilitySystemComponent()->GetAvatarActor());
		if (!IsValid(TargetCharacter))
		{
			return;
		}

		// This should be called only on the server
		ensure(TargetCharacter->HasAuthority());

		float ChanceToStun = ExecutionParams.GetOwningSpec().GetSetByCallerMagnitude(FWarlockGameplayTagCache::Get()->StunChanceTag);
		if(FMath::FRand() < ChanceToStun)
		{
			TargetCharacter->Stun();
		}
	}
};

