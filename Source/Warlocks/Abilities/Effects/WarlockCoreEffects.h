// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/Public/GameplayEffect.h"
#include "Warlocks/WarlockAttributes.h"
#include "Warlocks/Abilities/Effects/WarlockEffectContainer.h"
#include "Warlocks/Abilities/Effects/WarlockModifierCalculations.h"
#include "WarlockCoreEffects.generated.h"

UCLASS()
class WARLOCKS_API UWarlockSlotCooldownEffect : public UWarlockGameplayEffectContainer
{
	GENERATED_BODY()
public:
	UWarlockSlotCooldownEffect()
	{
		DurationPolicy = EGameplayEffectDurationType::HasDuration;

		FSetByCallerFloat SetByCallerFloat;
		SetByCallerFloat.DataTag = FWarlockGameplayTagCache::Get()->CooldownTag;
		DurationMagnitude = FGameplayEffectModifierMagnitude(SetByCallerFloat);
	}
};

UCLASS()
class WARLOCKS_API UWarlockDamageEffect : public UWarlockGameplayEffectContainer
{
	GENERATED_BODY()
public:
	UWarlockDamageEffect()
	{
		DurationPolicy = EGameplayEffectDurationType::Instant;

		Modifiers.SetNum(1);
		FGameplayModifierInfo& Damage = Modifiers[0];

		FCustomCalculationBasedFloat CustomMMC;
		CustomMMC.CalculationClassMagnitude = UWarlockDamageModMagnitudeCalculation::StaticClass();

		Damage.ModifierMagnitude = CustomMMC;
		Damage.ModifierOp = EGameplayModOp::Additive;
		Damage.Attribute = UWarlockAttributes::GetDamageAttribute();
	}
};

UCLASS()
class WARLOCKS_API UWarlockHazardFloorDotEffect : public UWarlockGameplayEffectContainer
{
	GENERATED_BODY()
public:
	UWarlockHazardFloorDotEffect()
	{
		DurationPolicy = EGameplayEffectDurationType::Infinite;
		Period = FScalableFloat(1.0f); // Default value, can be modified later
		bExecutePeriodicEffectOnApplication = false;
		OngoingTagRequirements.IgnoreTags.AddTag(FWarlockGameplayTagCache::Get()->ImmunityHazardFloor);

		Modifiers.SetNum(1);
		FGameplayModifierInfo& Damage = Modifiers[0];

		FCustomCalculationBasedFloat CustomMMC;
		CustomMMC.CalculationClassMagnitude = UWarlockDamageModMagnitudeCalculation::StaticClass();

		Damage.ModifierMagnitude = CustomMMC;
		Damage.ModifierOp = EGameplayModOp::Additive;
		Damage.Attribute = UWarlockAttributes::GetDamageAttribute();
	}
};

UCLASS()
class WARLOCKS_API UWarlockTagApplyEffect : public UWarlockGameplayEffectContainer
{
	GENERATED_BODY()
public:
	UWarlockTagApplyEffect()
	{
		DurationPolicy = EGameplayEffectDurationType::Infinite;
		// Granted Tag will be set in the Spec before application instead of here.
	}
};
