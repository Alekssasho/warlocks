// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Styling/SlateBrush.h"
#include "GameplayTagContainer.h"
#include "Misc/Guid.h"
#include "AttributeSet.h"
#include "GameplayAbilities/Public/GameplayCueInterface.h"
#include "Warlocks/Abilities/Effects/WarlockEffectContainer.h"

#include "Warlocks/CommonTypes.h"

#include "AbilityDescriptor.generated.h"

USTRUCT(BlueprintType)
struct FAbilityDescriptorData
{
	GENERATED_BODY()

public:
	FAbilityDescriptorData()
	{}

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Damage")
	FScalableFloat Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Damage")
	FScalableFloat KnockbackForce;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Duration")
	TSubclassOf<class UGameplayEffect> SelfBuffToApply;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Duration")
	TSubclassOf<UWarlockGameplayEffectContainer> ProjectileHitEffectToApply;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Floor")
	TSubclassOf<class ABaseFloor> ModifyFloorActorToSpawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Duration")
	FScalableFloat EffectDuration;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Projectile")
	TSubclassOf<class AWarlockProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Projectile")
	bool bIs2DProjectile;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Aura")
	FGameplayTag AuraTagToApply;

	// TODO: This is not the correct way to do it, however we should change the whole infrastructure
	// with overrides as this will do for the MVP
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Animation")
	FName SocketOriginOverride;
};

UCLASS(BlueprintType)
class UAbilityDescriptor : public UDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	UAbilityDescriptor();

	/** User-visible short name */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Abilities)
	FText AbilityName;

	/** User-visible long description */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Abilities)
	FText AbilityDescription;

	/** Icon to display */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Abilities)
	FSlateBrush AbilityIcon;

	/** Actual gameplay definition */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Abilities)
	TSubclassOf<class UGameplayAbility> AbilityDefinition;

	// Below will be gameplay related members

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Cooldown")
	FScalableFloat CooldownDuration;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Range")
	FScalableFloat Range;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Animation")
	TObjectPtr<class UAnimMontage> Montage = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Animation")
	FName SocketOrigin;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Animation")
	FGameplayCueTag GameplayCueTagToPlay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Projectile")
	FScalableFloat ProjectileSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Targeting")
	bool bIsInstantTargeting;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Targeting")
	TSubclassOf<class AGameplayAbilityWorldReticle> ReticleClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Targeting")
	TObjectPtr<class UMaterial> TargetingActorMaterial = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Targeting")
	bool bIsWallOrientation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, EditFixedSize)
	TArray<FAbilityDescriptorData> PerSchoolData;
};
