#include "LocationTargetActor.h"

#include <Warlocks/Blueprint/WarlocksBlueprintLibrary.h>

#include "Abilities/GameplayAbility.h"
#include "Components/DecalComponent.h"
#include "Materials/Material.h"
#include "Warlocks/PlayerControlledCharacter.h"
#include "Components/CapsuleComponent.h"

ALocationTargetActor::ALocationTargetActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionCapsule0"));
	CollisionComponent->InitCapsuleSize(0.f, 0.f);
	CollisionComponent->AlwaysLoadOnClient = true;
	CollisionComponent->SetUsingAbsoluteScale(true);
	CollisionComponent->SetCanEverAffectNavigation(false);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	RootComponent = CollisionComponent;

	MaxRange = 0.0f;

	ReticlePosition = EReticlePosition::AttachToCaster;
}

void ALocationTargetActor::SetupActorMesh(AActor* Actor, UMaterial* Material)
{
	// TODO: This potentially can be moved inside the reticle class to avoid polutting here
	// This is taken from AGameplayAbilityWorldReticle_ActorVisualization
	TInlineComponentArray<UMeshComponent*> MeshComps;
	USceneComponent* MyRoot = GetRootComponent();
	Actor->GetComponents(MeshComps);
	check(MyRoot);

	for (UMeshComponent* MeshComp : MeshComps)
	{
		MeshComponent = MeshComp;
		//Special case: If we don't clear the root component explicitly, the component will be destroyed along with the original visualization actor.
		if (MeshComp == Actor->GetRootComponent())
		{
			Actor->SetRootComponent(NULL);
		}

		//Disable collision on visualization mesh parts so it doesn't interfere with aiming or any other client-side collision/prediction/physics stuff
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);		//All mesh components are primitive components, so no cast is needed

		//Move components from one actor to the other, attaching as needed. Hierarchy should not be important, but we can do fixups if it becomes important later.
		MeshComp->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
		MeshComp->AttachToComponent(MyRoot, FAttachmentTransformRules::KeepRelativeTransform);
		MeshComp->SetRelativeLocation(FVector(0.0f, 0.0f, 5.0f));
		MeshComp->Rename(nullptr, this);
		if (Material)
		{
			MeshComp->SetMaterial(0, Material);
		}
	}
}

void ALocationTargetActor::EndPlay(EEndPlayReason::Type Reason)
{
	if (ReticleActor.IsValid())
	{
		ReticleActor->Destroy();
	}
}

void ALocationTargetActor::StartTargeting(UGameplayAbility* InAbility)
{
	Super::StartTargeting(InAbility);
	SourceActor = InAbility->GetCurrentActorInfo()->AvatarActor.Get();

	if (ReticleClass)
	{
		AGameplayAbilityWorldReticle* SpawnedReticleActor = GetWorld()->SpawnActor<AGameplayAbilityWorldReticle>(ReticleClass);
		if (SpawnedReticleActor)
		{
			switch (ReticlePosition)
			{
			case EReticlePosition::AttachToCaster:
				SpawnedReticleActor->AttachToActor(SourceActor, FAttachmentTransformRules::SnapToTargetIncludingScale);
				SpawnedReticleActor->InitializeReticle(this, MasterPC, ReticleParams);
				break;
			case EReticlePosition::AttachToLocation:
				SpawnedReticleActor->AttachToActor(this, FAttachmentTransformRules::SnapToTargetIncludingScale);
				SpawnedReticleActor->InitializeReticle(SourceActor, MasterPC, ReticleParams);
				break;
			}

			ReticleActor = SpawnedReticleActor;
			// This is to catch cases of playing on a listen server where we are using a replicated reticle actor.
			// (In a client controlled player, this would only run on the client and therefor never replicate. If it runs
			// on a listen server, the reticle actor may replicate. We want consistancy between client/listen server players.
			// Just saying 'make the reticle actor non replicated' isnt a good answer, since we want to mix and match reticle
			// actors and there may be other targeting types that want to replicate the same reticle actor class).
			if (!ShouldProduceTargetDataOnServer)
			{
				SpawnedReticleActor->SetReplicates(false);
			}
		}
	}
}

void ALocationTargetActor::Tick(float DeltaSeconds)
{
	if (SourceActor && Provider)
	{
		FVector Location = ClampToRange(Provider->ProvideLocation().Location);
		SetActorLocation(Location);

		if(bWallOrientation)
		{
			SetActorRotation(UWarlocksBlueprintLibrary::OrientWall(SourceActor->GetActorLocation(), Location));
		}
	}
}

void ALocationTargetActor::ConfirmTargetingAndContinue()
{
	if (SourceActor && Provider)
	{
		FHitResult Result = Provider->ProvideLocation();
		if (MaxRange > 0.0f)
		{
			Result.Location = GetActorLocation();
		}
		// TODO: Change Execute interface method to just calling the method
		// once we move AI to C++ and can implement the interface there, as
		// there is a lot of overhead in dealing with Blueprint interface
		FGameplayAbilityTargetDataHandle Handle =
			StartLocation.MakeTargetDataHandleFromHitResult(
				OwningAbility,
				Result
			);
		TargetDataReadyDelegate.Broadcast(Handle);
	}
}

FVector ALocationTargetActor::ClampToRange(FVector DesiredLocation)
{
	if (MaxRange > 0.0f)
	{
		FVector Direction = DesiredLocation - SourceActor->GetActorLocation();
		float Length = Direction.Size();
		if (Length > MaxRange)
		{
			return SourceActor->GetActorLocation() + ((MaxRange / Length) * Direction);
		}
	}

	return DesiredLocation;
}
