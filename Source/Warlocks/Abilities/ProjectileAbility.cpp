// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileAbility.h"
#include "GameplayAbilities/Public/Abilities/Tasks/AbilityTask_WaitTargetData.h"

#include "Warlocks/Abilities/Tasks/PlayWarlockAbilityCastingMontageTask.h"
#include "Warlocks/Abilities/Effects/WarlockEffectContainer.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"
#include "Warlocks/Abilities/LocationTargetActor.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Abilities/SpellCastingUtilities.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/Warlocks.h"

UProjectileAbility::UProjectileAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

void UProjectileAbility::SetupAbilityFromDescriptor(TObjectPtr<const class UAbilityDescriptor> Descriptor)
{
	Super::SetupAbilityFromDescriptor(Descriptor);
}

void UProjectileAbility::ActivateAbility(
	const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* OwnerInfo,
	const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	AActor* Avatar = GetAvatarActorFromActorInfo();
	if (!Avatar->Implements<ULocationProviderInterface>())
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
		UE_LOG(LogWarlocks, Error, TEXT("Avatar cannot provide location for targeting"));
		return;
	}

	ALocationTargetActor* TargetingActor = GetWorld()->SpawnActor<ALocationTargetActor>();
	TargetingActor->Provider = Cast<ILocationProviderInterface>(Avatar);
	TargetingActor->ReticleClass = AbilityDescriptor->bIsInstantTargeting ? nullptr : AbilityDescriptor->ReticleClass;

	UAbilityTask_WaitTargetData* TargetingTask = UAbilityTask_WaitTargetData::WaitTargetDataUsingActor(
		this,
		NAME_None,
		AbilityDescriptor->bIsInstantTargeting ? EGameplayTargetingConfirmation::Instant : EGameplayTargetingConfirmation::UserConfirmed,
		TargetingActor);

	TargetingTask->ValidData.AddDynamic(this, &UProjectileAbility::TargetConfirm);
	TargetingTask->Cancelled.AddDynamic(this, &UProjectileAbility::TargetCancel);
	TargetingTask->ReadyForActivation();
}

void UProjectileAbility::TargetConfirm(const FGameplayAbilityTargetDataHandle& Data)
{
	if (!CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo)) {
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
	}

	// TODO: Check if we can bind it to the delegate and pass it
	// We need to change the delegate to be non dynamic to work properly
	// Probably we don't need it as multicast as well.
	EndLocation = Data.Get(0)->GetHitResult()->Location;

	AWarlocksBaseCharacter* Avatar = Cast<AWarlocksBaseCharacter>(GetAvatarActorFromActorInfo());
	FVector EndVector = EndLocation - Avatar->GetActorLocation();
	FRotator Rotation = EndVector.GetSafeNormal2D().ToOrientationRotator();

	if (MontageToPlay)
	{
		Avatar->StartTurnInPlace(Rotation);

		UPlayWarlockAbilityCastingMontageTask* MontagePlayTask =
			UPlayWarlockAbilityCastingMontageTask::CreatePlayWarlockAbilityCastingMontageTaskProxy(
				this,
				NAME_None,
				MontageToPlay
			);

		MontagePlayTask->OnCancelled.AddDynamic(this, &UProjectileAbility::MontageCancel);
		MontagePlayTask->OnEffectCast.AddDynamic(this, &UProjectileAbility::MontageEffectCast);
		MontagePlayTask->OnCompleted.AddDynamic(this, &UProjectileAbility::MontageCompleted);
		MontagePlayTask->ReadyForActivation();
	}
	else
	{
		Avatar->SetActorRotation(Rotation);
		MontageEffectCast();
	}
}

void UProjectileAbility::MontageEffectCast()
{
	// Spawn only on the Server
	if (GetOwningActorFromActorInfo()->HasAuthority())
	{
		ACharacter* Avatar = Cast<ACharacter>(GetAvatarActorFromActorInfo());
		FName MuzzleSocketName = AbilityDescriptor->SocketOrigin;
		if (!GetSchoolData().SocketOriginOverride.IsNone())
		{
			MuzzleSocketName = GetSchoolData().SocketOriginOverride;
		}
		FVector SpawnLocation = Avatar->GetMesh()->GetSocketLocation(MuzzleSocketName);
		FVector EndVector = EndLocation - SpawnLocation;
		FRotator Rotation = (GetSchoolData().bIs2DProjectile ? EndVector.GetSafeNormal2D() : EndVector.GetSafeNormal()).ToOrientationRotator();
		FTransform Transform(Rotation, SpawnLocation);

		FSpellCastingUtilities::SpawnProjectile(AbilityDescriptor, GetSchoolData(), GetAbilityLevel(), Avatar, Transform);

	}
}

void UProjectileAbility::MontageCompleted()
{
	AWarlocksBaseCharacter* Avatar = Cast<AWarlocksBaseCharacter>(GetAvatarActorFromActorInfo());
	Avatar->StopTurnInPlace();
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UProjectileAbility::MontageCancel()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}

void UProjectileAbility::TargetCancel(const FGameplayAbilityTargetDataHandle& Data)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}