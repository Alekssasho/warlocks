// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlockSlotAbility.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/Blueprint/WarlocksBlueprintLibrary.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"
#include "Warlocks/Inventory/EquippableItem.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/WarlockPlayerState.h"

UWarlockSlotAbility::UWarlockSlotAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Slot abilities should be instanced per actor to properly work
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;

	// Block abilities when we are stunned
	ActivationBlockedTags.AddTag(FWarlockGameplayTagCache::Get()->StunTag);
}

void UWarlockSlotAbility::OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnAvatarSet(ActorInfo, Spec);
	ECharacterSlot Slot = static_cast<ECharacterSlot>(Spec.InputID);

	CooldownTags = FGameplayTagContainer(UWarlocksBlueprintLibrary::GetTagFromSlot(Slot));

	UEquippableItem* SourceItem = Cast<UEquippableItem>(Spec.SourceObject);
	check(SourceItem);

	WarlockAbilitySchool = SourceItem->School;
	WarlockAbilityLevel = SourceItem->Level;

	SetupAbilityFromDescriptor(SourceItem->GrantedAbility);
}

const FGameplayTagContainer* UWarlockSlotAbility::GetCooldownTags() const
{
	return &CooldownTags;
}

void UWarlockSlotAbility::ApplyCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const
{
	FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpec(UWarlockSlotCooldownEffect::StaticClass(), GetAbilityLevel());
	SpecHandle.Data.Get()->DynamicGrantedTags.AppendTags(CooldownTags);
	SpecHandle.Data.Get()->SetSetByCallerMagnitude(
		FWarlockGameplayTagCache::Get()->CooldownTag,
		CooldownDuration.GetValueAtLevel(GetAbilityLevel())
	);

	ApplyGameplayEffectSpecToOwner(Handle, ActorInfo, ActivationInfo, SpecHandle);

	AWarlockPlayerState* PlayerState = Cast<AWarlockPlayerState>(ActorInfo->OwnerActor.Get());
	if (!PlayerState || !PlayerState->HasAuthority())
	{
		return;
	}

	// TODO: It is a hacky solution to put ability cast tracking in this method. Consider a better spot.
	PlayerState->CurrencyManager->IncreaseAbilityCounter_OnServer(WarlockAbilitySchool);
}

void UWarlockSlotAbility::SetupAbilityFromDescriptor(TObjectPtr<const UAbilityDescriptor> Descriptor)
{
	AbilityDescriptor = Descriptor;
	// TODO: This could be not stored as we already have the Descriptor. It is left here for now, to minimize the changes.
	CooldownDuration = Descriptor->CooldownDuration;

	// TODO: Consider moving montage logic into this class as it is mostly copy pasted in all abilities
	MontageToPlay = Descriptor->Montage;
}

const FAbilityDescriptorData& UWarlockSlotAbility::GetSchoolData() const
{
	return AbilityDescriptor->PerSchoolData[uint8_t(WarlockAbilitySchool)];
}
