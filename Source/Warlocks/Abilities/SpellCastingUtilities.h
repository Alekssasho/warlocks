#pragma once

#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/WarlockProjectile.h"
#include "Warlocks/Arena/Floor.h"

class FSpellCastingUtilities
{
public:
	static ABaseFloor* SpawnModifyingFloor(
		const UAbilityDescriptor* Descriptor,
		const FAbilityDescriptorData& SchoolData,
		int32 Level,
		APawn* Avatar,
		const FTransform& Transform);

	static AWarlockProjectile* SpawnProjectile(
		const UAbilityDescriptor* Descriptor,
		const FAbilityDescriptorData& SchoolData,
		int32 Level,
		APawn* Avatar,
		const FTransform& Transform);
};
