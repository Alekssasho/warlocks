// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WarlockSlotAbility.h"
#include "GameplayAbilities/Public/GameplayCueInterface.h"
#include "GroundRadiusEffectAbility.generated.h"

UCLASS()
class WARLOCKS_API UGroundRadiusEffectAbility : public UWarlockSlotAbility
{
	GENERATED_UCLASS_BODY()
public:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* OwnerInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void SetupAbilityFromDescriptor(TObjectPtr<const class UAbilityDescriptor> Descriptor) override;

private:
	UFUNCTION()
	void TargetConfirm(const FGameplayAbilityTargetDataHandle& Data);
	UFUNCTION()
	void TargetCancel(const FGameplayAbilityTargetDataHandle& Data);
	UFUNCTION()
	void MontageCancel();
	UFUNCTION()
	void MontageEffectCast();

	FGameplayCueTag GameplayCueTagToPlay;
	FName SocketOrigin;
	FScalableFloat DamageValueSet;
	FScalableFloat KnockbackForce;
};
