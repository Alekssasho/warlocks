// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WarlockSlotAbility.h"
#include "SelfBuffAbility.generated.h"

UCLASS()
class WARLOCKS_API USelfBuffAbility : public UWarlockSlotAbility
{
	GENERATED_UCLASS_BODY()
public:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* OwnerInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void SetupAbilityFromDescriptor(TObjectPtr<const class UAbilityDescriptor> Descriptor) override;
private:
	TSubclassOf<UGameplayEffect> BuffEffectClass;
	FScalableFloat EffectDuration;

	UFUNCTION()
	void MontageCancel();
	UFUNCTION()
	void MontageEffectCast();
};
