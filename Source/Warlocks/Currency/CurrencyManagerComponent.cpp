// Fill out your copyright notice in the Description page of Project Settings.


#include "CurrencyManagerComponent.h"

#include "Algo/Accumulate.h"

#include "Net/UnrealNetwork.h"

#include "Warlocks/Warlocks.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Systems/PersistentProgression/PersistentProgressionSaveGame.h"

UCurrencyManagerComponent::UCurrencyManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(true);

	RunePower = 0;
}

void UCurrencyManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCurrencyManagerComponent, RunePower);
}

void UCurrencyManagerComponent::GainRunePower_OnServer(int32 GainedRunePower)
{
	check(GetOwner()->HasAuthority());
	RunePower += GainedRunePower;
}

void UCurrencyManagerComponent::SaveRunePower_OnServer(int32 Index)
{
	check(GetOwner()->HasAuthority());

	TSchoolArray<int32> Points{ EInPlace::InPlace, 0 };
	GeneratePersistentPointArray(Points);
	UPersistentProgressionSaveGame::SaveGame(Index, Points);
	ResetAbilityCounters();
}

void UCurrencyManagerComponent::LoadRunePower_OnServer(int32 Index)
{
	check(GetOwner()->HasAuthority());

	TSchoolArray<int32> Points{ EInPlace::InPlace, 0 };
	UPersistentProgressionSaveGame::LoadGame(Index, Points);

	//TODO : do something meaniningful with the loaded points
}

void UCurrencyManagerComponent::IncreaseAbilityCounter_OnServer(EAbilitySchool AbilitySchool)
{
	check(GetOwner()->HasAuthority());

	++AbilityCounter[uint8(AbilitySchool)];
}

void UCurrencyManagerComponent::ServerUpgradeAbilityLevel_Implementation(class UAbilityDescriptor* Ability)
{
	// TODO: What should the cost be?
	// TODO: Actually level up the skill!
	const int32 RunePowerCost = 10;
	if (RunePower < RunePowerCost)
	{
		UE_LOG(LogWarlocks, Display,
			TEXT("Can't upgrade skill <%s> to next level because of insufficient Rune Power! Upgrade costs <%d>, player has <%d>"),
				*Ability->AbilityName.ToString(), RunePowerCost, RunePower);
		return;
	}
	UE_LOG(LogWarlocks, Display, TEXT("Upgrading skill <%s> to next level by spending <%d> Rune Power on it"), *Ability->AbilityName.ToString(), RunePowerCost);
	RunePower -= RunePowerCost;
}

void UCurrencyManagerComponent::ResetAbilityCounters()
{
	FMemory::Memset(AbilityCounter.GetData(), 0, uint8(EAbilitySchool::Count) * sizeof(int32));
}

void UCurrencyManagerComponent::GeneratePersistentPointArray(TSchoolArray<int32>& OutPersistentPoints)
{
	int32 Sum = Algo::Accumulate(AbilityCounter, 0);
	if (Sum == 0)
	{
		FMemory::Memset(OutPersistentPoints.GetData(), 0, uint8(EAbilitySchool::Count) * sizeof(int32));
	}

	for (int i = 0; i < AbilityCounter.Num(); ++i)
	{
		OutPersistentPoints[i] = (static_cast<float>(AbilityCounter[i]) / Sum) * RunePower;
	}
}