// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Warlocks/CommonTypes.h"

#include "CurrencyManagerComponent.generated.h"


UCLASS( ClassGroup=(Warlocks), meta=(BlueprintSpawnableComponent) )
class WARLOCKS_API UCurrencyManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UCurrencyManagerComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Increases the amount of Rune Power available to the player; MUST be called on the server only
	UFUNCTION()
	void GainRunePower_OnServer(int32 GainedRunePower);

	UFUNCTION()
	void SaveRunePower_OnServer(int32 Index);

	UFUNCTION()
	void LoadRunePower_OnServer(int32 Index);

	UFUNCTION()
	void IncreaseAbilityCounter_OnServer(EAbilitySchool AbilitySchool);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerUpgradeAbilityLevel(class UAbilityDescriptor* Ability);

	UFUNCTION(BlueprintPure)
	int32 GetRunePower() const { return RunePower; }

private:
	void ResetAbilityCounters();
	void GeneratePersistentPointArray(TSchoolArray<int32>& OutPersistentPoints);

	// TODO: Should this be uint?
	UPROPERTY(VisibleAnywhere, Replicated)
	int32 RunePower;

	TSchoolArray<int32> AbilityCounter;
};
