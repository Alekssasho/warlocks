#include "WarlocksEngineSubsystem.h"
#include "AbilitySystemGlobals.h"

void UWarlocksEngineSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	UAbilitySystemGlobals::Get().InitGlobalData();
}
