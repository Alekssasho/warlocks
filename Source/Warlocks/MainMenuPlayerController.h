// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainMenuPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AMainMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AMainMenuPlayerController();
	// HUD management
	// Always spawn our HUD as default
	virtual void SpawnDefaultHUD() override;

	void InitFromPlayerState_OnClient();
};
