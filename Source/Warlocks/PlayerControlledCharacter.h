// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "WarlocksBaseCharacter.h"
#include "Abilities/LocationProviderInterface.h"
#include "PlayerControlledCharacter.generated.h"

UCLASS(Blueprintable)
class APlayerControlledCharacter : public AWarlocksBaseCharacter, public ILocationProviderInterface
{
	GENERATED_BODY()

public:
	APlayerControlledCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	virtual void BeginPlay() override;

	// Location Provider
	virtual FHitResult ProvideLocation() override;

	// Can be called on both client and server to initialize the Character from the replicated data in the PlayerState
	void InitFromPlayerState(class AWarlockPlayerState* InPlayerState);
	void BindAbilitySystemInput();
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UCameraComponent> TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class USpringArmComponent> CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UDecalComponent> CursorToWorld;

	FHitResult CurrentHitResult;

	bool bIsInputBound = false;

	virtual void OnDeath() override;
	virtual void OnStunStarted() override;
	virtual void OnStunEnded() override;
};

