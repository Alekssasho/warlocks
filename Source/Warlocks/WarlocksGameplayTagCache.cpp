#include "Warlocks/WarlocksGameplayTagCache.h"

FWarlockGameplayTagCache::FWarlockGameplayTagCache()
{
	DamageAmountTag = FGameplayTag::RequestGameplayTag("Data.Damage.Amount");
	StunChanceTag = FGameplayTag::RequestGameplayTag("Data.Stun.Chance");
	CooldownTag = FGameplayTag::RequestGameplayTag("Data.Cooldown");
	DurationTag = FGameplayTag::RequestGameplayTag("Data.Duration");
	ImmunityHazardFloor = FGameplayTag::RequestGameplayTag("Immunity.HazardFloor");
	ImmunityKnockback = FGameplayTag::RequestGameplayTag("Immunity.Knockback");
	KnockbackTag = FGameplayTag::RequestGameplayTag("Knockback");
	FloorSlide = FGameplayTag::RequestGameplayTag("Floor.Slide");
	ModificationCharacterSlow = FGameplayTag::RequestGameplayTag("Modification.Character.Slow");
	ModificationCharacterFast = FGameplayTag::RequestGameplayTag("Modification.Character.Fast");
	ModificationCharacterInvisible = FGameplayTag::RequestGameplayTag("Modification.Character.Invisible");
	StunTag = FGameplayTag::RequestGameplayTag("State.Character.Stun");
	EnhanceDamage = FGameplayTag::RequestGameplayTag("Aura.EnhanceDamage");
}

FWarlockGameplayTagCache* FWarlockGameplayTagCache::Get()
{
	static FWarlockGameplayTagCache Cache;
	return &Cache;
}
