// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Components/ActorComponent.h"

#include "WarlocksCharacterEffectApplicator.generated.h"

class UMaterialInterface;

UCLASS(ClassGroup = (Warlocks), meta = (BlueprintSpawnableComponent))
class WARLOCKS_API UCharacterEffectApplicatorComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	UCharacterEffectApplicatorComponent();

	void Setup();
	void RemoveDelegates();

private:
	class AWarlocksBaseCharacter* GetCharacter();

	UFUNCTION(Client, Reliable)
	void ClientSetGroundFriction(float GroundFriction);
	void FloorSlideChangedServer(const FGameplayTag Tag, int32 NewCount);

	UFUNCTION(Client, Reliable)
	void ClientSetMaxWalkSpeed(float MaxWalkSpeed);

	void CharacterSpeedChangedServer(const FGameplayTag Tag, int32 NewCount);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastCharacterToggleInvisible(bool ShouldBeInvisible);
	void CharacterInvisibleChangedServer(const FGameplayTag Tag, int32 NewCount);

	float DefaultGroundFriction = 0.0f;
	FDelegateHandle FloorSlideDelegateHandle;

	float DefaultMaxWalkSpeed = 0.0f;
	FDelegateHandle CharacterSlowDelegateHandle;
	FDelegateHandle CharacterFastDelegateHandle;

	FDelegateHandle CharacterInvisibleDelegateHandle;

	// Material to apply when you are the one to become invisible
	TObjectPtr<UMaterialInterface> InvisibleLocalControlledMaterial;
	// Material to apply when someone else is becoming invisible
	TObjectPtr<UMaterialInterface> InvisibleRemoteControlledMaterial;
};
