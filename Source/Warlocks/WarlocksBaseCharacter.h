// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameplayAbilities/Public/AbilitySystemInterface.h"

#include "Warlocks/CommonTypes.h"
#include "WarlockAttributes.h"
#include "Inventory/InventoryComponent.h"
#include "WarlocksBaseCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWarlockBaseCharacterHitReactDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWarlockBaseCharacterTargetRotationAbilityDelegate, float, Degrees);

UCLASS(Blueprintable)
class AWarlocksBaseCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AWarlocksBaseCharacter();

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override { return AbilitySystem.Get(); }

	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION(BlueprintCallable)
	bool GetCooldownInfoForSlot(ECharacterSlot Slot, float& Duration, float& RemainingTime) const;
	UFUNCTION(BlueprintCallable)
	bool IsSlotOnCooldown(ECharacterSlot Slot) const;

	void GiveAbilities();

	UFUNCTION(BlueprintCallable)
	void EquipItemSet(class UItemSet* Set);

	UFUNCTION(BlueprintCallable)
	float GetHealth();

	/** Stores slotted abilities usable by the character
	* This array always has a length equal to the number of CharacterSlots. Unslotted abilities are null
	* TODO: This needs to be refactored. It should be readonly for everyone, other than inventory component which can slot abilities.
	*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	TArray<TObjectPtr<const class UAbilityDescriptor>> SlottedAbilities;

	// Delegates
	UPROPERTY(BlueprintAssignable, Category = "WarlockCharacter|Delegates")
	FWarlockBaseCharacterHitReactDelegate ShowHitReact;
	UPROPERTY(BlueprintAssignable, Category = "WarlockCharacter|Delegates")
	FWarlockBaseCharacterTargetRotationAbilityDelegate TargetAbilityCasted;

	UFUNCTION(NetMulticast, Reliable)
	void HitReact();

	UFUNCTION(NetMulticast, Reliable)
	void Die();

	UFUNCTION(NetMulticast, Reliable)
	void Stun();

	// Death Animation
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "WarlockCharacter|Conditions")
	TObjectPtr<UAnimMontage> DeathMontage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "WarlockCharacter|Conditions")
	TObjectPtr<UAnimMontage> StunMontage;

	UFUNCTION(BlueprintCallable)
	void StartTurnInPlace(FRotator DesiredActorRotator);
	UFUNCTION(BlueprintCallable)
	void StopTurnInPlace();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastTargetRotationToNonOwningClients(float DesiredRotation);
protected:
	// Gameplay abilities
	TWeakObjectPtr<UAbilitySystemComponent> AbilitySystem;
	TWeakObjectPtr<UWarlockAttributes> Attributes;

	// Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UInventoryComponent> Inventory;

	virtual void OnDeath() {};
	virtual void OnStunStarted() {};
	virtual void OnStunEnded() {};

	void FinishDying();

	void OnDeathMontageEnded(class UAnimMontage* Montage, bool bInterrupted);
	void OnStunMontageEnded(class UAnimMontage* Montage, bool bInterrupted);

	UPROPERTY()
	TObjectPtr<class UCharacterEffectApplicatorComponent> EffectApplicator;
};
