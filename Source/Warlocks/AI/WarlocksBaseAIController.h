// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "WarlocksBaseAIController.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AWarlocksBaseAIController : public AAIController
{
	GENERATED_BODY()

public:
	AWarlocksBaseAIController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Brain)
	TObjectPtr<UBehaviorTree> BehaviorTreeToRun;

	virtual void BeginPlay() override;
};
