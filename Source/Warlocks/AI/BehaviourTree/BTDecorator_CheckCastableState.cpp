// Fill out your copyright notice in the Description page of Project Settings.


#include "BTDecorator_CheckCastableState.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Enum.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

#include "Warlocks/AI/WarlocksBaseAIController.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/Abilities/WarlockSlotAbility.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"


UBTDecorator_CheckCastableState::UBTDecorator_CheckCastableState()
{
	NodeName = "Check Ability Castable State";

	AbilityBlackboardKey.AddEnumFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_CheckCastableState, AbilityBlackboardKey), StaticEnum<ECharacterSlot>());
	AbilityTargetActorBlackboardKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_CheckCastableState, AbilityTargetActorBlackboardKey), AActor::StaticClass());
}

void UBTDecorator_CheckCastableState::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BBAsset = GetBlackboardAsset();
	if (BBAsset)
	{
		AbilityBlackboardKey.ResolveSelectedKey(*BBAsset);
		AbilityTargetActorBlackboardKey.ResolveSelectedKey(*BBAsset);
	}
	else
	{
		UE_LOG(LogBehaviorTree, Warning, TEXT("Can't initialize task: %s, make sure that behavior tree specifies blackboard asset!"), *GetName());
	}
}

EAbilityCastableState UBTDecorator_CheckCastableState::ComputeCastableState(const UBehaviorTreeComponent& OwnerComp) const
{
	auto Controller = Cast<AWarlocksBaseAIController>(OwnerComp.GetOwner());
	auto Caster = Controller->GetPawn<AWarlocksBaseCharacter>();
	check(IsValid(Caster));
	const UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComp));

	const UObject* BBTargetObject = BlackboardComp->GetValue<UBlackboardKeyType_Object>(AbilityTargetActorBlackboardKey.GetSelectedKeyID());
	const AActor* TargetActor = Cast<AActor>(BBTargetObject);

	uint8 BBTargetAbilitySlotInt = BlackboardComp->GetValue<UBlackboardKeyType_Enum>(AbilityBlackboardKey.GetSelectedKeyID());
	ECharacterSlot AbilitySlot = static_cast<ECharacterSlot>(BBTargetAbilitySlotInt);

	const UAbilityDescriptor* Descriptor = Caster->SlottedAbilities[BBTargetAbilitySlotInt];
	if (!IsValid(Descriptor))
	{
		FText SlotName = StaticEnum<ECharacterSlot>()->GetDisplayNameTextByValue(BBTargetAbilitySlotInt);
		UE_LOG(LogBehaviorTree, Error, TEXT("An attempt was made to activate a slot at which no ability is assigned! Slot: <%s>"), *SlotName.ToString());
		return EAbilityCastableState::InvalidSetup;
	}
	const UGameplayAbility* AbilityDefinitionDefault = Caster->SlottedAbilities[BBTargetAbilitySlotInt]->AbilityDefinition.GetDefaultObject();
	const UWarlockSlotAbility* SlottedAbility = Cast<UWarlockSlotAbility>(AbilityDefinitionDefault);
	float AbilityRange = SlottedAbility->AbilityDescriptor->Range.GetValueAtLevel(SlottedAbility->GetAbilityLevel());

	EAbilityCastableState AbilityState;
	if (Caster->GetHorizontalDistanceTo(TargetActor) > AbilityRange)
	{
		AbilityState = EAbilityCastableState::OutOfRange;
	}
	else if (Caster->IsSlotOnCooldown(AbilitySlot))
	{
		AbilityState = EAbilityCastableState::OnCooldown;
	}
	else
	{
		AbilityState = EAbilityCastableState::Castable;
	}
	return AbilityState;
}

bool UBTDecorator_CheckCastableState::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComp));
	EAbilityCastableState CastableState = ComputeCastableState(OwnerComp);
	uint8 CastableStateIntValue = static_cast<uint8>(CastableState);

	// Check if their equal because the Success state has a value of 0 so the bitand will fail
	return CastableState == ExpectedCastableState || (CastableStateIntValue & static_cast<uint8>(ExpectedCastableState));
}