// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Warlocks/CommonTypes.h"
#include "BTTask_FocusNearestPracticeDummy.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UBTTask_FocusNearestPracticeDummy : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_FocusNearestPracticeDummy();
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Blackboard key selector for the ability to be cast */
	UPROPERTY(EditAnywhere)
	ECharacterSlot AbilitySlotToUse;
};
