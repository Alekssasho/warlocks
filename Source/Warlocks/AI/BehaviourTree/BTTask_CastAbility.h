// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_CastAbility.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UBTTask_CastAbility : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_CastAbility();
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	/** Blackboard key selector for the ability to be cast */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector AbilityBlackboardKey;

	/** Blackboard key selector for the target location at which the ability should be cast*/
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector TargetLocationBlackboardKey;
};
