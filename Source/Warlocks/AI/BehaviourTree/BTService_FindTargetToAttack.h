// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"

#include "Warlocks/CommonTypes.h"

#include "BTService_FindTargetToAttack.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UBTService_FindTargetToAttack : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTService_FindTargetToAttack();
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	/** Blackboard key selector to be set for the target location at which the ability should be cast*/
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector TargetActorBlackboardKey;
	/** Blackboard key selector to be set for the ability to be cast */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector TargetAbilityBlackboardKey;
	/** Blackboard key selector to be set for the target location at which the ability should be cast*/
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector TargetLocationBlackboardKey;

private:
	TOptional<ECharacterSlot> FindBestAbilityForTarget(const class AWarlocksBaseCharacter* Caster, const class AWarlocksBaseCharacter* Target) const;
};
