// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_CastAbility.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Enum.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"

#include "Warlocks/AI/Tasks/AITask_CastAbility.h"

UBTTask_CastAbility::UBTTask_CastAbility()
{
	NodeName = "Cast Ability";
	bNotifyTick = false;
	bNotifyTaskFinished = false;

	AbilityBlackboardKey.AddEnumFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_CastAbility, AbilityBlackboardKey), StaticEnum<ECharacterSlot>());
	TargetLocationBlackboardKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_CastAbility, TargetLocationBlackboardKey));
}

void UBTTask_CastAbility::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BBAsset = GetBlackboardAsset();
	if (BBAsset)
	{
		AbilityBlackboardKey.ResolveSelectedKey(*BBAsset);
		TargetLocationBlackboardKey.ResolveSelectedKey(*BBAsset);
	}
	else
	{
		UE_LOG(LogBehaviorTree, Warning, TEXT("Can't initialize task: %s, make sure that behavior tree specifies blackboard asset!"), *GetName());
	}
}


EBTNodeResult::Type UBTTask_CastAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComp));
	auto CastTask = NewBTAITask<UAITask_CastAbility>(OwnerComp);

	uint8 AbilityEnumValue = BlackboardComp->GetValue<UBlackboardKeyType_Enum>(AbilityBlackboardKey.GetSelectedKeyID());
	CastTask->AbilitySlot = static_cast<ECharacterSlot>(AbilityEnumValue);
	CastTask->TargetLocation = BlackboardComp->GetValue<UBlackboardKeyType_Vector>(TargetLocationBlackboardKey.GetSelectedKeyID());

	CastTask->ReadyForActivation();
	EBTNodeResult::Type Result = (CastTask->GetState() == EGameplayTaskState::Finished) ? EBTNodeResult::Succeeded : EBTNodeResult::Failed;

	return Result;
}