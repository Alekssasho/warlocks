// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_FocusNearestPracticeDummy.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Enum.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"
#include "Engine/Public/EngineUtils.h"
#include "Engine/StaticMeshActor.h"

#include "Warlocks/AI/Tasks/AITask_CastAbility.h"
#include "Warlocks/AI/WarlocksBaseAIController.h"
#include "Warlocks/WarlocksBaseCharacter.h"

UBTTask_FocusNearestPracticeDummy::UBTTask_FocusNearestPracticeDummy()
{
	NodeName = "Focus Nearest Practice Dummy";
	bNotifyTick = false;
	bNotifyTaskFinished = false;
}

EBTNodeResult::Type UBTTask_FocusNearestPracticeDummy::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto Controller = Cast<AWarlocksBaseAIController>(OwnerComp.GetOwner());
	auto Caster = Controller->GetPawn<AWarlocksBaseCharacter>();
	check(IsValid(Caster));

	if (Caster->IsSlotOnCooldown(AbilitySlotToUse))
	{
		// Avoid spamming errors
		return EBTNodeResult::Failed;
	}

	AStaticMeshActor* TargetDummy = nullptr;
	float SqDistanceToTarget = MAX_FLT;

	for (AStaticMeshActor* Dummy : TActorRange<AStaticMeshActor>(GetWorld()))
	{
		if (Dummy->ActorHasTag(FName(TEXT("PracticeDummy"))) && (!TargetDummy || SqDistanceToTarget > Dummy->GetSquaredDistanceTo(Caster)))
		{
			TargetDummy = Dummy;
			SqDistanceToTarget = Dummy->GetSquaredDistanceTo(Caster);
		}
	}

	auto CastTask = NewBTAITask<UAITask_CastAbility>(OwnerComp);

	CastTask->AbilitySlot = static_cast<ECharacterSlot>(AbilitySlotToUse);
	CastTask->TargetLocation = TargetDummy->GetActorLocation();

	CastTask->ReadyForActivation();
	EBTNodeResult::Type Result = (CastTask->GetState() == EGameplayTaskState::Finished) ? EBTNodeResult::Succeeded : EBTNodeResult::Failed;

	return Result;
}