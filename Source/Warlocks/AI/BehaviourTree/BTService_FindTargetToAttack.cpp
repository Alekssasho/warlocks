// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_FindTargetToAttack.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Enum.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "Engine/Classes/GameFramework/PlayerController.h"

#include "Warlocks/AI/WarlocksBaseAIController.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"


UBTService_FindTargetToAttack::UBTService_FindTargetToAttack()
{
	NodeName = "Find Best Target To Attack and Ability To Use";

	TargetActorBlackboardKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_FindTargetToAttack, TargetActorBlackboardKey), AActor::StaticClass());
	TargetAbilityBlackboardKey.AddEnumFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_FindTargetToAttack, TargetAbilityBlackboardKey), StaticEnum<ECharacterSlot>());
	TargetLocationBlackboardKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_FindTargetToAttack, TargetLocationBlackboardKey));
}

void UBTService_FindTargetToAttack::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BBAsset = GetBlackboardAsset();
	if (BBAsset)
	{
		TargetActorBlackboardKey.ResolveSelectedKey(*BBAsset);
		TargetAbilityBlackboardKey.ResolveSelectedKey(*BBAsset);
		TargetLocationBlackboardKey.ResolveSelectedKey(*BBAsset);
	}
	else
	{
		UE_LOG(LogBehaviorTree, Warning, TEXT("Can't initialize task: %s, make sure that behavior tree specifies blackboard asset!"), *GetName());
	}
}

void UBTService_FindTargetToAttack::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	auto Controller = Cast<AWarlocksBaseAIController>(OwnerComp.GetOwner());
	auto Caster = Controller->GetPawn<AWarlocksBaseCharacter>();
	check(IsValid(Caster));
	UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
	check(IsValid(BlackboardComp));

	// TODO: Run an EQS query
	AController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!IsValid(PlayerController))
	{
		// The PC is null in the editor if ejected
		return;
	}
	auto PlayerCharacter = PlayerController->GetPawn<AWarlocksBaseCharacter>();
	if (!IsValid(PlayerCharacter))
	{
		// Player is probably dead
		return;
	}

	BlackboardComp->SetValue<UBlackboardKeyType_Object>(TargetActorBlackboardKey.GetSelectedKeyID(), PlayerCharacter);
	BlackboardComp->SetValue<UBlackboardKeyType_Vector>(TargetLocationBlackboardKey.GetSelectedKeyID(), PlayerCharacter->GetActorLocation());
	TOptional<ECharacterSlot> AbilitySlot = FindBestAbilityForTarget(Caster, PlayerCharacter);
	if (!AbilitySlot.IsSet())
	{
		UE_LOG(LogBehaviorTree, Error, TEXT("Caster doesn't have any abilities slotted! Can't attack! Caster: <%s>"), *AActor::GetDebugName(Caster));
		return;
	}
	int32 EnumIntValue = static_cast<int32>(AbilitySlot.GetValue());
	BlackboardComp->SetValue<UBlackboardKeyType_Enum>(TargetAbilityBlackboardKey.GetSelectedKeyID(), EnumIntValue);
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}


TOptional<ECharacterSlot> UBTService_FindTargetToAttack::FindBestAbilityForTarget(const AWarlocksBaseCharacter* Caster, const AWarlocksBaseCharacter* Target) const
{
	TArray<ECharacterSlot, TInlineAllocator<16>> AvailableAbilities;
	for (int i = 0; i < Target->SlottedAbilities.Num(); i++)
	{
		const UAbilityDescriptor* Descriptor = Target->SlottedAbilities[i];
		if (IsValid(Descriptor))
		{
			AvailableAbilities.Add(static_cast<ECharacterSlot>(i));
		}
	}
	if (AvailableAbilities.Num() == 0)
	{
		return TOptional<ECharacterSlot>();
	}
	int RandomIdx = FMath::RandRange(0, AvailableAbilities.Num() - 1);
	return AvailableAbilities[RandomIdx];
}