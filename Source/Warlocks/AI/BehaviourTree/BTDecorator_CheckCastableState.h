// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "Warlocks/CommonTypes.h"
#include "BTDecorator_CheckCastableState.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UBTDecorator_CheckCastableState : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDecorator_CheckCastableState();
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	/** Blackboard key selector for the ability slot to be checked if castable */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector AbilityBlackboardKey;
	/** Blackboard key selector to be set for the target actor towards whom the ability is to be cast */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector AbilityTargetActorBlackboardKey;
	/** Blackboard key selector for the selected values */
	UPROPERTY(EditAnywhere, Category = Blackboard, meta=(Bitmask))
	EAbilityCastableState ExpectedCastableState;

private:
	EAbilityCastableState ComputeCastableState(const UBehaviorTreeComponent& OwnerComp) const;
};
