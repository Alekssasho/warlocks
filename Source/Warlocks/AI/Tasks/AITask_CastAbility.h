// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tasks/AITask.h"
#include "Warlocks/CommonTypes.h"
#include "AITask_CastAbility.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API UAITask_CastAbility : public UAITask
{
	GENERATED_BODY()
	
public:
	ECharacterSlot AbilitySlot;
	FVector TargetLocation;

protected:
	virtual void Activate() override;
};
