// Fill out your copyright notice in the Description page of Project Settings.


#include "AITask_CastAbility.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "Warlocks/AI/WarlocksBaseAIController.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"


void UAITask_CastAbility::Activate()
{
	check(IsValid(GetAIController()));
	auto Caster = GetAIController()->GetPawn<AWarlocksBaseCharacter>();
	check(IsValid(Caster));


    UBlackboardComponent* BlackboardComp = GetAIController()->FindComponentByClass<UBlackboardComponent>();
    check(BlackboardComp);
    FHitResult Result;
    // TODO: This is a very fragile implementation relying on a string as the key, refactor *somehow*
    BlackboardComp->SetValueAsVector(FName("TargetLocation"), TargetLocation);

	UAbilitySystemComponent* AbilityComp = Caster->GetAbilitySystemComponent();
	check(Caster->SlottedAbilities.IsValidIndex(static_cast<int>(AbilitySlot)));
	const UAbilityDescriptor* AbilityDescriptor = Caster->SlottedAbilities[static_cast<int>(AbilitySlot)];
	check(IsValid(AbilityDescriptor));
	const bool bWasActivationSuccessful = AbilityComp->TryActivateAbilityByClass(AbilityDescriptor->AbilityDefinition);
	if (!bWasActivationSuccessful)
	{
		UE_LOG(LogGameplayTasks, Error, TEXT("Failed to cast ability %s"), *AbilityDescriptor->AbilityName.ToString());
	}
	AbilityComp->TargetConfirm();
}