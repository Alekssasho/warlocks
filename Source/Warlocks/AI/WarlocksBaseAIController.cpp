// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlocksBaseAIController.h"
#include "AIModule/Classes/BrainComponent.h"

AWarlocksBaseAIController::AWarlocksBaseAIController()
{
	bSetControlRotationFromPawnOrientation = false;
}

void AWarlocksBaseAIController::BeginPlay()
{
	Super::BeginPlay();

	check(BehaviorTreeToRun);
	RunBehaviorTree(BehaviorTreeToRun);
}
