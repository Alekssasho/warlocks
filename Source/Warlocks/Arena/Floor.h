// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Floor.generated.h"

UCLASS()
class WARLOCKS_API ABaseFloor : public AActor
{
	GENERATED_BODY()

public:
	ABaseFloor();

private:
	UFUNCTION()
	void OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	void OnOverlapEnd(AActor* OverlappedActor, AActor* OtherActor);
};

