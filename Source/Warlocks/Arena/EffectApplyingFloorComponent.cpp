// Fill out your copyright notice in the Description page of Project Settings.


#include "EffectApplyingFloorComponent.h"
#include "AbilitySystemInterface.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"

void UEffectApplyingFloorComponent::OnOverlapBegin(AActor* OtherActor)
{
	// Don't do anything on the clients
	if (!GetOwner()->HasAuthority())
	{
		return;
	}

	IAbilitySystemInterface* Interface = Cast<IAbilitySystemInterface>(OtherActor);
	// If the other actor doesn't have AbilitySystemInterface just return
	if (!Interface)
	{
		return;
	}

	UAbilitySystemComponent* AbilitySystem = Interface->GetAbilitySystemComponent();
	// This should be a check/assert, but there are some times
	// if a Pawn is spawned on a HazardFloor, where the AbilitySystem is not
	// setup and this prevents a crash.
	if (!AbilitySystem)
	{
		return;
	}

	float Level = 1;
	TArray<FGameplayEffectSpec> Specs;
	for (TSubclassOf<UWarlockGameplayEffectContainer>& Effect : GameplayEffectsToApply)
	{
		UWarlockGameplayEffectContainer* DefaultObject = Effect.GetDefaultObject();
		FGameplayEffectSpec Spec(DefaultObject, AbilitySystem->MakeEffectContext(), Level);
		DefaultObject->ApplyToSpec(&Spec);
		Specs.Push(Spec);
	}

	PrepareGameplayEffectSpecForApplication(Specs);

	TArray<FActiveGameplayEffectHandle> Handles;
	for (FGameplayEffectSpec& Spec : Specs)
	{
		FActiveGameplayEffectHandle Handle = AbilitySystem->ApplyGameplayEffectSpecToSelf(Spec);
		// Infinity duration means we have to remove the effect on overlap end with this actor
		if (Spec.Def->DurationPolicy == EGameplayEffectDurationType::Infinite)
		{
			Handles.Push(Handle);
		}
	}

	ActiveInfiniteDurationEffects.Add(OtherActor, FInfiniteDurationEffects{ Handles });
}

void UEffectApplyingFloorComponent::OnOverlapEnd(AActor* OtherActor)
{
	// Don't do anything on the clients
	if (!GetOwner()->HasAuthority())
	{
		return;
	}

	IAbilitySystemInterface* Interface = Cast<IAbilitySystemInterface>(OtherActor);
	// If the other actor doesn't have AbilitySystemInterface just return
	if (!Interface)
	{
		return;
	}

	UAbilitySystemComponent* AbilitySystem = Interface->GetAbilitySystemComponent();
	// This should be a check/assert, but there are some times
	// if a Pawn is spawned on a HazardFloor, where the AbilitySystem is not
	// setup and this prevents a crash.
	if (!AbilitySystem)
	{
		return;
	}

	// Try to find the active effect which will be only present in the map if it has infinite duration which is indicator we need to remove it on overlap end.
	if (ActiveInfiniteDurationEffects.Find(OtherActor))
	{
		FInfiniteDurationEffects Handles = ActiveInfiniteDurationEffects.FindAndRemoveChecked(OtherActor);
		for (FActiveGameplayEffectHandle Handle : Handles.Handles)
		{
			AbilitySystem->RemoveActiveGameplayEffect(Handle);
		}
	}
}
