// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "FloorComponent.generated.h"

UCLASS()
class WARLOCKS_API UFloorComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	UFloorComponent();
	virtual void OnOverlapBegin(AActor* OtherActor) {};
	virtual void OnOverlapEnd(AActor* OtherActor) {};
};
