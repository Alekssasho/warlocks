// Fill out your copyright notice in the Description page of Project Settings.


#include "ForcePullFloorComponent.h"

#include <Warlocks/WarlocksBaseCharacter.h>
#include <Warlocks/WarlockProjectile.h>

#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/ProjectileMovementComponent.h>

UForcePullFloorComponent::UForcePullFloorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UForcePullFloorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	for (AActor* Actor : ProjectileActorsToApplyForce)
	{
		FVector ForceVector = GetOwner()->GetActorLocation() - Actor->GetActorLocation();
		ForceVector = ForceVector.GetUnsafeNormal2D();
		ForceVector *= ProjectileForceStrength.GetValueAtLevel(0);

		if (AWarlockProjectile* Projectile = Cast<AWarlockProjectile>(Actor))
		{
			Projectile->GetMovement()->AddForce(ForceVector);
		}
		else if (ACharacter* Character = Cast<ACharacter>(Actor))
		{
			Character->GetCharacterMovement()->AddForce(ForceVector);
		}
	}
}

void UForcePullFloorComponent::OnOverlapBegin(AActor* OtherActor)
{
	AActor* Owner = GetOwner();
	if (AWarlockProjectile* Projectile = Cast<AWarlockProjectile>(OtherActor))
	{
		// Don't do anything on the clients
		if (!Owner->HasAuthority())
		{
			return;
		}

		ProjectileActorsToApplyForce.Add(OtherActor);
	}
	else if (ACharacter* Character = Cast<ACharacter>(OtherActor))
	{
		FVector Origin;
		FVector Extent;
		GetOwner()->GetActorBounds(true, Origin, Extent);

		// TODO: Probably this needs to be some kind of data asset
		TSharedPtr<FRootMotionSource_RadialForce> RadialForce = MakeShared<FRootMotionSource_RadialForce>();
		RadialForce->InstanceName = "Force Pull Floor Component";
		RadialForce->AccumulateMode = ERootMotionAccumulateMode::Additive;
		RadialForce->Priority = 5;
		RadialForce->Location = Owner->GetActorLocation();
		RadialForce->LocationActor = Owner;
		RadialForce->Duration = -1.0f; // Needs to be < 0 in order to te infinite, as we remove it manually
		RadialForce->Radius = Extent.GetMax();
		RadialForce->Strength = CharacterForceStrength.GetValueAtLevel(0);
		RadialForce->bIsPush = false;
		RadialForce->bNoZForce = true;
		RadialForce->StrengthDistanceFalloff = nullptr;
		RadialForce->StrengthOverTime = nullptr;
		RadialForce->bUseFixedWorldDirection = false;
		RadialForce->FixedWorldDirection = FRotator::ZeroRotator;
		RadialForce->FinishVelocityParams.Mode = ERootMotionFinishVelocityMode::SetVelocity;
		RadialForce->FinishVelocityParams.SetVelocity = FVector::ZeroVector;
		RadialForce->FinishVelocityParams.ClampVelocity = 0.0;

		uint16 RootMotionSourceID = Character->GetCharacterMovement()->ApplyRootMotionSource(RadialForce);
		CharacterActorToRootMotionId.Add(OtherActor, RootMotionSourceID);
	}
}

void UForcePullFloorComponent::OnOverlapEnd(AActor* OtherActor)
{
	if (AWarlockProjectile* Projectile = Cast<AWarlockProjectile>(OtherActor))
	{
		// Don't do anything on the clients
		if (!GetOwner()->HasAuthority())
		{
			return;
		}

		ProjectileActorsToApplyForce.Remove(OtherActor);
	}
	else if (ACharacter* Character = Cast<ACharacter>(OtherActor))
	{
		uint16 RootMotionSourceId;
		CharacterActorToRootMotionId.RemoveAndCopyValue(OtherActor, RootMotionSourceId);
		Character->GetCharacterMovement()->RemoveRootMotionSourceByID(RootMotionSourceId);
	}
}
