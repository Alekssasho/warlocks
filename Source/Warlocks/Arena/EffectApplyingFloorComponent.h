// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <Warlocks/Abilities/Effects/WarlockEffectContainer.h>
#include <Warlocks/Arena/FloorComponent.h>

#include "GameplayAbilities/Public/GameplayEffect.h"
#include "EffectApplyingFloorComponent.generated.h"

USTRUCT()
struct FInfiniteDurationEffects
{
	GENERATED_BODY()

	TArray<FActiveGameplayEffectHandle> Handles;
};

// TODO: Maybe if this actor dies when still applying an effect on actor, we need to remove it manually as OnOverlapEnd could probably not be called ?
UCLASS(ClassGroup=(Warlocks), meta=(BlueprintSpawnableComponent))
class WARLOCKS_API UEffectApplyingFloorComponent : public UFloorComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TSubclassOf<UWarlockGameplayEffectContainer>> GameplayEffectsToApply;
private:
	virtual void OnOverlapBegin(AActor* OtherActor) final;
	virtual void OnOverlapEnd(AActor* OtherActor) final;

	UPROPERTY()
	TMap<AActor*, FInfiniteDurationEffects> ActiveInfiniteDurationEffects;
protected:
	// This could be overloaded by child classes to modify the spec before it being applied to the target
	virtual void PrepareGameplayEffectSpecForApplication(TArray<FGameplayEffectSpec>& Specs) {}
};

