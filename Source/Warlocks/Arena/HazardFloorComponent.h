// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Warlocks/Arena/EffectApplyingFloorComponent.h"
#include "HazardFloorComponent.generated.h"

UCLASS(ClassGroup=(Warlocks), meta=(BlueprintSpawnableComponent))
class WARLOCKS_API UHazardFloorComponent : public UEffectApplyingFloorComponent
{
	GENERATED_BODY()
public:
	UHazardFloorComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FScalableFloat DamageAmount;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FScalableFloat DamageTickPeriod;
private:
	virtual void PrepareGameplayEffectSpecForApplication(TArray<FGameplayEffectSpec>& Specs) final;
};
