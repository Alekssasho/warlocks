// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaFloor.h"

#include "Engine/Public/TimerManager.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"
#include "ProceduralMeshComponent/Public/KismetProceduralMeshLibrary.h"

// Sets default values
AArenaFloor::AArenaFloor()
{
	bReplicates = true;
	bAlwaysRelevant = true;

	ProceduralMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Procedural Mesh Component"));
	RootComponent = ProceduralMesh;
	CurrentMeshSectionsCount = 0;
}

void AArenaFloor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AArenaFloor, CurrentMeshSectionsCount);
}

void AArenaFloor::OnConstruction(const FTransform& Transform)
{
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FProcMeshTangent> Tangents;
	TArray<FLinearColor> EmptyArray;

	UKismetProceduralMeshLibrary::GetSectionFromStaticMesh(InitialMesh, 0, 0, Vertices, Triangles, Normals, UVs, Tangents);

	for (uint32 i = 0; i < NumberOfMeshes; i++)
	{
		FVector Scale(InitialScale + i * SectionScale);
		TArray<FVector> ScaledVertices(Vertices);
		for (FVector& Vertex : ScaledVertices)
		{
			Vertex *= Scale;
		}

		ProceduralMesh->CreateMeshSection_LinearColor(i, ScaledVertices, Triangles, Normals, UVs, EmptyArray, Tangents, true);
		ProceduralMesh->SetMaterial(i, InitialMaterial);
	}

	CurrentMeshSectionsCount = NumberOfMeshes;
}

// Called when the game starts or when spawned
void AArenaFloor::BeginPlay()
{
	Super::BeginPlay();

	// Start timer only on the Server
	if (bShouldBreakOnTimer && HasAuthority())
	{
		GetWorldTimerManager().SetTimer(
			BreakdownTimerHandle,
			FTimerDelegate::CreateUObject(this, &AArenaFloor::Breakdown),
			SecondsUntilBreakdown,
			true /* bIsLooping */);
	}
}

void AArenaFloor::EndPlay(const EEndPlayReason::Type Reason)
{
	Super::EndPlay(Reason);

	if (bShouldBreakOnTimer && HasAuthority())
	{
		GetWorldTimerManager().ClearTimer(BreakdownTimerHandle);
	}
}

void AArenaFloor::Breakdown()
{
	// Should happen only on the Server
	check(HasAuthority());

	if (CurrentMeshSectionsCount > 0)
	{
		--CurrentMeshSectionsCount;
		ProceduralMesh->ClearMeshSection(CurrentMeshSectionsCount);
	}
}

void AArenaFloor::OnRep_CurrentMeshSectionsCount(const int32& OldValue)
{
	// Should happen only on the client
	// Guard against the first update
	if (CurrentMeshSectionsCount < OldValue)
	{
		ProceduralMesh->ClearMeshSection(CurrentMeshSectionsCount);
	}
}