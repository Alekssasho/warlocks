// Fill out your copyright notice in the Description page of Project Settings.


#include "Floor.h"
#include "Warlocks/Arena/FloorComponent.h"

ABaseFloor::ABaseFloor()
{
	bReplicates = true;
	bAlwaysRelevant = true;

	OnActorBeginOverlap.AddDynamic(this, &ABaseFloor::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &ABaseFloor::OnOverlapEnd);
}

void ABaseFloor::OnOverlapBegin(AActor* OverlappedActor, AActor* OtherActor)
{
	TInlineComponentArray<UFloorComponent*> FloorComponents(this);

	for(UFloorComponent* FloorComponent : FloorComponents)
	{
		FloorComponent->OnOverlapBegin(OtherActor);
	}
}

void ABaseFloor::OnOverlapEnd(AActor* OverlappedActor, AActor* OtherActor)
{
	TInlineComponentArray<UFloorComponent*> FloorComponents(this);

	for (UFloorComponent* FloorComponent : FloorComponents)
	{
		FloorComponent->OnOverlapEnd(OtherActor);
	}
}
