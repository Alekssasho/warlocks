// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include <AttributeSet.h>
#include <Warlocks/Arena/FloorComponent.h>

#include "ForcePullFloorComponent.generated.h"

// Requires the actor to have ticking enabled to work properly
UCLASS(ClassGroup=(Warlocks), meta=(BlueprintSpawnableComponent))
class WARLOCKS_API UForcePullFloorComponent : public UFloorComponent
{
	GENERATED_BODY()
	UForcePullFloorComponent();
private:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnOverlapBegin(AActor* OtherActor) override;
	virtual void OnOverlapEnd(AActor* OtherActor) override;

	TSet<AActor*> ProjectileActorsToApplyForce;
	TMap<AActor*, uint16> CharacterActorToRootMotionId;
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FScalableFloat ProjectileForceStrength;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FScalableFloat CharacterForceStrength;
};
