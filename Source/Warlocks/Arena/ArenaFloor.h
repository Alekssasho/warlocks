// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "ArenaFloor.generated.h"

UCLASS()
class WARLOCKS_API AArenaFloor : public AActor
{
	GENERATED_BODY()
	
public:
	AArenaFloor();

	UPROPERTY(EditAnywhere)
	uint32 NumberOfMeshes = 3u;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UStaticMesh> InitialMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UMaterialInterface> InitialMaterial;

	UPROPERTY(EditAnywhere)
	float SecondsUntilBreakdown = 5.f;

	UPROPERTY(EditAnywhere)
	bool bShouldBreakOnTimer = true;

	UPROPERTY(EditAnywhere)
	float InitialScale = 1.f;

	UPROPERTY(EditAnywhere)
	float SectionScale = 3.f;

protected:
	virtual void OnConstruction(const FTransform& Transform) override;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void Breakdown();

	UPROPERTY()
	TObjectPtr<UProceduralMeshComponent> ProceduralMesh;

	// This is the current visible mesh sections. When the arena is breakdown
	// it decrements this count, and it replicates to user so that they can remove
	// the specific section as well.
	UPROPERTY(ReplicatedUsing = OnRep_CurrentMeshSectionsCount)
	int32 CurrentMeshSectionsCount;
private:
	FTimerHandle BreakdownTimerHandle;

	UFUNCTION()
	void OnRep_CurrentMeshSectionsCount(const int32& OldValue);

};
