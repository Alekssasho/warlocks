// Fill out your copyright notice in the Description page of Project Settings.


#include "HazardFloorComponent.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"

// Sets default values
UHazardFloorComponent::UHazardFloorComponent()
{
	GameplayEffectsToApply.Push(UWarlockHazardFloorDotEffect::StaticClass());
}

void UHazardFloorComponent::PrepareGameplayEffectSpecForApplication(TArray<FGameplayEffectSpec>& Specs)
{
	float Level = 1;
	Specs[0].Period = DamageTickPeriod.GetValueAtLevel(Level);
	Specs[0].SetSetByCallerMagnitude(FWarlockGameplayTagCache::Get()->DamageAmountTag, DamageAmount.GetValueAtLevel(Level));
}
