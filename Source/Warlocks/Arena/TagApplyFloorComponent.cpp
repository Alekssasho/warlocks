// Fill out your copyright notice in the Description page of Project Settings.


#include "TagApplyFloorComponent.h"
#include "Warlocks/Abilities/Effects/WarlockCoreEffects.h"

UTagApplyFloorComponent::UTagApplyFloorComponent()
{
	GameplayEffectsToApply.Push(UWarlockTagApplyEffect::StaticClass());
}

void UTagApplyFloorComponent::PrepareGameplayEffectSpecForApplication(TArray<FGameplayEffectSpec>& Specs)
{
	Specs[0].DynamicGrantedTags.AddTag(TagToApply);
}

