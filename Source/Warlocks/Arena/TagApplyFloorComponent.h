// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Warlocks/Arena/EffectApplyingFloorComponent.h"
#include "TagApplyFloorComponent.generated.h"

UCLASS(ClassGroup=(Warlocks), meta=(BlueprintSpawnableComponent))
class WARLOCKS_API UTagApplyFloorComponent : public UEffectApplyingFloorComponent
{
	GENERATED_BODY()
public:
	UTagApplyFloorComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FGameplayTag TagToApply;
private:
	virtual void PrepareGameplayEffectSpecForApplication(TArray<FGameplayEffectSpec>& Specs) final;
};