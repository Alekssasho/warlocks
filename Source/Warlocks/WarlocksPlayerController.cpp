// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "WarlocksPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PlayerControlledCharacter.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"

#include "Warlocks/WarlockPlayerState.h"
#include "Warlocks/UI/WarlockUIControllerBaseWidget.h"
#include "Warlocks/UI/HUD/InGameHUD.h"

AWarlocksPlayerController::AWarlocksPlayerController()
{
	PathFollowingComponent = CreateDefaultSubobject<UPathFollowingComponent>(TEXT("PathFollowingComponent"));

	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::None;
	bIsAlive = true;
	bHasAcknowledgedPlayerState = false;
	bCanMoveToCursor = true;
}

void AWarlocksPlayerController::SpawnDefaultHUD()
{
	if (GetNetMode() == NM_ListenServer || GetNetMode() == NM_Standalone)
	{
		// This is called multiple times when we are a ListenServer so avoid recreating the HUD
		if (IsValid(MyHUD))
		{
			return;
		}
	}
	ClientSetHUD(AInGameHUD::StaticClass());
}

void AWarlocksPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AWarlocksPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AWarlocksPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AWarlocksPlayerController::OnSetDestinationReleased);

	InputComponent->BindAxis("ChangeCameraZoom", this, &AWarlocksPlayerController::OnChangeCameraZoom);
}

void AWarlocksPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AWarlockPlayerState* PS = GetPlayerState<AWarlockPlayerState>();
	APlayerControlledCharacter* PossessedCharacter = Cast<APlayerControlledCharacter>(InPawn);
	if (HasAuthority() && IsValid(PS) && IsValid(PossessedCharacter))
	{
		OnRespawnCharacter(PS, PossessedCharacter);
	}
}

void AWarlocksPlayerController::OnUnPossess()
{
	Super::OnUnPossess();

	check(PathFollowingComponent)
	PathFollowingComponent->Cleanup();
}

void AWarlocksPlayerController::ClientRestart_Implementation(APawn* NewPawn)
{
	Super::ClientRestart_Implementation(NewPawn);

	if (!NewPawn)
	{
		return;
	}

	AWarlockPlayerState* PS = GetPlayerState<AWarlockPlayerState>();
	APlayerControlledCharacter* PossessedCharacter = GetPawn<APlayerControlledCharacter>();
	check(IsValid(PS));
	check(IsValid(PossessedCharacter));

	// Reset internal state but only if we are on a separate client
	// Standalone / listen server would call this func in OnPossess
	if (GetNetMode() == NM_Client)
	{
		OnRespawnCharacter(PS, PossessedCharacter);
	}
}

void AWarlocksPlayerController::OnRespawnCharacter(AWarlockPlayerState* PS, APlayerControlledCharacter* PossessedCharacter)
{
	bIsAlive = true;
	bCanMoveToCursor = true;
	check(PathFollowingComponent)
	PathFollowingComponent->Initialize();
	PossessedCharacter->InitFromPlayerState(PS);
	if (HasAuthority())
	{
		PossessedCharacter->GiveAbilities();
		PossessedCharacter->EquipItemSet(PS->StartingItemSet);
	}
	if (IsLocalController())
	{
		PossessedCharacter->BindAbilitySystemInput();
		auto HUDInstance = CastChecked<AWarlocksBaseHUD>(MyHUD);
		HUDInstance->UIControllerWidget->SetPossessedCharacter(PossessedCharacter);
	}
}

void AWarlocksPlayerController::OnRep_PlayerState()
{
	if (!bHasAcknowledgedPlayerState)
	{
		ServerAcknowledgePlayerState();
		bHasAcknowledgedPlayerState = true;
	}
	InitFromPlayerState_OnClient();
}

void AWarlocksPlayerController::InitFromPlayerState_OnClient()
{
	auto HUDInstance = CastChecked<AWarlocksBaseHUD>(MyHUD);
	HUDInstance->UIControllerWidget->InitializeFromPlayerController(this);
}

void AWarlocksPlayerController::ServerAcknowledgePlayerState_Implementation()
{
	bHasAcknowledgedPlayerState = true;
}

void AWarlocksPlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(Hit.ImpactPoint);
	}
}

void AWarlocksPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APlayerControlledCharacter* const MyPawn = Cast<APlayerControlledCharacter>(GetPawn());
	// If we are in UseControllerDesiredRotation means we are casting a spell which wants
	// to turn in place, stop all movement then.
	if (MyPawn && !MyPawn->GetCharacterMovement()->bUseControllerDesiredRotation)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AWarlocksPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	if (bCanMoveToCursor)
	{
		bMoveToMouseCursor = true;
	}
}

void AWarlocksPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	if (bCanMoveToCursor)
	{
		bMoveToMouseCursor = false;
	}
}

void AWarlocksPlayerController::OnChangeCameraZoom(float value)
{
	APlayerControlledCharacter* const MyPawn = Cast<APlayerControlledCharacter>(GetPawn());
	if (MyPawn)
	{
		USpringArmComponent* CameraArm = MyPawn->GetCameraBoom();
		CameraArm->TargetArmLength -= value * 80;
		CameraArm->TargetArmLength = FMath::Clamp(CameraArm->TargetArmLength, 400.0f, 1000.0f);
	}
}

void AWarlocksPlayerController::ToggleMouseOnUI(bool bIsVisible)
{
	bShowMouseCursor = bIsVisible;
	bEnableClickEvents = bIsVisible;
	bEnableMouseOverEvents = bIsVisible;

	if (bIsVisible)
	{
		CurrentMouseCursor = EMouseCursor::Default;
	}
	else
	{
		CurrentMouseCursor = EMouseCursor::None;
	}
}

void AWarlocksPlayerController::OnDeath()
{
	bMoveToMouseCursor = false;
	bIsAlive = false;
	bCanMoveToCursor = false;
	UnPossess();
}

void AWarlocksPlayerController::OnStunStarted()
{
	bCanMoveToCursor = false;
	bMoveToMouseCursor = false;
}

void AWarlocksPlayerController::OnStunEnded()
{
	bCanMoveToCursor = true;
}

// Connection and match handling
void AWarlocksPlayerController::ClientNotifyWaitingForMatchToStart_Implementation()
{
	check(IsLocalController());

	auto HUDInstance = Cast<AWarlocksBaseHUD>(MyHUD);
	check(HUDInstance);
	HUDInstance->UIControllerWidget->OnWaitingForMatchToStart();
}

void AWarlocksPlayerController::ClientNotifyMatchHasStarted_Implementation()
{
	check(IsLocalController());

	auto HUDInstance = Cast<AWarlocksBaseHUD>(MyHUD);
	check(HUDInstance);
	HUDInstance->UIControllerWidget->OnMatchHasStarted();
}

void AWarlocksPlayerController::ClientNotifyMatchHasEnded_Implementation()
{
	check(IsLocalController());

	auto HUDInstance = Cast<AWarlocksBaseHUD>(MyHUD);
	check(HUDInstance);
	HUDInstance->UIControllerWidget->OnMatchHasEnded();
}

void AWarlocksPlayerController::ClientNotifyPrepareNextRound_Implementation()
{
	check(IsLocalController());

	auto HUDInstance = Cast<AWarlocksBaseHUD>(MyHUD);
	check(HUDInstance);
	HUDInstance->UIControllerWidget->OnPrepareNextRound();
}


void AWarlocksPlayerController::ClientNotifyRoundHasStarted_Implementation()
{
	check(IsLocalController());

	ToggleMouseOnUI(false);

	auto HUDInstance = Cast<AWarlocksBaseHUD>(MyHUD);
	check(HUDInstance);
	HUDInstance->UIControllerWidget->OnRoundHasStarted();

	// Reset readiness flag
	if (AWarlockPlayerState* PS = GetPlayerState<AWarlockPlayerState>())
	{
		PS->bIsPlayerReadyForNextRound = false;
	}
}

void AWarlocksPlayerController::ClientNotifyRoundHasEnded_Implementation()
{
	check(IsLocalController());

	// TODO: Also, stop accepting movement/ability input
	ToggleMouseOnUI(true);

	auto HUDInstance = Cast<AWarlocksBaseHUD>(MyHUD);
	check(HUDInstance);
	HUDInstance->UIControllerWidget->OnRoundHasEnded();
}
