#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "GameplayAbilities/Public/AbilitySystemInterface.h"
#include "GameplayAbilities/Public/AbilitySystemInterface.h"
#include "WarlockAttributes.h"
#include "Currency/CurrencyManagerComponent.h"
#include "Inventory/ItemSet.h"
#include "WarlockPlayerState.generated.h"

UCLASS()
class WARLOCKS_API AWarlockPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()
public:
	AWarlockPlayerState();

	// Replication
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UWarlockAttributes* GetAttributes() const;

	// Sets the value of bIsPlayerReadyForNextRound and sends a RPC to notify the server about the new value
	// Should ONLY be called on the owning client
	UFUNCTION(BlueprintCallable, meta=(DisplayName="Set Replicated Is Player Ready For Next Round"))
	void SetReplicatedIsPlayerReadyForNextRound_OnClient(bool bIsPlayerReady);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Items)
	TObjectPtr<UItemSet> StartingItemSet;

	UPROPERTY(Replicated, BlueprintReadOnly, Category=MatchState)
	bool bIsPlayerReadyForNextRound;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<UCurrencyManagerComponent> CurrencyManager;

private:
	UFUNCTION(Server, Reliable)
	void ServerSetIsPlayerReadyForNextRound(bool bIsPlayerReady);

	UPROPERTY()
	TObjectPtr<UAbilitySystemComponent> AbilitySystem;

	UPROPERTY()
	TObjectPtr<UWarlockAttributes> Attributes;
};
