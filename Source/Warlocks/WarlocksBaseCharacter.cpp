// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "WarlocksBaseCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"
#include "Net/UnrealNetwork.h"
#include "Animation/AnimInstance.h"

#include "Warlocks/Abilities/KnockbackAbility.h"
#include "Warlocks/Inventory/ItemSet.h"
#include "Warlocks/Blueprint/WarlocksBlueprintLibrary.h"
#include "Warlocks/WarlocksGameplayTagCache.h"
#include "Warlocks/WarlocksCharacterEffectApplicator.h"

AWarlocksBaseCharacter::AWarlocksBaseCharacter()
{
	bReplicates = true;
	bAlwaysRelevant = true;

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// This is needed for dedicated server, as we are using bone positions to spawn
	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;

	Inventory = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory"));
	const UEnum* SlotEnum = StaticEnum<ECharacterSlot>();
	SlottedAbilities.SetNumZeroed(SlotEnum->NumEnums());

	EffectApplicator = CreateDefaultSubobject<UCharacterEffectApplicatorComponent>(TEXT("EffectApplicator"));
}

void AWarlocksBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AWarlocksBaseCharacter::GiveAbilities()
{
	if (AbilitySystem.IsValid() && HasAuthority())
	{
		// Every warlock should be knockback-able
		AbilitySystem->GiveAbility(FGameplayAbilitySpec(UKnockbackAbility::StaticClass()));

		EffectApplicator->Setup();
	}
}

void AWarlocksBaseCharacter::EquipItemSet(UItemSet* Set)
{
	if (!Set)
	{
		return;
	}

	// Remove old ones, as there could be slots that are present in one set and not in another set
	Inventory->RemoveAllItems();

	for (const auto& Item : Set->ItemsToEquip)
	{
		Inventory->EquipItem(Item);
	}
}

bool AWarlocksBaseCharacter::GetCooldownInfoForSlot(ECharacterSlot Slot, float& Duration, float& RemainingTime) const
{
	// Currently we have only a global cooldown, so ingore the slots for now
	if (AbilitySystem.IsValid())
	{
		RemainingTime = 0.f;
		Duration = 0.f;

		FGameplayTagContainer TagContainer = FGameplayTagContainer(UWarlocksBlueprintLibrary::GetTagFromSlot(Slot));
		FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAllOwningTags(TagContainer);
		TArray<TPair<float, float>> DurationAndTimeRemaining = AbilitySystem->GetActiveEffectsTimeRemainingAndDuration(Query);
		if (DurationAndTimeRemaining.Num() > 0)
		{
			int32 BestIdx = 0;
			float LongestTime = DurationAndTimeRemaining[0].Key;
			for (int32 Idx = 1; Idx < DurationAndTimeRemaining.Num(); ++Idx)
			{
				if (DurationAndTimeRemaining[Idx].Key > LongestTime)
				{
					LongestTime = DurationAndTimeRemaining[Idx].Key;
					BestIdx = Idx;
				}
			}

			RemainingTime = DurationAndTimeRemaining[BestIdx].Key;
			Duration = DurationAndTimeRemaining[BestIdx].Value;

			return true;
		}
	}

	return false;
}

bool AWarlocksBaseCharacter::IsSlotOnCooldown(ECharacterSlot Slot) const
{
	FGameplayAbilitySpecHandle Handle = Inventory->AbilityHandlesForEquippedItems[uint8(Slot)];
	FGameplayAbilitySpec* Spec = AbilitySystem->FindAbilitySpecFromHandle(Handle);
	if (!Spec->Ability->CanActivateAbility(Handle, AbilitySystem->AbilityActorInfo.Get()))
	{
		return true;
	}

	// Our abilities are instanced per actor, so we cannot activate them again if there is an activation running
	if (Spec->IsActive())
	{
		return true;
	}

	return false;
}

float AWarlocksBaseCharacter::GetHealth()
{
	if (Attributes.IsValid())
	{
		return Attributes->GetHealth();
	}

	return 0.0f;
}

void AWarlocksBaseCharacter::HitReact_Implementation()
{
	ShowHitReact.Broadcast();
}

void AWarlocksBaseCharacter::Die_Implementation()
{
	// TODO: Probably add loose gameplay tag that we are dead, and disable abilities from actication when we have the dead tag
	// as the same as Stun tag.
	OnDeath();

	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);

	GetCharacterMovement()->StopMovementImmediately();

	if (AbilitySystem.IsValid())
	{
		// TODO: This is null in many cases, we are prbably missing something about the lifetime of the player state
		AbilitySystem->CancelAllAbilities();

		EffectApplicator->RemoveDelegates();
	}

	UAnimInstance* AnimInstance = (GetMesh()) ? GetMesh()->GetAnimInstance() : nullptr;
	if (IsValid(DeathMontage) && IsValid(AnimInstance))
	{
		AnimInstance->Montage_Play(DeathMontage);

		FOnMontageBlendingOutStarted EndDelegate;
		EndDelegate.BindUObject(this, &AWarlocksBaseCharacter::OnDeathMontageEnded);
		AnimInstance->Montage_SetBlendingOutDelegate(EndDelegate, DeathMontage);
	}
	else
	{
		FinishDying();
	}
}

void AWarlocksBaseCharacter::OnDeathMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	FinishDying();
}

void AWarlocksBaseCharacter::FinishDying()
{
	Destroy();
}

void AWarlocksBaseCharacter::Stun_Implementation()
{
	OnStunStarted();

	GetCharacterMovement()->StopMovementImmediately();

	// Add a tag so other system know we are stunned
	AbilitySystem->AddLooseGameplayTag(FWarlockGameplayTagCache::Get()->StunTag);

	UAnimInstance* AnimInstance = (GetMesh()) ? GetMesh()->GetAnimInstance() : nullptr;
	if (IsValid(StunMontage) && IsValid(AnimInstance))
	{
		AnimInstance->Montage_Play(StunMontage);

		FOnMontageBlendingOutStarted EndDelegate;
		EndDelegate.BindUObject(this, &AWarlocksBaseCharacter::OnStunMontageEnded);
		AnimInstance->Montage_SetBlendingOutDelegate(EndDelegate, StunMontage);
	}
	else
	{
		// We must have a valid animation montage as otherwise we don't know when we are no longer stunned
		ensureAlways(false);
	}
}

void AWarlocksBaseCharacter::OnStunMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	AbilitySystem->RemoveLooseGameplayTag(FWarlockGameplayTagCache::Get()->StunTag);
	OnStunEnded();
}

void AWarlocksBaseCharacter::StartTurnInPlace(FRotator DesiredActorRotator)
{
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;

	FRotator CurrentRotator = GetActorRotation();
	// Set the rotation to the current one, so we don't have a snap
	// Animation notify will change the control rotation to the desired one
	GetController()->SetControlRotation(CurrentRotator);

	FRotator DeltaRotator = DesiredActorRotator - CurrentRotator;
	DeltaRotator.Normalize();

	float DesiredYawRotation = DeltaRotator.Yaw;
	TargetAbilityCasted.Broadcast(DesiredYawRotation);

	if (HasAuthority())
	{
		MulticastTargetRotationToNonOwningClients(DesiredYawRotation);
	}
}

void AWarlocksBaseCharacter::MulticastTargetRotationToNonOwningClients_Implementation(float DesiredRotation)
{
	// Owning client has already done this, as the server as well
	// This is only to appear correctly on other client machines
	if (GetLocalRole() == ROLE_SimulatedProxy)
	{
		TargetAbilityCasted.Broadcast(DesiredRotation);
	}
}

void AWarlocksBaseCharacter::StopTurnInPlace()
{
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bUseControllerDesiredRotation = false;
}

void AWarlocksBaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AWarlocksBaseCharacter, SlottedAbilities, COND_OwnerOnly);
}
