#pragma once

#include "Containers/StaticArray.h"
#include "CommonTypes.generated.h"

/** Slots usable by the inventory system for items and the corresponding ability slots for the ability system */
UENUM(BlueprintType)
enum class ECharacterSlot : uint8
{
	RightFinger,
	LeftFinger,
	RightHand,
	Chest,
	Feet,
};

UENUM(BlueprintType)
enum class EAbilitySchool : uint8
{
	Fire,
	Water,
	Earth,
	Air,
	Count UMETA(Hidden)
};

UENUM(BlueprintType)
enum class EAbilityLevel : uint8
{
	L1,
	L2,
	L3,
	Count UMETA(Hidden)
};

/** Check */
UENUM(BlueprintType, meta=(Bitflags))
enum class EAbilityCastableState : uint8
{
	Castable = 0,
	OutOfRange = 1,
	OnCooldown = 2,
	InsufficientResources = 4,
	InvalidSetup,
};

template <typename T>
using TSchoolArray = TStaticArray<T, uint8(EAbilitySchool::Count)>;

template <typename T>
using TLevelArray = TStaticArray<T, uint8(EAbilityLevel::Count)>;
