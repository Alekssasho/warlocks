// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WarlocksBaseCharacter.h"
#include "Warlocks/Abilities/LocationProviderInterface.h"
#include "AIControlledCharacter.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AAIControlledCharacter : public AWarlocksBaseCharacter, public ILocationProviderInterface
{
	GENERATED_BODY()
public:
	AAIControlledCharacter();

	virtual void BeginPlay() override;

	// ILocationProviderInterface
	virtual FHitResult ProvideLocation() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Items)
	TObjectPtr<UItemSet> StartingItemSet;

private:
	UPROPERTY()
	TObjectPtr<UAbilitySystemComponent> HardRefAbilitySystem;

	UPROPERTY()
	TObjectPtr<UWarlockAttributes> HardRefAttributes;
};
