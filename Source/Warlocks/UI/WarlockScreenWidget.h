#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WarlockScreenWidget.generated.h"

UCLASS()
class UWarlockScreenWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	void InitializePlayerState(class AWarlockPlayerState* InPlayerState);
	void InitializeGameState(class AStandardMatchGameState* InGameState);

	// Initializes this screen with all the data in the character
	// TODO: Do we even need the character? Most screens can make-do with simple the PlayerState data!
	void SetPossessedCharacter(class AWarlocksBaseCharacter* InCharacter);

	// Called when all params of the screen has been setup and is time to initialize all the data
	UFUNCTION(BlueprintImplementableEvent, Category = Warlocks)
	void OnInitializeScreen();
protected:
	UPROPERTY(BlueprintReadOnly)
	class AWarlocksBaseCharacter* Character;
	UPROPERTY(BlueprintReadOnly)
	class AWarlockPlayerState* PlayerState;
	UPROPERTY(BlueprintReadOnly)
	class AStandardMatchGameState* GameState;
};
