#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WarlockUIControllerBaseWidget.generated.h"

// The controller is responsible for managing all screens in the game.
// It is initialized when the PlayerController first spawns its HUD and makes sure to 
// propagate all relevant data (playerstate, gamestate, character) to its screens as well as
// manage the active screen. Most of the screen management is done in BP.
// TODO: Do we even care about the possessed character? Most of the data the UI needs is in the PlayerState anyway.
UCLASS()
class UWarlockUIControllerBaseWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	// UUserWidget virtuals
	virtual void NativeOnInitialized() override;

	// Warlock-specific initialization
	void InitializeFromPlayerController(class APlayerController*);
	void SetPossessedCharacter(class AWarlocksBaseCharacter* Character);

	// Called when all params have been setup and is time to initialize all the data
	UFUNCTION(BlueprintImplementableEvent, Category = Warlocks)
	void OnInitializeController();

	UFUNCTION(BlueprintImplementableEvent)
	void OnWaitingForMatchToStart();
	UFUNCTION(BlueprintImplementableEvent)
	void OnMatchHasStarted();
	UFUNCTION(BlueprintImplementableEvent)
	void OnMatchHasEnded();
	UFUNCTION(BlueprintImplementableEvent)
	void OnPrepareNextRound();
	UFUNCTION(BlueprintImplementableEvent)
	void OnRoundHasStarted();
	UFUNCTION(BlueprintImplementableEvent)
	void OnRoundHasEnded();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnOpenInventory();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnCloseInventory();
protected:
	UFUNCTION(BlueprintCallable, Category = "Screen Management")
	void ChangeActiveScreen(class UWarlockScreenWidget* NextActiveScreen);

	void ToggleScreen(class UWarlockScreenWidget* Screen, bool bShouldEnable);

	UPROPERTY(BlueprintReadOnly)
	class AWarlocksBaseCharacter* Character;

	UPROPERTY(BlueprintReadOnly, Category = "Screen Management")
	TArray<TObjectPtr<class UWarlockScreenWidget>> AllScreens;

	UPROPERTY(BlueprintReadOnly, Category = "Screen Management")
	TObjectPtr<UWarlockScreenWidget> CurrentActiveScreen;
};


