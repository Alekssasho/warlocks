#include "WarlockUIControllerBaseWidget.h"
#include "WarlockScreenWidget.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/WarlocksPlayerController.h"
#include "Warlocks/WarlockPlayerState.h"
#include "Warlocks/GameRules/StandardMatchGameState.h"

void UWarlockUIControllerBaseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	auto InitScreenGameStates = [this](AGameStateBase* State)
	{
		AStandardMatchGameState* StandardGameState = Cast<AStandardMatchGameState>(State);

		if (!IsValid(StandardGameState))
		{
			// No StandardGameState in the main menu
			return;
		}
		for (UWarlockScreenWidget* Screen : AllScreens)
		{
			Screen->InitializeGameState(StandardGameState);
		}
	};
	GetWorld()->GameStateSetEvent.AddWeakLambda(this, InitScreenGameStates);
	if (auto GameState = GetWorld()->GetGameState())
	{
		InitScreenGameStates(GameState);
	}

	for (UWarlockScreenWidget* Screen : AllScreens)
	{
		ToggleScreen(Screen, false);
	}
}

void UWarlockUIControllerBaseWidget::InitializeFromPlayerController(APlayerController* PlayerController)
{
	AWarlockPlayerState* PlayerState = PlayerController->GetPlayerState<AWarlockPlayerState>();
	for (UWarlockScreenWidget* Screen : AllScreens)
	{
		Screen->InitializePlayerState(PlayerState);
	}
}

void UWarlockUIControllerBaseWidget::SetPossessedCharacter(class AWarlocksBaseCharacter* InCharacter)
{
	Character = InCharacter;
	for (UWarlockScreenWidget* Screen : AllScreens)
	{
		Screen->SetPossessedCharacter(InCharacter);
	}
	OnInitializeController();
}

void UWarlockUIControllerBaseWidget::ChangeActiveScreen(class UWarlockScreenWidget* NextActiveScreen)
{
	check(IsValid(NextActiveScreen));

	if (IsValid(CurrentActiveScreen))
	{
		ToggleScreen(CurrentActiveScreen, false);
	}
	CurrentActiveScreen = NextActiveScreen;
	ToggleScreen(CurrentActiveScreen, true);
}

void UWarlockUIControllerBaseWidget::ToggleScreen(class UWarlockScreenWidget* Screen, bool bShouldEnable)
{
	Screen->SetIsEnabled(bShouldEnable);
	Screen->SetVisibility(bShouldEnable ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
}