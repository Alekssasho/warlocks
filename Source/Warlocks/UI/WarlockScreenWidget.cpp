#include "WarlockScreenWidget.h"
#include "Warlocks/GameRules/StandardMatchGameState.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/WarlocksPlayerController.h"
#include "Warlocks/WarlockPlayerState.h"

void UWarlockScreenWidget::InitializePlayerState(AWarlockPlayerState* InPlayerState)
{
	PlayerState = InPlayerState;
}
void UWarlockScreenWidget::InitializeGameState(AStandardMatchGameState* InGameState)
{
	GameState = InGameState;
}

void UWarlockScreenWidget::SetPossessedCharacter(class AWarlocksBaseCharacter* InCharacter)
{
	check(IsValid(PlayerState));
	Character = InCharacter;
	OnInitializeScreen();
}