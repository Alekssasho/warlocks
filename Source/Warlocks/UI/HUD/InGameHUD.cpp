// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameHUD.h"
#include "UObject/ConstructorHelpers.h"

AInGameHUD::AInGameHUD()
{
	static ConstructorHelpers::FClassFinder<UWarlockUIControllerBaseWidget> UIControllerClass(TEXT("/Game/UI/WB_InGameHUDUIController"));
	if (UIControllerClass.Class != NULL)
	{
		UIControllerWidgetClass = UIControllerClass.Class;
	}
}