// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Warlocks/UI/WarlockUIControllerBaseWidget.h"
#include "WarlocksBaseHUD.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AWarlocksBaseHUD : public AHUD
{
	GENERATED_BODY()

public:
	AWarlocksBaseHUD();
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = "Warlocks|UI")
	TObjectPtr<UWarlockUIControllerBaseWidget> UIControllerWidget;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Warlocks|UI")
	TSubclassOf<UWarlockUIControllerBaseWidget> UIControllerWidgetClass;
};
