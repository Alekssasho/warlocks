// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Warlocks/UI/HUD/WarlocksBaseHUD.h"
#include "MainMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class WARLOCKS_API AMainMenuHUD : public AWarlocksBaseHUD
{
	GENERATED_BODY()
	
public:
	AMainMenuHUD();
};
