// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuHUD.h"
#include "UObject/ConstructorHelpers.h"

AMainMenuHUD::AMainMenuHUD()
{
	static ConstructorHelpers::FClassFinder<UWarlockUIControllerBaseWidget> UIControllerClass(TEXT("/Game/UI/WB_MainMenuUIController"));
	if (UIControllerClass.Class != NULL)
	{
		UIControllerWidgetClass = UIControllerClass.Class;
	}
}