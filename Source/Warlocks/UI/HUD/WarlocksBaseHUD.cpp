// Fill out your copyright notice in the Description page of Project Settings.


#include "WarlocksBaseHUD.h"

AWarlocksBaseHUD::AWarlocksBaseHUD()
{
}

void AWarlocksBaseHUD::OnConstruction(const FTransform& Transform)
{
	UIControllerWidget = CreateWidget<UWarlockUIControllerBaseWidget>(GetWorld(), UIControllerWidgetClass);
}

void AWarlocksBaseHUD::BeginPlay()
{
	check(UIControllerWidget);
	UIControllerWidget->AddToViewport();
}