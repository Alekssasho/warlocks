// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Warlocks : ModuleRules
{
	public Warlocks(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "NavigationSystem",
            "AIModule",
            "GameplayAbilities",
            "GameplayTags",
            "GameplayTasks",
            "OnlineSubsystem",
            "OnlineSubsystemSteam",
            "Slate",
            "SlateCore",
            "UMG",
            "ProceduralMeshComponent"
        });

        if(Target.Type == TargetType.Editor)
        {
            PublicDependencyModuleNames.Add("UnrealEd");
        }
	}
}
