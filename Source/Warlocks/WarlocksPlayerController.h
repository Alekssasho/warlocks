// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Navigation/PathFollowingComponent.h"
#include "WarlocksPlayerController.generated.h"

UCLASS()
class AWarlocksPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AWarlocksPlayerController();

	void OnDeath();
	void OnStunStarted();
	void OnStunEnded();

	// HUD management
	// Always spawn our HUD as default
	virtual void SpawnDefaultHUD() override;

	// Checks if the client has acknowledge the player state. Must be called only on the server
	bool HasAcknowledgedPlayerState_OnServer() const { return bHasAcknowledgedPlayerState; }

	// Initializes all internal state dependent on the PlayerState
	void InitFromPlayerState_OnClient();

	UFUNCTION(Client, Reliable)
	void ClientNotifyWaitingForMatchToStart();
	UFUNCTION(Client, Reliable)
	void ClientNotifyMatchHasStarted();
	UFUNCTION(Client, Reliable)
	void ClientNotifyMatchHasEnded();
	UFUNCTION(Client, Reliable)
	void ClientNotifyPrepareNextRound();
	UFUNCTION(Client, Reliable)
	void ClientNotifyRoundHasStarted();
	UFUNCTION(Client, Reliable)
	void ClientNotifyRoundHasEnded();
protected:
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;
	// Called when RestartPlayer is called on the server each round
	virtual void ClientRestart_Implementation(APawn* InPawn) override;
	// Needed to notify the UI about the new PlayerState
	virtual void OnRep_PlayerState() override;
	// End PlayerController interface

	// Server communication
	/** Notifies the server the client has received its PlayerState
	 *
	*/
	UFUNCTION(Server, Reliable)
	void ServerAcknowledgePlayerState();

	/** Resets the state of the PlayerController when a new character is spawned & possessed.
	 ** Must be called on both the server and on the client.
	*/
	void OnRespawnCharacter(class AWarlockPlayerState* PS, class APlayerControlledCharacter* PossessedCharacter);

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	void OnChangeCameraZoom(float value);

	void ToggleMouseOnUI(bool bIsVisible);

	/** Component used for moving along a path. */
	UPROPERTY(VisibleDefaultsOnly, Category = Movement)
	TObjectPtr<UPathFollowingComponent> PathFollowingComponent;

	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;
	uint32 bIsAlive : 1;
	uint32 bHasAcknowledgedPlayerState : 1;
	uint32 bCanMoveToCursor : 1;
};


