// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Warlocks/CommonTypes.h"
#include "GameplayAbilitySpec.h"
#include "InventoryComponent.generated.h"

class UEquippableItem;
/** Delegate called when an item is equipped changes */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemEquipped, const UEquippableItem*, Item);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnItemEquippedNative, const UEquippableItem*);

/** Extra information about a UInventoryItem that is in a player's inventory */
USTRUCT(BlueprintType)
struct FInventoryItemData
{
	GENERATED_BODY()

	/** Constructor, default to count/level 1 so declaring them in blueprints gives you the expected behavior */
	FInventoryItemData()
		: ItemCount(1)
	{}

	FInventoryItemData(int32 InItemCount)
		: ItemCount(InItemCount)
	{}

	/** The number of instances of this item in the inventory, can never be below 1 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	int32 ItemCount;
};

UCLASS( ClassGroup=(Warlocks), meta=(BlueprintSpawnableComponent) )
class WARLOCKS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/** Delegate called when an inventory item has been equipped */
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FOnItemEquipped OnItemEquipped;
	FOnItemEquippedNative OnItemEquippedNative;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void EquipItem(UEquippableItem* Equippable);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void RemoveAllItems();

	/** Stores equipped items. Index corresponds to ECharacterSlot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
	TArray<TObjectPtr<const UEquippableItem>> EquippedItems;

	TArray<FGameplayAbilitySpecHandle> AbilityHandlesForEquippedItems;
protected:
	/** Stores equippable and consumables */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TMap<FPrimaryAssetId, FInventoryItemData> InventoryData;
};
