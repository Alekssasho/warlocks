// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "EquippableItem.h"
#include "ItemSet.generated.h"

UCLASS(BlueprintType)
class UItemSet : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	UItemSet()
	{
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Items)
	TArray<TObjectPtr<UEquippableItem>> ItemsToEquip;

	/** User-visible short name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
	FText ItemSetName;
};


