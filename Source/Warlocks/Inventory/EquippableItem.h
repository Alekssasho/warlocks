#pragma once

#include "Templates/SubclassOf.h"
#include "Engine/DataTable.h"

#include "Warlocks/CommonTypes.h"
#include "Warlocks/Inventory/InventoryItem.h"

#include "EquippableItem.generated.h"

/** Native base class for weapons, should be blueprinted */
UCLASS()
class UEquippableItem : public UInventoryItem
{
	GENERATED_BODY()

public:
	/** The slot this item fills */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
	ECharacterSlot Slot;

	/** Equippable actor to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Equippable)
	TSubclassOf<AActor> EquippableActor;

	/** Ability to grant if this item is slotted */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
	TObjectPtr<class UAbilityDescriptor> GrantedAbility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	EAbilitySchool School;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	EAbilityLevel Level;
};
