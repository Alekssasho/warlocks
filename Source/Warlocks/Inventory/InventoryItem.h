// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Styling/SlateBrush.h"
#include "InventoryItem.generated.h"

/** Base class for all items, do not blueprint directly */
UCLASS(Abstract, BlueprintType)
class UInventoryItem : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	/** Constructor */
	UInventoryItem()
	{}

	/** User-visible short name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
	FText ItemName;

	/** User-visible long description */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
	FText ItemDescription;

	/** Icon to display */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
	FSlateBrush ItemIcon;
};


