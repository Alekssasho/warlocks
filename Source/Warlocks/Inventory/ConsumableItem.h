// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "InventoryItem.h"
#include "ConsumableItem.generated.h"

/** Native base class for weapons, should be blueprinted */
UCLASS()
class UConsumableItem : public UInventoryItem
{
	GENERATED_BODY()

public:
	/** Constructor */
	UConsumableItem()
	{
	}
};