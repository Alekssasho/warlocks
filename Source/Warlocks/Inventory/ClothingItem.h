// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EquippableItem.h"
#include "ClothingItem.generated.h"

/** Native base class for weapons, should be blueprinted */
UCLASS()
class UClothingItem : public UEquippableItem
{
	GENERATED_BODY()

public:
	/** Constructor */
	UClothingItem()
	{
	}
};