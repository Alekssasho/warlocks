// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EquippableItem.h"
#include "WeaponItem.generated.h"

/** Native base class for weapons, should be blueprinted */
UCLASS()
class UWeaponItem : public UEquippableItem
{
	GENERATED_BODY()

public:
	/** Constructor */
	UWeaponItem()
	{
	}
};