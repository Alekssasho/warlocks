// Fill out your copyright notice in the Description page of Project Settings.


#include "Warlocks/Inventory/InventoryComponent.h"
#include "GameplayAbilities/Public/AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "Net/UnrealNetwork.h"

#include "Warlocks/Inventory/EquippableItem.h"
#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/Abilities/AbilityDescriptor.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	SetIsReplicatedByDefault(true);

	const UEnum* InventoryEnum = StaticEnum<ECharacterSlot>();
	check(InventoryEnum);
	EquippedItems.SetNumZeroed(InventoryEnum->NumEnums());
	AbilityHandlesForEquippedItems.SetNumZeroed(InventoryEnum->NumEnums());
}

void UInventoryComponent::EquipItem(UEquippableItem* Item)
{
	uint8 SlotIndex = static_cast<uint8>(Item->Slot);
	const UEquippableItem* OldEquipped = EquippedItems[SlotIndex];

	AWarlocksBaseCharacter* Character = Cast<AWarlocksBaseCharacter>(GetOwner());

	UAbilitySystemComponent* AbilityComp = Character->GetAbilitySystemComponent();

	if (IsValid(OldEquipped))
	{
		const FGameplayAbilitySpecHandle& Handle = AbilityHandlesForEquippedItems[SlotIndex];
		AbilityComp->SetRemoveAbilityOnEnd(Handle);
	}
	EquippedItems[SlotIndex] = Item;
	if (IsValid(Item->GrantedAbility) && IsValid(Item->GrantedAbility->AbilityDefinition))
	{
		Character->SlottedAbilities[SlotIndex] = Item->GrantedAbility;
		FGameplayAbilitySpec AbilitySpec(Item->GrantedAbility->AbilityDefinition, uint8_t(Item->Level), SlotIndex, Item);
		AbilityHandlesForEquippedItems[SlotIndex] = AbilityComp->GiveAbility(AbilitySpec);
	}

	OnItemEquippedNative.Broadcast(Item);
	OnItemEquipped.Broadcast(Item);
}

void UInventoryComponent::RemoveAllItems()
{
	AWarlocksBaseCharacter* Character = Cast<AWarlocksBaseCharacter>(GetOwner());

	UAbilitySystemComponent* AbilityComp = Character->GetAbilitySystemComponent();

	for(int SlotIndex = 0; SlotIndex < EquippedItems.Num(); ++SlotIndex)
	{
		const UEquippableItem* Item = EquippedItems[SlotIndex];
		if(IsValid(Item))
		{
			const FGameplayAbilitySpecHandle& Handle = AbilityHandlesForEquippedItems[SlotIndex];
			AbilityComp->SetRemoveAbilityOnEnd(Handle);
			Character->SlottedAbilities[SlotIndex] = nullptr;
		}
		EquippedItems[SlotIndex] = nullptr;
	}
}


void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UInventoryComponent, EquippedItems, COND_OwnerOnly);
}