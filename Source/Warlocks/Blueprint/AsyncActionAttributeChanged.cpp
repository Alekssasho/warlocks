#include "AsyncActionAttributeChanged.h"

UAsyncActionAttributeChanged* UAsyncActionAttributeChanged::ListenForAttributeChange(AWarlocksBaseCharacter* Character, FGameplayAttribute Attribute)
{
	UAsyncActionAttributeChanged* WaitForAttributeChanged = NewObject<UAsyncActionAttributeChanged>();
	WaitForAttributeChanged->Character = Character;
	WaitForAttributeChanged->AttributeToListenFor = Attribute;

	if (!IsValid(Character) || !IsValid(Character->GetAbilitySystemComponent()) || !Attribute.IsValid())
	{
		WaitForAttributeChanged->RemoveFromRoot();
		return nullptr;
	}

	Character->GetAbilitySystemComponent()->GetGameplayAttributeValueChangeDelegate(Attribute).AddUObject(WaitForAttributeChanged, &UAsyncActionAttributeChanged::AttributeChanged);

	return WaitForAttributeChanged;
}

void UAsyncActionAttributeChanged::EndTask()
{
	if (IsValid(Character) && IsValid(Character->GetAbilitySystemComponent()))
	{
		Character->GetAbilitySystemComponent()->GetGameplayAttributeValueChangeDelegate(AttributeToListenFor).RemoveAll(this);
	}

	SetReadyToDestroy();
	MarkAsGarbage();
}

void UAsyncActionAttributeChanged::AttributeChanged(const FOnAttributeChangeData& Data)
{
	OnAttributeChanged.Broadcast(Data.Attribute, Data.NewValue, Data.OldValue);
}