#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "AttributeSet.h"

#include "Warlocks/WarlocksBaseCharacter.h"
#include "Warlocks/CommonTypes.h"

#include "WarlocksBlueprintLibrary.generated.h"

UCLASS()
class WARLOCKS_API UWarlocksBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintPure, Category = Ability)
	static float EvaluateScalableFloatAtLevel(const FScalableFloat& Float, int Level);

	UFUNCTION(BlueprintPure, Category = Ability)
	static FGameplayTag GetTagFromSlot(ECharacterSlot Slot);

	UFUNCTION(BlueprintCallable, Category = AbilityDescription)
	static void ApplyHelperToDescription(
		class UAbilityDataHelper* Helper,
		class UAbilityDescriptor* Descriptor,
		EAbilitySchool School,
		EAbilityLevel Level
	);

	UFUNCTION(BlueprintCallable, Category = AbilityDescription)
	static void PopulateHelperFromDescription(
		class UAbilityDataHelper* Helper,
		class UAbilityDescriptor* Descriptor,
		EAbilitySchool School,
		EAbilityLevel Level
	);

	// Return rotator which will be wall orientation, which means 90 degrees Yaw delta from the Target-Source orientation.
	UFUNCTION(BlueprintCallable, Category = Targeting)
	static FRotator OrientWall(FVector SourceLocation, FVector TargetLocation);
};
