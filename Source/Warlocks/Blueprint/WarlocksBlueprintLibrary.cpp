#include "Warlocks/Blueprint/WarlocksBlueprintLibrary.h"

#include "Warlocks/Abilities/AbilityDescriptor.h"
#include "Warlocks/Abilities/AbilityDataHelper.h"

float UWarlocksBlueprintLibrary::EvaluateScalableFloatAtLevel(const FScalableFloat& Float, int Level)
{
	return Float.GetValueAtLevel(Level);
}

FGameplayTag UWarlocksBlueprintLibrary::GetTagFromSlot(ECharacterSlot Slot)
{
	FString SlotName = StaticEnum<ECharacterSlot>()->GetNameStringByValue((int64)Slot);
	FString GameplayTagName = FString::Printf(TEXT("Ability.Cooldown.%sSlot"), *SlotName);
	check(FGameplayTag::IsValidGameplayTagString(GameplayTagName));
	return FGameplayTag::RequestGameplayTag(FName(*GameplayTagName));
}

void UWarlocksBlueprintLibrary::ApplyHelperToDescription(
	UAbilityDataHelper* Helper,
	UAbilityDescriptor* Descriptor,
	EAbilitySchool School,
	EAbilityLevel Level
) {
	uint8 SchoolIndex = uint8(School);
	uint8 LevelIndex = uint8(Level);

	Descriptor->Modify();
	Descriptor->PerSchoolData[SchoolIndex] = Helper->Data;
}

void UWarlocksBlueprintLibrary::PopulateHelperFromDescription(
	class UAbilityDataHelper* Helper,
	class UAbilityDescriptor* Descriptor,
	EAbilitySchool School,
	EAbilityLevel Level
) {
	uint8 SchoolIndex = uint8(School);
	uint8 LevelIndex = uint8(Level);

	Helper->Data = Descriptor->PerSchoolData[SchoolIndex];
}

FRotator UWarlocksBlueprintLibrary::OrientWall(FVector SourceLocation, FVector TargetLocation)
{
	FRotator Orientation = (TargetLocation - SourceLocation).Rotation();

	Orientation.Add(0.0f, 90.0f, 0.0f);

	// Remove all other rotation as we have a flat plane
	Orientation.Roll = 0.0f;
	Orientation.Pitch = 0.0f;

	return Orientation;
}
