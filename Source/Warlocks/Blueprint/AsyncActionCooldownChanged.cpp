#include "AsyncActionCooldownChanged.h"
#include "../Blueprint/WarlocksBlueprintLibrary.h"

UAsyncActionCooldownChanged * UAsyncActionCooldownChanged::ListenForCooldownChange(AWarlocksBaseCharacter* Character, ECharacterSlot Slot)
{
	UAsyncActionCooldownChanged* ListenForCooldownChange = NewObject<UAsyncActionCooldownChanged>();
	ListenForCooldownChange->Character = Character;
	ListenForCooldownChange->Slot = Slot;

	if (!IsValid(Character) || !IsValid(Character->GetAbilitySystemComponent()))
	{
		ListenForCooldownChange->EndTask();
		return nullptr;
	}

	ListenForCooldownChange->SlotTag = UWarlocksBlueprintLibrary::GetTagFromSlot(Slot);

	Character->GetAbilitySystemComponent()->RegisterGameplayTagEvent(ListenForCooldownChange->SlotTag, EGameplayTagEventType::NewOrRemoved).AddUObject(ListenForCooldownChange, &UAsyncActionCooldownChanged::CooldownTagChanged);

	return ListenForCooldownChange;
}

void UAsyncActionCooldownChanged::EndTask()
{
	if (IsValid(Character) && IsValid(Character->GetAbilitySystemComponent()))
	{
		Character->GetAbilitySystemComponent()->RegisterGameplayTagEvent(SlotTag, EGameplayTagEventType::NewOrRemoved).RemoveAll(this);
	}

	SetReadyToDestroy();
	MarkAsGarbage();
}

void UAsyncActionCooldownChanged::CooldownTagChanged(const FGameplayTag CooldownTag, int32 NewCount)
{
	if (NewCount == 1)
	{
		float TimeRemaining = 0.0f;
		float Duration = 0.0f;
		Character->GetCooldownInfoForSlot(Slot, TimeRemaining, Duration);

		OnCooldownBegin.Broadcast(Slot, TimeRemaining, Duration);
	}

	if (NewCount == 0)
	{
		OnCooldownEnd.Broadcast(Slot, -1.0f, -1.0f);
	}
}
