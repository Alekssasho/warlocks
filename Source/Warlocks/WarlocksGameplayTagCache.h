#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

struct FWarlockGameplayTagCache
{
	FWarlockGameplayTagCache();
	static FWarlockGameplayTagCache* Get();

	// Gameplay tags cached below
	FGameplayTag DurationTag;
	FGameplayTag DamageAmountTag;
	FGameplayTag StunChanceTag;
	FGameplayTag CooldownTag;
	FGameplayTag ImmunityHazardFloor;
	FGameplayTag ImmunityKnockback;
	FGameplayTag KnockbackTag;
	// TODO: change this to be a modification
	FGameplayTag FloorSlide;
	FGameplayTag ModificationCharacterSlow;
	FGameplayTag ModificationCharacterFast;
	FGameplayTag ModificationCharacterInvisible;
	FGameplayTag StunTag;
	FGameplayTag EnhanceDamage;
};
