// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Warlocks/Abilities/Effects/WarlockEffectContainer.h"
#include "Warlocks/CommonTypes.h"
#include "WarlockProjectile.generated.h"

UCLASS()
class WARLOCKS_API AWarlockProjectile : public AActor
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	AWarlockProjectile();

	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY()
	TArray<TSubclassOf<UWarlockGameplayEffectContainer>> GameplayEffectsToApply;

	float KnockbackForce;
	float Damage;

	EAbilitySchool OwnAbilitySchool;

	UProjectileMovementComponent* GetMovement() { return Movement; }
protected:
	void RespondToCharacterCollision(AActor* Other);
	void RespondToProjectileCollision(AWarlockProjectile* OtherProjectile);

	void CollisionCastProjectile(class UProjectileAbility* AbilityToFire, AWarlockProjectile* OtherProjectile);
	void CollisionCastModifyFloor(class UModifyFloorAbility* AbilityToFire, AWarlockProjectile* OtherProjectile);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UProjectileMovementComponent> Movement;

	// TODO: Assume sphere collision for intersection, can be changed to just BasicShape and let child classes specify the exact shape if needed
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USphereComponent> Collision;

	// TODO: Either change to UFXSystemComponent or Niagara component when we decide to switch to using Niagara for effects
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UParticleSystemComponent> ProjectileVisualParticleSystem;
};
