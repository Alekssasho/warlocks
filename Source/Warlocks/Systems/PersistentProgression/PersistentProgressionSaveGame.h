#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"

#include "Warlocks/CommonTypes.h"

#include "PersistentProgressionSaveGame.generated.h"

UCLASS()
class WARLOCKS_API UPersistentProgressionSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	static void SaveGame(int32 Index, const TSchoolArray<int32>& Points);
	static void LoadGame(int32 Index, TSchoolArray<int32>& OutPoints);

private:

	//This is the same size as a TSchoolArray, but is a raw array, because TSchoolArray (which is aTStaticArray) cant be UPROPERTY().
	UPROPERTY()
	int32 Points[uint8(EAbilitySchool::Count)];
};
