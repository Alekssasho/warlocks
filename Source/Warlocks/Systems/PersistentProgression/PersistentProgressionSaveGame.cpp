// Fill out your copyright notice in the Description page of Project Settings.


#include "PersistentProgressionSaveGame.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h" 


void UPersistentProgressionSaveGame::SaveGame(int32 Index, const TSchoolArray<int32>& Points)
{
	FString SaveGameName = FString::Printf(TEXT("PersistentProgression%i"), Index);
	UPersistentProgressionSaveGame* SaveGame;
	if (UGameplayStatics::DoesSaveGameExist(SaveGameName, Index))
	{
		SaveGame = Cast<UPersistentProgressionSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveGameName, Index));
	}
	else
	{
		SaveGame = Cast<UPersistentProgressionSaveGame>(UGameplayStatics::CreateSaveGameObject(UPersistentProgressionSaveGame::StaticClass()));
	}

	FMemory::Memcpy(SaveGame->Points, Points.GetData(), uint8(EAbilitySchool::Count) * sizeof(int32));
	UGameplayStatics::SaveGameToSlot(SaveGame, SaveGameName, Index);
}

void UPersistentProgressionSaveGame::LoadGame(int32 Index, TSchoolArray<int32>& OutPoints)
{
	FString SaveGameName = FString::Printf(TEXT("PersistentProgression%i"), Index);
	UPersistentProgressionSaveGame* SaveGame;
	if (!UGameplayStatics::DoesSaveGameExist(SaveGameName, Index))
	{
		return;
	}
	
	SaveGame = Cast<UPersistentProgressionSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveGameName, Index));
	FMemory::Memcpy(OutPoints.GetData(), SaveGame->Points, uint8(EAbilitySchool::Count) * sizeof(int32));
}