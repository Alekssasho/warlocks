# Warlocks? Gift of the Sanctum?

A game about beating other people with spells

## Coding convention

We use UE4's convention wherever possible. Some notable exceptions include
some basic notation on special methods:

* `Async_Foo()` indicates `Foo` will do some processing in the background and its results isn't immediately available (e.g. send a network request)
* `Foo_OnClient()` / `Foo_OnServer()` indicates `Foo` needs to be called only on the client / server.
